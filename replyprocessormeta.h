#ifndef REPLYPROCESSORMETA_H
#define REPLYPROCESSORMETA_H
#include "replyprocessor.h"

class ReplyProcessorMeta : public ReplyProcessor
{
public:
    ReplyProcessorMeta();
    bool addData(QByteArray & nData);
private:
    int getMessageInfo();
    bool mFLSMeta;
    bool mFLSMetaOpen;
};

#endif // REPLYPROCESSORMETA_H
