import QtQuick 1.1
import Network 1.0
import "./Components"

Page {
    id: container

    property variant config: null

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        spacing: 10;

        HeaderText {
            text: "IP Settings";
        }

        Rectangle {
            color: "white"

            border.color: "gray"
            border.width: 2
            radius: 10

            width: 400
            height: settingsColumn.height + 10

            Column {
                id: settingsColumn

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }

                OnOffSetting {
                    id: dhcpSwitch
                    width: 380
                    text: "DHCP"

                    onOnChanged: {
                        config.useDhcp = on;
                    }
                }
                EditValueItem {
                    id: ipAddressEdit
                    enabled: !dhcpSwitch.on
                    text: qsTr("IP Address")
                    valueMask: "000.000.000.000;_"
                    numericValue: true
                }
                EditValueItem {
                    id: subnetEdit
                    enabled: !dhcpSwitch.on
                    text: qsTr("Subnet mask")
                    valueMask: "000.000.000.000;_"
                    numericValue: true
                }
                EditValueItem {
                    id: gatewayEdit
                    enabled: !dhcpSwitch.on
                    text: qsTr("Gateway")
                    valueMask: "000.000.000.000;_"
                    numericValue: true
                }
                EditValueItem {
                    id: dnsEdit
                    enabled: !dhcpSwitch.on
                    text: qsTr("Primary DNS")
                    valueMask: "000.000.000.000;_"
                    numericValue: true
                }
            }
        }

        Button {
            text: qsTr("Apply settings")

            onClicked: {
                config.ipAddress = ipAddressEdit.value;
                config.subnetMask = subnetEdit.value;
                config.defaultGateway = gatewayEdit.value;
                config.primaryDns = dnsEdit.value;
            }
        }

        HeaderText {
            text: "HTTP Proxy"
        }

        Rectangle {
            id: networklist
            width: 400
            height: proxyColumn.height + 10
            radius: 10
            border.width: 2
            border.color: "gray"

            Column {
                id: proxyColumn

                anchors {
                    horizontalCenter: parent.horizontalCenter
                    verticalCenter: parent.verticalCenter
                }

                EditValueItem {
                    text: qsTr("Server")
                }
                EditValueItem {
                    text: qsTr("Port")
                    validator: IntValidator{bottom: 1; top: 65535;}
                    numericValue: true
                }
                EditValueItem {
                    text: qsTr("Username")
                }
                EditValueItem {
                    text: qsTr("Password")
                    echoMode: TextInput.Password
                }
            }
        }
    }
}
