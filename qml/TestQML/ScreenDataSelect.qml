// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Models 1.0
import "../js/Styler.js" as Styler
import "./Components"

Page {
    id: container

    signal dataSelected(string name, string value);

    ListModel {
        id: lmConnectionType;
        ListElement { display: "3G UMTS" }
        ListElement { display: "LAN" }
        ListElement { display: "WiFi" }
    }

    ListModel {
        id: lmLockScreenTimeout
        ListElement { display: "2 minutes" }
        ListElement { display: "5 minutes" }
        ListElement { display: "10 minutes" }
        ListElement { display: "Never" }
    }

    property string displayName;

    function loadData(name)
    {
        displayName = name;

        if (displayName == "Airline")
        {
            dataLoadModel.loadAirlines();
        }
        else if (displayName == "ACType")
        {
            displayName = "A/C Model";
            if (main.oldState == "DataLoadUsb")
                dataLoadModel.loadActmodels(dataLoadUsbPage.airline);
            else
                dataLoadModel.loadActmodels(backend.airline);
        }
        else if (displayName == "Tailsign")
        {
            displayName = "A/C Reg";
            if (main.oldState == "DataLoadUsb")
                dataLoadModel.loadTailsigns(dataLoadUsbPage.airline, dataLoadUsbPage.acType);
            else
                dataLoadModel.loadTailsigns(backend.airline, backend.actype);
        }
        else if (displayName == "ATA Chapter")
        {
            displayName = "ATA Chapter";
            if (main.oldState == "DataLoadUsb")
                dataLoadModel.loadAtaChapters(dataLoadUsbPage.airline, dataLoadUsbPage.acType, dataLoadUsbPage.tailsign);
            else
                dataLoadModel.loadAtaChapters(backend.airline, backend.actype, backend.tailsign);
        }
        else if (displayName == "ATA Section")
        {
            displayName = "ATA Section";
            if (main.oldState == "DataLoadUsb")
                dataLoadModel.loadAtaSections(dataLoadUsbPage.airline, dataLoadUsbPage.acType, dataLoadUsbPage.tailsign, dataLoadUsbPage.ataChapter);
            else
                dataLoadModel.loadAtaSections(backend.airline, backend.actype, backend.tailsign, backend.ataChapter);
        }
        else if (displayName == "Software")
        {
            displayName = "Software P/N";
            if (main.oldState == "DataLoadUsb")
                dataLoadModel.loadSwpn(dataLoadUsbPage.airline, dataLoadUsbPage.acType, dataLoadUsbPage.tailsign, dataLoadUsbPage.ataChapter, dataLoadUsbPage.ataSection);
            else
                dataLoadModel.loadSwpn(backend.airline, backend.actype, backend.tailsign, backend.ataChapter, backend.ataSection);
        }
        else if (displayName == "Connection Type")
        {
            list_view1.model = lmConnectionType;
        }
        else if (displayName == "Lock Screen Timeout")
        {
            list_view1.model = lmLockScreenTimeout;
        }
    }

    function selectItem(item)
    {

    }

    Rectangle {
        id: container_

        anchors {
            horizontalCenter: parent.horizontalCenter
            verticalCenter: container_.height < container.height ? undefined : parent.verticalCenter
            top: container_.height < container.height ? container.top : undefined
            topMargin: container_.height < container.height ? 10 : 0
        }

        height: {
            var h = ((list_view1.count) * Styler.Sizes.MenuItem.Height) + 10;
            return (h > container.height) ? container.height - 20 : h;
        }
        width: Styler.Sizes.MenuItem.Width + 10


        radius: 10
        border.width: 2
        border.color: "gray"
        color: "lightgray"

        ListView {
            anchors.fill: parent;
            anchors.margins: 5
            clip: true
            id: list_view1

            model: DataLoadModel {
                id: dataLoadModel
            }

            delegate:
                MenuItem {
                text: displayName == "Connection Type" ? model.display : model.value
                showSelector: false
                onClicked: {
                    if (displayName == "Connection Type")
                        container.dataSelected(model.display, "");
                    else
                        container.dataSelected(model.id, model.value);
                }
            }
        }
    }
}
