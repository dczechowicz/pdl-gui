#!/bin/bash

case `uname -n` in
	"pentacom-2")
                NUMFILE=./build_number;
                HEADER=./build_number.h;
		;;
	*)
                NUMFILE=../poland/build_number;
                HEADER=../poland/build_number.h;
                ;;
esac

number=`cat $NUMFILE`
let number++
echo "$number" | tee $NUMFILE
export DATE=\"`date +%d/%m/%Y`\"
export TIME=\"`date +%H:%M`\"
export BUILD=\"$number\"
(
echo "#define BUILD" $BUILD
echo "#define BUILDDATE" $DATE
echo "#define BUILDTIME" $TIME
) | tee $HEADER
