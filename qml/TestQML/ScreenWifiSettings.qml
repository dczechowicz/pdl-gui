// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "./Components"

Page {
    id: container

    signal wifiDetails

    onVisibleChanged: {
        if (visible && wiFiConfig.active) {
            wiFiConfig.requestAccessPoints();
        }
    }

    Connections {
        target: wiFiConfig

        onAccessPointsChanged: {
            accessPointsModel.clear();

            for (var i = 0; i < wiFiConfig.accessPoints.length; ++i) {
                var aPoint = wiFiConfig.accessPoints[i];
                accessPointsModel.append({"ssid": aPoint.ssid, "stregth": aPoint.stregth,
                                             "isPasswordRequired": aPoint.isPasswordRequired })
            }
        }

        onActiveChanged: {
            if (wiFiConfig.active)
                wiFiConfig.requestAccessPoints();
        }
    }

    ListModel {
        id: accessPointsModel
    }

    Rectangle {
        x: 40
        y:1
        Column {
            spacing: 7;

            HeaderText {
                text: "General settings";
            }

            Rectangle {
                color: "white"

                border.color: "gray"
                border.width: 2
                radius: 10

                width: 400
                height: 40

                OnOffSetting {
                    anchors.fill: parent;
                    anchors.margins: 4
                    anchors.leftMargin: 10

                    id: userloginsetting
                    text: "WiFi Enabled:"

                    onOnChanged: {
                        if (on) {
                            wiFiConfig.upInterface();
                        } else
                            wiFiConfig.downInterface();
                    }
                }
            }

            HeaderText {
                text: "WiFi networks"
            }

            Rectangle {
                id: networklist
                width: 400
                height: 300
                radius: 10
                border.width: 2
                border.color: "gray"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 4
                    anchors.leftMargin: 5

                    ListView {
                        id: wapList
                        anchors.fill: parent
                        clip: true
                        model: accessPointsModel
                        delegate: MenuItem {
                            text: model.ssid
                            width: wapList.width
                            onClicked: wiFiConfig.connectAccessPoint(model.ssid, "")
                        }
                    }
                }
            }

            Button {
                text: "Add network"
            }

            Button {
                text: "IP Settings"
                onClicked: container.wifiDetails()
            }
        }
    }
}
