// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
    width: 480;
    height: 750;
    id: container
    color: "lightgray"

    signal selectData( string name, string selected)
    signal lanSettingsClicked;
    signal wifiSettingsClicked;

    function setData(datatype, value)
    {
        if (datatype == "Lock Screen Timeout")
        {
            lockScreenTimeoutMenuItem.value = value;
        }
    }

    Flickable {
//        y: 1
        anchors.topMargin: 1
        anchors.fill: parent
        contentHeight: column.height  + 20
        clip: true

        Column {
            id: column
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 1;


            HeaderText {
                text: "Data Load Support"
            }

            Rectangle {
                id: dataloadsupport
                width: 400
                height: 80
                radius: 10
                border.width: 2
                border.color: "gray"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 4
                    anchors.leftMargin: 10

                    Column {
                        OnOffSetting {
                            width: 385
                            id: arinc6153
                            text: "ARINC 615-3";
                        }
                        OnOffSetting {
                            width: 385
                            id: arinc615A
                            text: "ARINC 615A";
                        }
                    }
                }
            }
            /*
      Rectangle {
          id: dataloadsettings
          width: 400
          height: 40
          radius: 10
          border.width: 2
          border.color: "gray"

          MenuItem {
            anchors.margins: 5
            anchors.fill: parent
            text: "Data Load Settings"
          }
      }
*/

            HeaderText {
                text: "Secure USB"
            }

            Rectangle {
                color: "white"

                border.color: "gray"
                border.width: 2
                radius: 10

                width: 400
                height: 40

                OnOffSetting {
                    anchors.fill: parent;
                    anchors.margins: 4
                    anchors.leftMargin: 10

                    id: secureusb
                    text: "Secure USB:"
                }
            }
            /*
      Rectangle {
          id: automaticpurge
          width: 400
          height: 80
          radius: 10
          border.width: 2
          border.color: "gray"

          Rectangle {
              anchors.fill: parent
              anchors.margins: 4
              anchors.leftMargin: 5

              Column {
                MenuItem {
                  text: "Automatic Purge"
                }
                MenuItem {
                  text: "Purge secure USB now";
                  showSelector: false
                }
              }
          }
      }
*/

            HeaderText {
                text: "Repository"
            }
            Rectangle {
                id: repositorySettings
                width: 400
                height: 80
                radius: 10
                border.width: 2
                border.color: "gray"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 4
                    anchors.leftMargin: 10

                    Column {
                        OnOffSetting {
                            width: 385
                            text: "Repository";
                        }
                        OnOffSetting {
                            width: 385
                            text: "Update Repository";
                        }
                    }
                }
            }
            Rectangle {
                id: reposUpdateSetting
                width: 400
                height: 190
                radius: 10
                border.width: 2
                border.color: "gray"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 4
                    anchors.leftMargin: 5

                    Column {
                        MenuItem {
                            text: "Update Settings"
                            width: 390
                        }
                        MenuItem {
                            text: "Purge Repository Now";
                            width: 390
                            showSelector: false
                        }
                    }
                }
            }

            HeaderText {
                text: "Connection Settings";
            }

            Rectangle {
                id: connectionSettings
                width: 400
                height: 280
                radius: 10
                border.width: 2
                border.color: "gray"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 4
                    anchors.leftMargin: 5

                    Column {
                        MenuItem {
                            text: "3G/UMTS"
                            width: 390
                            //onClicked: container.umtsSettingsClicked();
                        }
                        MenuItem {
                            text: "LAN";
                            width: 390
                            onClicked: container.lanSettingsClicked();
                        }
                        MenuItem {
                            text: "WiFi";
                            width: 390
                            onClicked: container.wifiSettingsClicked();
                        }
                    }
                }
            }
        }
    }
}
