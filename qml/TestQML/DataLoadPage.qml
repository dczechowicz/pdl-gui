import QtQuick 1.1
import "./Components"

Page {
    id: root

    signal a6153MenuSelection()
    signal a6153ManualSelection()
    signal secUsbMenuSelection()
    signal secUsbManualSelection()
    signal a615AMenuSelection()
    signal a615AManualSelection()

    Column {
        anchors {
            top: parent.top
            topMargin: 30
            horizontalCenter: parent.horizontalCenter
        }

        spacing: 15

        HeaderText {
            text: "A615-3"
            visible: backend.getA6153Enabled()
        }

        GroupBox {
            radius: 10
            border.width: 2
            border.color: "blue"
            visible: backend.getA6153Enabled()
            color: "lightblue"

            MenuItem {
                text: "Menu selection"

                onClicked: root.a6153MenuSelection()
            }
            MenuItem {
                text: "Manual selection"

                onClicked: root.a6153ManualSelection()
            }
        }

        HeaderText {
            text: "Secure USB"
            visible: backend.getSecureUsbEnabled()
        }

        GroupBox {
            radius: 10
            border.width: 2
            border.color: "blue"
            visible: backend.getSecureUsbEnabled()
            color: "lightblue"

            MenuItem {
                text: "Menu selection"

                onClicked: root.secUsbMenuSelection()
            }
            MenuItem {
                text: "Manual selection"

                onClicked: root.secUsbManualSelection()
            }
        }

        HeaderText {
            text: "A615-A"
            visible: backend.getA615AEnabled()
        }

        GroupBox {
            radius: 10
            border.width: 2
            border.color: "blue"
            visible: backend.getA615AEnabled()
            color: "lightblue"

            MenuItem {
                text: "Menu selection"

                onClicked: root.a615AMenuSelection()
            }
            MenuItem {
                text: "Manual selection"

                onClicked: root.a615AManualSelection()
            }
        }
    }
}
