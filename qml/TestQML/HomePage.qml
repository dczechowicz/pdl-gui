// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "./Components"

Page {
    id: container

    signal dataLoadClicked;
    signal dataLoadUsbClicked;
    signal dataLoadManualClicked;
    signal secureUsbClicked;
    signal systemClicked;
    signal updateRepositoryClicked;
    signal informationClicked;
    signal shutdownClicked;
    signal repositoryClicked;
    signal internalClicked;

    GroupBox {
        id: menupdl

        anchors {
            top: parent.top
            topMargin: 30
            horizontalCenter: parent.horizontalCenter
        }

        radius: 10
        border.width: 2
        border.color: "gray"

        color: "lightgray"

        signal administration;
        signal information;

        MenuItem {
            id: dataLoadItem
            text: "Data Load"
            onClicked: container.dataLoadClicked();
        }

        MenuItem {
            id: syncItem
            text: "Synchronisation"
            onClicked: container.updateRepositoryClicked();
        }

        MenuItem {
            id: repositoryItem
            text: "Repository"
            onClicked: container.repositoryClicked();
        }

        MenuItem {
            text: "System Information"
            onClicked: container.informationClicked();
        }

        MenuItem {
            text: "Shutdown"
            visible: false;
            onClicked: container.shutdownClicked();
        }
        MenuItem {
            text: "Internal"
            visible: false
            //visible: backend.isFreeMode();
            onClicked: container.internalClicked();
        }
    }

    Column {
        anchors {
            bottom: parent.bottom
            bottomMargin: 15
            horizontalCenter: parent.horizontalCenter
        }

        ValueItem {
            text: qsTr("PDL-Name:")
            value: backend.pdlName
            color: "transparent"
            gradient: null
        }

        ValueItem {
            text: qsTr("Last sync:")
            value: backend.lastSync
            color: "transparent"
            gradient: null
        }

    }
}
