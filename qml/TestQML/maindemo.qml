// import QtQuick 1.0 // to target S60 5th Edition or Memo 5
import QtQuick 1.1
import '../colibri'


Rectangle {
  id: main
  width: 480
  height: 800
  x: 0
  y: 0

  property string oldState: ""

  ScreenInformation {
      id: screen_info

      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"
      visible: false
  }

    ScreenLogin {
      id: screen_login
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"
      visible: false

      onHomeClicked: main.state = "Home"
      onShutdownClicked: backend.quit();
      onRestartClicked: backend.restart();
    }

    ScreenMain {
      id: screen_home
      visible: false
      color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      border.color: "#000000"

      onDataLoadManualClicked: { main.state = "DataLoadMan"; }
      onDataLoadClicked: { main.state = "DataLoad_1"; }
      onSecureUsbClicked: main.state = "SecureUsb";
      onUpdateRepositoryClicked: main.state = "UpdateRepository";
      onUploadResultsClicked: main.state = "UploadResults";
      onAdministrationClicked: main.state = "Administration";
      onInformationClicked: main.state = "Information";
      onShutdownClicked: main.state = "Login";
    }

    ScreenDataLoad {
      id: screen_dataload
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      property string currentType;

      onSelectData: {
        currentType = name;
        screen_dataselect.loadData(name);
        screen_dataselect.selectItem(selected);

        main.oldState = main.state;
        main.state = "DataSelect";
      }

      function test(name)
      {
        screen_dataselect.selectItem(name);
        setData(currentType, name);
      }
    }

    ScreenDataLoadMan {
      id: screen_dataloadman
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      property string currentType;

      onSelectData: {
        currentType = name;
        screen_dataselect.loadData(name);
        screen_dataselect.selectItem(selected);

        main.oldState = main.state;
        main.state = "DataSelect";
      }

      function test(name)
      {
        screen_dataselect.selectItem(name);
        setData(currentType, name);
      }
    }

    ScreenDataLoad_1 {
      id: screen_dataload_1
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      property string currentType;

      onSelectData: {
        currentType = name;
        screen_dataselect.loadData(name);
        screen_dataselect.selectItem(selected);

        main.oldState = main.state;
        main.state = "DataSelect";
      }

      function test(name)
      {
        screen_dataselect.selectItem(name);
        setData(currentType, name);
      }
    }

    ScreenSecureUSB {
      id: screen_secureUSb
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      property string currentType;

      onSelectData: {
        console.log("Test:" + name + " select: " + selected);

        currentType = name;
        screen_dataselect.loadData(name);
        screen_dataselect.selectItem(selected);

        main.oldState = main.state;
        main.state = "DataSelect";
      }

      function test(name)
      {
        console.log("all is wel: " + name);
        screen_dataselect.selectItem(name);
        setData(currentType, name);
      }
    }

    ScreenAdministration {
      id: screen_administration;
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      property string currentType;

      onWifiSettingsClicked: {
        main.state = "WiFiSettings"
      }

      onSelectData: {
        console.log("Test:" + name + " select: " + selected);

        currentType = name;
        screen_dataselect.loadData(name);
        screen_dataselect.selectItem(selected);

        main.oldState = main.state;
        main.state = "DataSelect";
      }

      function test(name)
      {
        console.log("all is wel: " + name);
        screen_dataselect.selectItem(name);
        setData(currentType, name);
      }
    }

    ScreenUpdateFirmware {
      id: screen_updatefirmware;
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      property string currentType;

      onSelectData: {
        console.log("Test:" + name + " select: " + selected);

        currentType = name;
        screen_dataselect.loadData(name);
        screen_dataselect.selectItem(selected);

        main.oldState = main.state;
        main.state = "DataSelect";
      }

      function test(name)
      {
        console.log("all is wel: " + name);
        screen_dataselect.selectItem(name);
        setData(currentType, name);
      }
    }

    ScreenUploadResults {
      id: screen_uploadResults;
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      property string currentType;

      onSelectData: {
        console.log("Test:" + name + " select: " + selected);

        currentType = name;
        screen_dataselect.loadData(name);
        screen_dataselect.selectItem(selected);

        main.oldState = main.state;
        main.state = "DataSelect";
      }
    }
    ScreenDataSelect {
      id: screen_dataselect;
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      onDataSelected: {
        console.log("Selected: " + name);

        if (main.oldState == "DataLoad") {
          screen_dataload.test(name);
        }
        else if (main.oldState == "DataLoadMan") {
          screen_dataloadMan.test(name);
        }
        else if (main.oldState == "DataLoad_1") {
          screen_dataload_1.test(name);
        }
        else if (main.oldState == "SecureUsb") {
          screen_secureUSb.test(name);
        }
        else if (main.oldState == "UpdateRepository") {
          screen_updatefirmware.test(name)
        }
        else if (main.oldState == "Administration") {
          screen_administration.test(name)
        }

        main.state = main.oldState;
      }
    }

    ScreenWifiSettings {
      id: screen_wifisettings
      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"

      onWifiDetails: {
        main.state = "WiFiDetails"
      }
    }

    ScreenWifiDetails {
      id: screen_wifidetail

      visible: false
      //color: "#aaaaaa"
      anchors.right: parent.right
      anchors.rightMargin: 0
      anchors.left: parent.left
      anchors.leftMargin: 0
      anchors.bottom: parent.bottom
      anchors.bottomMargin: 0
      anchors.top: header.bottom
      anchors.topMargin: 0
      //border.color: "#000000"
    }

    Rectangle {
        id: header
        width: 480
        height: 50
        gradient: Gradient {
          GradientStop {
              position: 0
              color: "#646464"
          }

          GradientStop {
              position: 0.340
              color: "#000000"
          }

          GradientStop {
              position: 0.750
              color: "#000000"
          }

          GradientStop {
              position: 1
              color: "#646363"
          }
        }
        transformOrigin: Item.Center
        visible: true
        border.color: "#000000"
        border.width: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        Text {
            id: header_name
            color: "#ffffff"
            text: qsTr("Home")
            font.bold: true
            font.family: "Verdana"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent
            font.pixelSize: 30
        }

        Button {
            id: header_lock
            x: 361
            y: 15
            width: 94
            height: 20
            color: "#000000"
            radius: 15
            border.width: 2
            border.color: "#808080"
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 25
            text: qsTr("Lock Screen");
            textSize: 8
            visible: false
            //onClicked: main.state = "Login";
          onClicked: {
            if (backend.loginEnabled) {
              main.state = "Login";
            }
          }
        }

        Button {
            id: header_home
            y: 12
            width: 94
            height: 20
            color: "#424891"
            radius: 15
            anchors.left: parent.left
            anchors.leftMargin: 25
            anchors.top: parent.top
            anchors.topMargin: 10
            border.color: "#808080"
            text: qsTr("Home");
            textSize: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            border.width: 2

            onClicked: {
                if (backend.homeEnabled) {
                    main.state = "Home";
                }
            }
        }
        Button {
            id: header_back
            y: 12
            width: 94
            height: 20
            color: "#424891"
            radius: 15
            anchors.left: parent.left
            anchors.leftMargin: 25
            anchors.top: parent.top
            anchors.topMargin: 10
            border.color: "#808080"
            text: qsTr("Back");
            textSize: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            border.width: 2

            visible: false
            onClicked: {
                if (backend.homeEnabled) {
                    main.state = main.oldState;
                }
            }
        }

    }

    state: "DataLoad_1"
    //state: main.state = backend.mCurrPage

    states: [
      State {
        name: "Login"
        PropertyChanges { target: screen_login; visible: true }

        PropertyChanges { target: header_lock; visible: false }
        PropertyChanges { target: header_home; visible: false }
        PropertyChanges { target: header_name; text: "Login" }
      },
      State {
        name: "Home"
        PropertyChanges { target: screen_home; visible: true }

        PropertyChanges { target: header_home; visible: false }
        PropertyChanges { target: header_name; text: "Home" }
      },
        State {
          name: "DataLoad"
          PropertyChanges { target: screen_dataload; visible: true }
          PropertyChanges { target: header_name; text: "Data Load" }
        },
        State {
          name: "DataLoadMan"
          PropertyChanges { target: screen_dataloadman; visible: true }
          PropertyChanges { target: header_name; text: "Data Load" }
        },
      State {
        name: "DataLoad_1"
        PropertyChanges { target: screen_dataload_1; visible: true }
        PropertyChanges { target: header_name; text: "Data Load" }
      },
      State {
        name: "DataSelect"
        PropertyChanges { target: screen_dataselect; visible: true }
        PropertyChanges { target: header_back; visible: true }

        PropertyChanges { target: header_name; text: screen_dataselect.displayName }
      },
      State {
        name: "SecureUsb"
        PropertyChanges { target: screen_secureUSb; visible: true }

        PropertyChanges { target: header_name; text: "Secure USB" }
      },
      State {
        name: "UpdateRepository"
        PropertyChanges { target: screen_updatefirmware; visible: true }

        PropertyChanges { target: header_name; text: "Repository" }
      },
        State {
          name: "UploadResults"
          PropertyChanges { target: screen_uploadResults; visible: true }

          PropertyChanges { target: header_name; text: "Upload Results" }
        },
      State {
        name: "Administration"
        PropertyChanges { target: screen_administration; visible: true }

        PropertyChanges { target: header_name; text: "Administration" }
      },
      State {
        name: "Information"
        PropertyChanges { target: screen_info; visible: true }

        PropertyChanges { target: header_name; text: "Information" }
      },
      State {
        name: "WiFiSettings"
        PropertyChanges { target: screen_wifisettings; visible: true }

        PropertyChanges { target: header_name; text: "WiFi Settings" }
      },
      State {
        name: "WiFiDetails"
        PropertyChanges { target: screen_wifidetail; visible: true }

        PropertyChanges { target: header_name; text: "Network" }
      }
    ]

}
