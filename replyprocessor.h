#ifndef REPLYPROCESSOR_H
#define REPLYPROCESSOR_H

#include <QObject>
#include <QFile>
#include "mediainfo.h"

typedef enum {
    PROC_FIRST_HEADER,
    PROC_FIRST_DATA,
    PROC_DATA
} proc_seq_t;

class ReplyProcessor : public QObject
{
    Q_OBJECT
    friend class ReplyProcessorMedia;
    friend class ReplyProcessorMeta;
    friend class ReplyProcessorLogin;
    friend class ReplyProcessorSoftware;

public:
    explicit ReplyProcessor(QObject *parent = 0);
    
signals:
    
public slots:

private:
    static QByteArray mLocalBuffer;

    bool mDebug;
    proc_seq_t mState;
    bool mChunked;
    bool mGZipped;
    bool mOpened;
    int  mContentLen;
    int  mInOffset;
    int  mSkip;
    int  mFileOffset;
    int  mFileSize;
    int  mLimit;
    int  mCurr;
    int  mChunkedLen;
    int  mLength;
    QFile mFile;

    // dumpfile
    QFile dFile;
    bool mDumpData;
    bool mDumpOpen;
    bool mCompleted;
    static int  mDumpCount;
    bool dumpData(QByteArray & nData);
    void closeDump();

    mediaInfo mMediaInf;

    QByteArray mChunkedField;
    QByteArray mHeader;
    QByteArray mBoundary;
    QByteArray mBuffer;
    QByteArray mZipBuffer;
    QString    mDirname;
    QString    mPartNumber;
    QString    mSWPath;

    QHash<QString, QString> mNameVal;

    virtual bool openFile();
    void closeFile();
    virtual void write2File();

    virtual int getMessageInfo() = 0;

    void getHeader(QByteArray & nData);
    bool getChunked(QByteArray & nData);
    bool getData(QByteArray & nData);
    bool getLine(QByteArray & nData, QByteArray & line);
    int  getLineGen(QByteArray & data, int offset, QByteArray & line);
    void getNameValue(QByteArray & line, QByteArray & name, QByteArray & value);
    QByteArray getAttr(const char *, QByteArray & value);
    int  getChunkLen(QByteArray & nData);
    int  getRemLength();
    int  getFileOffset();
    QByteArray gzipDecompress( QByteArray compressData );

public:
    QString getValue(const QString & name);
    virtual bool addData(QByteArray & nData) = 0;
    bool testFromFile(QString filename);
    void setDirectory(QString dirname, QString partNumber);
    void setRepDirectory(QString dirname);
};

#endif // REPLYPROCESSOR_H
