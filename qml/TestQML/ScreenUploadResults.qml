// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "./Components"

Page {
    id: container

    signal selectData( string name, string selected)

    function setData(datatype, value)
    {
        if (datatype == "Connection Type")
        {
            bsConnectionType.value = value;
        }
    }

    Column {
        anchors.horizontalCenter: parent.horizontalCenter
        HeaderText {
            text: "Connection Type";
            visible: false
        }

        Rectangle {
            radius: 10
            border.width: 2
            border.color: "gray"

            width: 400
            height: 100
            visible: false
            color: "lightgray"
            Column {
                anchors.fill: parent;
                anchors.margins: 5
                spacing: 1

                MenuItem {
                    id: bsConnectionType
                    width: 390
                    text: "Type"
                    value: "WiFi"

                    onClicked: container.selectData("Connection Type", value);
                }
            }
        }

        HeaderText {
            text: "Update"
        }

        Rectangle {
            radius: 10
            border.width: 2
            border.color: "gray"

            width: 400
            height: 82 + 36

            color: "lightgray"
            Column {
                anchors.fill: parent;
                anchors.margins: 5
                spacing: 1

                ValueItem {
                    text: "Status"
                    value: backend.updateStatus
                }
                ValueItem {
                    text: "Start Time";
                    value: backend.updateStartTime
                }
                ValueItem {
                    text: "Progress"
                    value: backend.updateProgress
                }
            }
        }
        Rectangle {
            color: "#AAAAAA"
            height: 5
            width: 5
        }

        Button {
            text: backend.updateRunning?"Stop Upload":"Start Upload"
            onClicked: {
                if (backend.updateRunning)
                {
                    //backend.stopUpload();
                }
                else
                {
                    //backend.startUpload();
                }
            }
        }
        Rectangle {
            color: "#AAAAAA"
            height: 5
            width: 10
        }

        Rectangle {
            width: 400
            height: 220
            color: "white"
            radius: 10
            visible: backend.showLogWindow;
            border.width: 2
            border.color: "gray"
            Flickable {
                contentHeight: logid.height
                contentWidth: logid.width
                anchors.margins: 10
                anchors.fill: parent;
                clip: true
                flickableDirection: Flickable.VerticalFlick
                contentY: logid.height - height;

                Text {
                    id: logid
                    text: backend.logText;
                }
            }
        }
    }
}
