import QtQuick 1.1
import Network 1.0
import "../colibri"
import "./Components"
import "../js/Styler.js" as Styler

Page {
  id: container
  height: 800
  signal homeClicked

  Connections {
      target: backend
  }

  Rectangle {
      x: 40
      y: 1
      Column {
          HeaderText {
              text: "Interface exception";
          }

          Rectangle {
              width: 400
              height: 150
              color: "white"
              radius: 10
              visible: true;
              border.width: 2
              border.color: "gray"

              Flickable {
                  contentHeight: logid.height
                  contentWidth: logid.width
                  anchors.margins: 10
                  anchors.fill: parent;
                  clip: true
                  flickableDirection: Flickable.VerticalFlick
                  contentY: logid.height - height;

                  Text {
                      id: logid
                      text: backend.logText;
                  }
              }
          }

          Button {
             id: btnSignals
             text: qsTr("Restart")
             visible: false

             onClicked: backend.homeClicked();
          }

      }
  }
}
