#include <QDebug>
#include <QFile>
#include <QString>
#include <QList>
#include <QListIterator>
#include <zlib.h>
#include "replyprocessor.h"
#include "fileinfo.h"

int ReplyProcessor::mDumpCount=0;
QByteArray ReplyProcessor:: mLocalBuffer;

ReplyProcessor::ReplyProcessor(QObject *parent) :
    QObject(parent)
{
    mState      = PROC_FIRST_HEADER;
    mChunked    = false;
    mGZipped    = false;
    mContentLen = -1;
    mChunkedLen = -1;
    mFileOffset = 0;
    mOpened     = false;
    mDumpOpen   = false;
    mCompleted  = false;
    mSkip       = 0;
    mLocalBuffer.clear();
    mLocalBuffer.reserve(8192);
    mDebug      = false;
    mDumpData   = false;
}

QByteArray ReplyProcessor::gzipDecompress( QByteArray compressData )
{
    //decompress GZIP data
    //strip header and trailer
    compressData.remove(0, 10);
    compressData.chop(12);

    const int buffersize = 500000;
    quint8 buffer[buffersize];

    z_stream cmpr_stream;
    cmpr_stream.next_in   = (unsigned char *)compressData.data();
    cmpr_stream.avail_in  = compressData.size();
    cmpr_stream.total_in  = 0;

    cmpr_stream.next_out  = buffer;
    cmpr_stream.avail_out = buffersize;
    cmpr_stream.total_out = 0;

    cmpr_stream.zalloc    = Z_NULL;

    if( inflateInit2(&cmpr_stream, -8 ) != Z_OK) {
        qDebug() << "cmpr_stream error!";
    }

    QByteArray uncompressed;
    do {
        int status = inflate( &cmpr_stream, Z_SYNC_FLUSH );

        if(status == Z_OK || status == Z_STREAM_END) {
            uncompressed.append(QByteArray::fromRawData((char *)buffer, buffersize - cmpr_stream.avail_out));
            cmpr_stream.next_out  = buffer;
            cmpr_stream.avail_out = buffersize;
            qDebug() << "buffersize" << buffersize;
        } else {
            inflateEnd(&cmpr_stream);
            qDebug() << "inflate 1";
        }

        if(status == Z_STREAM_END) {
            inflateEnd(&cmpr_stream);
            qDebug() << "inflate 2";
            break;
        }
        qDebug() << "avail_out" << cmpr_stream.avail_out;
    } while(cmpr_stream.avail_out == 0);

    return uncompressed;
}

bool ReplyProcessor::testFromFile(QString filename) {


    if (!QFile::exists(filename)) {
        qDebug() << "Failed to find '" << filename << "'";
        return false;
    }

    QFile f(filename);

    qDebug() << "Opening" << filename;

    if (!f.open(QIODevice::ReadOnly))  {
        qDebug() << "Failed to open file.";
        return false;
    }

    QByteArray block;
    bool ret = false;

    int size = 1000;
    int offset = 0;

    while (!f.atEnd()) {
        qDebug() << "Offset:" << hex << offset;
        QByteArray block = f.read(size);
        offset += size;

        size = (size == 4096) ? 4095 : 4096;
        ret = addData(block);
        if (ret) {
            qDebug() << "addData returned:" << ret;
            break;
        }
        //if (fsize == mFileSize)
            //break;
    }
    f.close();
    return true;
}

bool ReplyProcessor::dumpData(QByteArray & nData) {
    if (!mDumpData)
        return false;
    if (!mDumpOpen) {
        QString t;
        QString filename = t.sprintf("/tmp/dump%02d", mDumpCount++);
        dFile.setFileName(filename);

        if (!dFile.open(QIODevice::WriteOnly|QIODevice::Unbuffered)) {
            qDebug() << "Can't open:<" + filename + ">";
            return false;
        }
        mDumpOpen = true;
    }
    dFile.write(nData);
    //qDebug() << "DATALEN" << nData.length();
    return true;
}

void ReplyProcessor::closeDump() {
    if (mDumpOpen) {
        dFile.close();
        mDumpOpen=false;
    }
}

void ReplyProcessor::write2File() {
    int offset = 0;

    if (mState == PROC_FIRST_DATA) {
        offset = getMessageInfo();
        mState = PROC_DATA;
    }
    if (!openFile()) {
        return;
    }

    // Only write file size length.
    //qDebug() << "Length" << mBuffer.length() << "rest" << mFileSize;
    int length = mBuffer.length();
    if (length > mFileSize)
        length = mFileSize;

    mFileSize -= length;
    mCurr     += length;
    if (mCurr >= mLimit) {
        //qDebug() << "Length" << length << "rest" << mFileSize;
        mCurr = 0;
    }
    if (length) {
        //qDebug() << "write" << length;
        mFile.write(mBuffer, length);
    }

    mBuffer.clear();
}

bool ReplyProcessor::openFile() {
    if (mOpened)
        return true;

    QString filename = mSWPath + "/" + mMediaInf.getValue("filename");
    mFile.setFileName(filename);
    qDebug() << "zipfile:" << filename;

    if (!mFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Can't open:<" + filename + ">";
        return false;
    }
    mOpened = true;
    return true;
}

void ReplyProcessor::closeFile() {
    if (!mOpened)
        return;
    mOpened = false;
    mCompleted = true;
    qDebug() << "close file";
    mFile.close();
}

int ReplyProcessor::getFileOffset() {
    int fo = mFileOffset + mInOffset;
    //qDebug() << hex << fo;
    return fo;
}

void ReplyProcessor::setRepDirectory(QString dirname) {
    mDirname    = dirname;
    qDebug() << "mDirname" << mDirname;
}

void ReplyProcessor::setDirectory(QString dirname, QString partNumber) {
    mDirname    = dirname;
    mPartNumber = partNumber;
    mSWPath     = mDirname + "/" + mPartNumber;
    qDebug() << "mSWPath" << mSWPath;
}

void ReplyProcessor::getHeader(QByteArray & nData) {
    QByteArray line;
    QByteArray name;
    QByteArray value;

    while(getLine(nData, line)) {
        //qDebug() << "RP" << mInOffset << "line" << line;
        mHeader += line;
        mHeader += '\n';
        getNameValue(line, name, value);
        if (value == "gzip") {
            qDebug() << "gzipped";
            mGZipped = true;
            continue;
        }
        if (name == "content-length") {
            mContentLen = value.toInt();
            qDebug() << "content-length" << mContentLen;
            continue;
        }
        if (value == "chunked") {
            qDebug() << "chunked";
            mChunked = true;
            continue;
        }
        if (name == "content-type") {
            mBoundary = "--" + getAttr((const char *)"boundary", value);
            qDebug() << "boundary" << mBoundary;
        }
    }
}

int ReplyProcessor::getChunkLen(QByteArray & nData) {
    int  len = -1;

    getFileOffset();
    if (mDebug) qDebug() << "getChunkLenIn " << len << hex << mInOffset << mChunkedField.length();
    for (int i=mInOffset; i < nData.length(); i++) {
        switch(nData[i]) {
        case '\r':
            continue;
        case '\n':
            len = mChunkedField.toInt((bool *)0, 16);
            mChunkedField.clear();
            mInOffset = i + 1;
            break;
        default:
            mChunkedField.append(nData[i]);
            continue;
        }
        break;
    }
    if (mDebug) qDebug() << "getChunkLenOut" << len << hex << mInOffset << mChunkedField.length();
    return len;
}

int ReplyProcessor::getRemLength() {
    return mLength - mInOffset;
}

bool ReplyProcessor::getChunked(QByteArray & nData) {
    int remLength;
    while (1) {
        if (mChunkedLen == -1) {
            mInOffset += mSkip;
            mSkip = 0;
            mChunkedLen = getChunkLen(nData);
            // Length field incomplete;
            if (mChunkedLen == -1) {
                return false;
            }
            // End of message
            if (mChunkedLen == 0) {
                qDebug() << "EndOfMessage";
                return true;
            }
        }
        remLength = getRemLength();
        if (mDebug) qDebug() << "getChunked      " << mInOffset << remLength << mChunkedLen;
        // Chunk is complete
        if (remLength >= mChunkedLen) {
            //qDebug() << "Whole";
            mBuffer.append(nData.mid(mInOffset, mChunkedLen));
            mInOffset  += (mChunkedLen + 2);

            switch (mInOffset - mLength) {
            case 2:  mSkip = 2; break;
            case 1:  mSkip = 1; break;
            default: mSkip = 0;
            }

            if (mDebug || mSkip) {
                qDebug() << "getChunkedWhole " << mInOffset << mLength << mChunkedLen << mSkip;
                //mDebug = true;
            }

            mChunkedLen = -1;
            if (mInOffset >= mLength)
                break;
            continue;
        }
        //qDebug() << "Part";
        mBuffer.append(nData.mid(mInOffset, remLength));
        mChunkedLen -= remLength;
        break;
    }
    return false;
}

void ReplyProcessor::getNameValue(QByteArray & line, QByteArray & name, QByteArray & value) {
    name.clear();
    value.clear();
    QByteArray *bap = &name;

    for (int i=0; i < line.length(); i++) {
        switch(line[i]) {
        case ':':
            i++;
            bap = &value;
            continue;
        default:
            bap->append(line[i]);
            continue;
        }
        break;
    }
    name = name.toLower();
    if (mDebug) qDebug() << "getNameValue" << name << value;
}

bool ReplyProcessor::getData(QByteArray & nData) {
    mBuffer.append(nData);
    // check for end of message.
    return false;
}

QByteArray ReplyProcessor::getAttr(const char * attr, QByteArray & value) {
    QByteArray res;
    int alen = strlen(attr) + 2;

    QList<QByteArray> attrs = value.split(' ');
    QListIterator<QByteArray> i(attrs);

    while(i.hasNext()) {
        QByteArray ba = i.next();
        if (ba.startsWith(attr)) {
            res = ba.mid(alen, ba.length() - alen - 1);
            break;
        }
    }
    qDebug() << "getAttr" << attr << res;
    return res;
}

bool ReplyProcessor::getLine(QByteArray & nData, QByteArray & line) {
    line.clear();
    int i;
    for (i=mInOffset; i < nData.length(); i++) {
        switch(nData[i]) {
        case '\n':
            break;
        case '\r':
            continue;
        default:
            line.append(nData[i]);
            continue;
        }
        break;
    }
    mInOffset = i + 1;
    return (line.isEmpty()) ? false : true;
}

int ReplyProcessor::getLineGen(QByteArray & data, int offset, QByteArray & line) {
    line.clear();
    int i;

    if (offset >= data.length()) {
        qDebug() << "######################## getLineGen:" << offset << data.length();
    }

    for (i=offset; i < data.length(); i++) {
        // 8 bit characters;
        if (data[i] & 0x80) {
            switch(data[i] & 0xff) {
            case 0xdf:
                line.append('S');
                line.append('S');
                continue;
            case 0xc4:
                line.append('A');
                line.append('E');
                continue;
            case 0xe4:
                line.append('a');
                line.append('e');
                continue;
            case 0xd6:
                line.append('O');
                line.append('E');
                continue;
            case 0xf6:
                line.append('o');
                line.append('e');
                continue;
            case 0xdc:
                line.append('U');
                line.append('E');
                continue;
            case 0xfc:
                line.append('u');
                line.append('e');
                continue;
            }
            break;
        }
        switch(data[i]) {
        case '\n':
            break;
        case '\r':
            continue;
        default:
            line.append(data[i]);
            continue;
        }
        break;
    }
    offset = i + 1;
    return offset;
}

QString ReplyProcessor::getValue(const QString & name) {
    return mNameVal[name];
}
