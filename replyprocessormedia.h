#ifndef REPLYPROCESSORMEDIA_H
#define REPLYPROCESSORMEDIA_H
#include "replyprocessor.h"

class ReplyProcessorMedia : public ReplyProcessor
{
public:
    ReplyProcessorMedia();
    bool addData(QByteArray & nData);
private:
    int getMessageInfo();
};

#endif // REPLYPROCESSORMEDIA_H
