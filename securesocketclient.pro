HEADERS   += certificateinfo.h \
              sslclient.h \
    hide.h
SOURCES   += certificateinfo.cpp \
              main.cpp \
              sslclient.cpp \
    hide.cpp
#RESOURCES += securesocketclient.qrc
FORMS     += certificateinfo.ui \
              sslclient.ui \
              sslerrors.ui \
    sslerrors.ui
QT        += network

 # install
target.path = /Users/kevin/Oldhome/DataLoader/qt/ssl
sources.files = $$SOURCES $$HEADERS $$RESOURCES $$FORMS *.pro *.png *.jpg images
sources.path = /Users/kevin/Oldhome/DataLoader/qt/ssl
INSTALLS += target sources

#
