#include "webconnect.h"
#include <typeinfo>
#include <QDebug>
#include <QTcpSocket>
#include <QSslSocket>
#include <QAbstractSocket>

WebConnect::WebConnect(QObject *parent) : QObject (parent)
{

}

void WebConnect::Test() {
    socket = new QTcpSocket(this);

    connect(socket, SIGNAL(connected()),                this, SLOT(connected()));
    connect(socket, SIGNAL(disconnected()),             this, SLOT(disconnected()));
    connect(socket, SIGNAL(bytesWritten(qint64)),       this, SLOT(bytesWritten(qint64)));
    connect(socket, SIGNAL(readyRead()),                this, SLOT(readyRead()));

    socket->connectToHost("192.168.1.20", 10000);
    if (!socket->waitForConnected(3000)) {
        qDebug() << "Connection failed";
    }
}

void	WebConnect::connected() {
    qDebug() << "Connected!";
    socket->write("getmeta\n");
}

void	WebConnect::disconnected() {
    qDebug() << "Disconnected!";
}

void	WebConnect::error(QAbstractSocket::SocketError) {

}

void	WebConnect::stateChanged(QAbstractSocket::SocketState) {

}

void	WebConnect::aboutToClose() {

}

void	WebConnect::bytesWritten ( qint64 bytes) {
    qDebug() << "bytesWritten" << bytes;
}

void	WebConnect::readChannelFinished() {

}

void	WebConnect::readyRead() {
    qDebug() << "bytes read:";
    qDebug() << socket->readAll();
}

