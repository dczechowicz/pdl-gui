// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "./Components"

Page {
  id: container

  signal selectData( string name, string selected)

  function setData(datatype, value)
  {
    if (datatype == "Airline")
    {
      bsAirline.value = value;
    }
    else if (datatype == "ACType")
    {
      bsACType.value = value;
    }
    else if (datatype == "Tailsign")
    {
      bsTailsign.value = value;
    }
    else if (datatype == "HWFIN")
    {
      bsHWFin.value = value;
    }
    else if (datatype == "Software")
    {
      bsSoftware.value = value;
    }
  }

  Rectangle {
    x: 40
    y: 1

    height: childrenRect.height

    Column {
        HeaderText {
            text: "Select Software"
        }

        Rectangle {
          radius: 10
          border.width: 2
          border.color: "gray"

          width: 400
          height: 82 + 36 + 36 + 36

          color: "lightgray"
          Column {
            anchors.fill: parent;
            anchors.margins: 5
            spacing: 1

            MenuItem {
              id: bsAirline
              text: "Airline"
              value: ""

              onClicked: container.selectData("Airline", value);
            }

            MenuItem {
              id: bsACType
              text: "A/C Type"
              value: ""

              onClicked: container.selectData("ACType", value);
            }

            MenuItem {
              id: bsTailsign
              text: "Tailsign"
              value: ""

              onClicked: container.selectData("Tailsign", value);
            }

            MenuItem {
              id: bsHWFin;
              text: "HW FIN";
              value: ""

              onClicked: container.selectData("HWFIN", value);
            }

            MenuItem {
              id: bsSoftware;
              text: "Software part number";
              value: ""

              onClicked: container.selectData("Software", value);
            }
          }
        }

        HeaderText {
            text: "Secure USB"
        }

        Rectangle {
          radius: 10
          border.width: 2
          border.color: "gray"

          width: 400
          height: 82 + 36 + 36

          color: "lightgray"
          Column {
            anchors.fill: parent;
            anchors.margins: 5
            spacing: 1

            ValueItem {
              text: "Status"
              value: "PROG"
            }
            ValueItem {
              text: "Start time";
              value: "12:12:12"
            }
            ValueItem {
              text: "Remaining time (Est)"
              value: "00:03:00"
            }
            ValueItem {
              text: "Progress"
              value: "25%"
            }
          }
        }
        Rectangle {
            color: "#AAAAAA"
            height: 5
            width: 10
        }

        Button {
          text: "Prepare Secure USB"
        }
    }
  }
}
