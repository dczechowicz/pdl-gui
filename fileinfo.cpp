
#include "fileinfo.h"

FileInfo::FileInfo(QObject *parent) :
    QObject(parent)
{
    setPath(MACDIR, false);
    setPath(LINDIR, false);
    setPath(PDLDIR, true);
    QDir t(getTempDir());
    if (!t.exists()) {
        t.mkpath(getTempDir());
    }

}

bool FileInfo::setPath(const QString &path, bool pdl) {
    QDir p(path);
    if (!p.exists())
        return false;
    mPath  = path;
    mOnPDL = pdl;
    return true;
}

int FileInfo::getPort() {
    QFile f(mPath + "/.port");
    int port = 80;
    if (!f.exists())
        return port;

    if (!f.open(QIODevice::ReadOnly))  {
        return port;
    }

    QByteArray line = f.readLine();
    QByteArray v;

    for (int i=0; i < line.size(); i++) {
        if (line[i] >= '0' and line[i] <= '9') {
            v.append(line[i]);
            continue;
        }
        break;
    }
    qDebug() << "v" << v;
    port = v.toInt();

    f.close();

    qDebug() << "PORT" << port << v;
    return port;
}
