/****************************************************************************
 **
 ** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: http://www.qt-project.org/
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
 **     the names of its contributors may be used to endorse or promote
 **     products derived from this software without specific prior written
 **     permission.
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 **
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#ifndef ZXXXSSLCLIENT_H
#define ZXXXSSLCLIENT_H

#include <QtGui/QWidget>
#include <QtNetwork/QAbstractSocket>
#include <QtNetwork/QSslSocket>
#include <QProcess>
#include <QRegExp>
#include <QTimer>
#include "fileinfo.h"
#include "replyprocessormeta.h"
#include "replyprocessormedia.h"
#include "replyprocessorlogin.h"
#include "replyprocessorsoftware.h"
#include "sslclientbase.h"
#include "syncthread.h"

typedef enum {
    SYNCST_SYNCHRONISING,
    SYNCST_RETRIEVE_META_COMPLETE,
    SYNCST_UPLOADING,
    SYNCST_COMPLETE,
    SYNCST_ABORTED,
    SYNCST_FAILED,
    SYNCST_TIMEOUT,
    SYNCST_NETWORK_SETUP,
    SYNCST_LOGIN,
    SYNCST_RETRIEVE_META,
    SYNCST_PROC_META,
    SYNCST_DOWNLOADING,
} syncstates_t;

typedef enum {
    SYNC_NULL,
    SYNC_SETUPCOMM = 1,
    SYNC_LOGIN,
    SYNC_POSTLOGIN,
    SYNC_SENDCLINFO,
    SYNC_PROCCLINFO,
    SYNC_SENDGETMETA,
    SYNC_PROCGETMETA,
    SYNC_EXTRACTMETA,
    SYNC_GETNEXTPARTNUM,
    SYNC_RETRYPARTNUM,
    SYNC_EXTRACTDISK,
    SYNC_ISSORTWARE,
    SYNC_ISSORTWAREPROC,
    SYNC_GETSORTWARE,
    SYNC_GETSORTWAREPROC,
    UPLD_STARTUPLOAD = 30,
    UPLD_SETUPLOADED,
    SYNC_PROCESSLOGOFF = 90,
    SYNC_POWERLOSS     = 99
} sync_types_t;

#define SYNC_MAXFAILCOUNT 10

typedef enum {
    COM_SYNC,
    COM_SWUPDATE
} commode_t;

#define DOWNLOAD_INTV 25
#define DOWNLOAD_TIME 5000

class SslClient : public SslClientBase
{
    Q_OBJECT

public:
    SslClient(QObject *parent = 0);
    ~SslClient();

    void setThread(SyncThread * thread) {
        mSyncThread = thread;
    }

public slots:
    void synchroniseWithServer();
    void setSyncStatus(syncstates_t status);

private slots:
    void startSynchronisation();
    void startSynchronisationLogin();

signals:
    void reload();
    void startSync();
    void addLog(QString text);
    void processMessage();
    void updateElapsed();
    void setUpdateStatus(QString status);
    void setUpdateProgress(QString progress);
    void setUpdateStartTime(QString time);
    void reloadData();
    void setConnectionType(QString progress);
    void setUploadProgress(QString progress);
    void setUploadStartTime(QString progress);
    void setUpdating(bool updating);
    void setLastSync(int);
    void checkPN(QString pn);
    void startLogin();

public slots:
    void PNOk(QString pn, bool pnok);

private slots:
    void handleMessage();
    void socketStateChanged(QAbstractSocket::SocketState state);
    void socketEncrypted();
    void socketReadyRead();
    void readFromStdOut();
    void finishedApp();
    void timeout();
    void setConfigValue(const QString a, const QString b);

private:
    void init();
    ReplyProcessor *mpReply;
    void setReplyProcessor(ReplyProcessor *rp);

    void setSystemDateTime(qint32 time);
    SyncThread *mSyncThread;
    void syncComplete();
    QStringList mSyncStatii;
    QStringList mSyncLogmsg;
    bool mTimerActive;
    void enableTimer();
    void disableTimer();
    void startTimer();
    void stopTimer();
    QString calcPercent(int n, int from);
    bool isEndOfMessage();
    QRegExp mEndOfMessage;
    QRegExp mDatePattern;
    QRegExp mDatePatternMs;
    QProcess *process;
    QByteArray mReply;
    QString mXOPInc;
    QString mPartnumber;
    QString script;
    QString mParts;

    QString mSWPath;
    int mPartIdx;
    bool mNewSoftware;
    QString mSWCheckSum;

    QTimer timer;
    bool mRequestOpen;
    int mDelayCount;
    int mDiskExp;
    int mDiskAct;
    int mPerBase;
    int mSequence;
    int diskcount;
    bool mUp2Date;
    QString mLastChange;
    commode_t commode;

    void startSWUpdate();
    void stopSync(QString logmsg, QString status);
    void setProgress();

    qint64 getUTC(QString datetime);
    bool executingDialog;
    void procMessage();
    void procMessage_sync();
    void procMessage_swupdate();
    void saveReply();
    void runCommand(QString command);
    bool getNextUpload();
    bool getNextPartNumber();
    bool extractPartnumbers();
    void checkPartNumber(QString pnfs);
};

#endif
