import QtQuick 1.1

Item {
    id: root

    signal clicked()

    MouseArea {
        anchors.fill: parent

        onClicked: backend.switchScreen(true)
    }
}
