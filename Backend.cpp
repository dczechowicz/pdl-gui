#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include <QDeclarativeContext>
#include <QtDeclarative>
#include "Backend.h"
#include <QApplication>
#include <QFile>
#include <QDateTime>
#include <QDebug>
#include <QThread>
#include <QXmlStreamReader>
#include "fileinfo.h"
#include "config.h"
#include <QTimer>
#include <QMessageBox>
#include "dbsettings.h"
#include "./helpers/confighelper.h"
#include "replyprocessor.h"
#include "mediainfo.h"
#include "checkrepository.h"
#include "modaldialog.h"
#include "syncstarter.h"
#include "syncthread.h"
#include "build_number.h"
#include "checkrepository.h"

#ifdef Q_WS_WIN
#include <windows.h>
#include <winbase.h>
#endif

/* Global values used in GUI */
const bool g_ShowAdministration = false;
extern QmlApplicationViewer *gViewer;

Backend::Backend(QObject *parent) :
    QObject(parent)
{
}

void Backend::WatchInit()
{
    gBackend = this;
    mDebugSetting = 0;
    mMonServer = NULL;

    mShowLogWindow = false;
    mProcess = NULL;
    mNetConfigServer = NULL;
}

void Backend::Init()
{
    qRegisterMetaType<syncstates_t>("syncstates_t");

    gBackend = this;
    mDebugSetting = 0;
    mMonServer = NULL;
    mAirlineModel  = new QStringListModel(this);
    mAcTypeModel   = new QStringListModel(this);
    mTailsignModel = new QStringListModel(this);
    mHwFinModel    = new QStringListModel(this);
    mSoftwareModel = new QStringListModel(this);

    mStatus = "RDY";
    mStart = "";
    mRemainingTime = "";
    mElapsedTime = "";
    mConnectionType = "";
    mProgress = "";
    mSyncAllowed = true;
    mShowLogWindow = false;
    mUsbButtonEnabled = false;
    mHomeEnabled = true;
    mPower = true;
    mLogText = "";
    mRunning = false;
    mUpdating = false;
    mInAbort = false;
    mCurrPage = "Administration";
    mProcess = NULL;
    mNetConfigServer = NULL;
    mTailVal = SWPN_NOTSET;
    mSwpnVal = SWPN_NOTSET;
    mPreparing = false;
    mSyncTimerExc = 0;

    setShowUSBAbort(false);
    setUSBButtonText("Enable Secure USB");
    mUSBState = USB_PREPARE;

    mColors.append("white");
    mColors.append("red");
    mColors.append("yellow");
    mColors.append("lightgreen");
    mColors.append("blue");

    mColHash.insert("COMP",  COL_BLUE);
    mColHash.insert("ABORT", COL_RED);
    mColHash.insert("HRDW",  COL_RED);
    mColHash.insert("R/W",   COL_RED);
    mColHash.insert("XREF",  COL_RED);
    setStatusColor("blue");

    mRETailSignWhole.setPattern("^[A-Z][A-Z0-9-/]*");
    mRETailSignStart.setPattern("^[A-Z]");
    mNetConfigServer = NULL;
    creatMonitorSocket();
    startNetConfigServer();

    connect(&mRunClient,              SIGNAL(setStatus(QString)),            this, SLOT(setStatus(QString)));
    connect(&mRunClient,              SIGNAL(commandResponse(int, QString)), this, SLOT(commandResponse(int, QString)));
    connect(&mRunClient,              SIGNAL(commandOutput(QString)),        this, SLOT(commandOutput(QString)));

    connect(&mTimer, SIGNAL(timeout()), this, SLOT(timeout()));
    mTimer.start(1000);

    connect(&mTimerSync, SIGNAL(timeout()), this, SLOT(startAutoSync()));

    mSyncTimer = mFi.getAutoSync();

    if (mSyncTimer) {
        mTimerSync.start(mSyncTimer);
    }

    switchScreen(true);

    createSslThread();

    QString buildinfo;
    buildinfo  += BUILDDATE;
    buildinfo  += " ";
    buildinfo  += BUILDTIME;
    buildinfo  += " (";
    buildinfo  += BUILD;
    buildinfo  += ")";
    setBuildInfo(buildinfo);

    qDebug() << "Build info" << BUILD << BUILDDATE << BUILDTIME;

    connect(ConfigHelper::instance(), SIGNAL(reply(QString, QString)),       this, SLOT(setConfigValue(QString, QString)));
    ConfigHelper::instance()->execute("C GetAccessKey");
    ConfigHelper::instance()->execute("C GetDemoMode");
}

void Backend::setConfigValue(const QString a, const QString b) {
    qDebug() << "Backend::setConfigValue" << a << b;
    if (a == "GetAccessKey") {
        mAccessKey=b;
    }
    if (a == "GetDemoMode") {
        mDemoPDLMode=b.toInt();
        emit demoPDLModeChanged();
    }
}

QStringListModel *Backend::airlineModel()
{
    return mAirlineModel;
}

QStringListModel *Backend::actypeModel()
{
    return mAcTypeModel;
}

QStringListModel *Backend::tailsignModel()
{
    return mTailsignModel;
}

QStringListModel *Backend::softwareModel()
{
    return mSoftwareModel;
}

void Backend::setAirline(QString name)
{
    mAirline = name;
    mAirlineKey = Airlines.key(name);
    if (!name.isEmpty()) qDebug() << "setAirLine:" + name;

    setACType(mEmpty);

    mAcTypeModel->setStringList( Aircraft_Type[mAirlineKey] );
    mAcTypeModel->sort(0);

    emit airlineChanged();
}

void Backend::setACType(QString name)
{
    mACType = name;

    setTailsign(mEmpty);
    mTailsignModel->setStringList( Aircraft_Tailsign[mAirlineKey + "-" + name] );
    mHwFinModel->setStringList( Type_HwFin[mACType] );
    mTailsignModel->sort(0);
    mHwFinModel->sort(0);

    emit actypeChanged();
}

void Backend::setTailsign(QString name)
{
    mTailsign = name;

    setAtaChapter(mEmpty);

    emit tailsignChanged();
}

void Backend::setAtaChapter(QString name)
{
    mAtaChapter = name;
    setAtaSection(mEmpty);

    emit ataChapterChanged();
}

void Backend::setAtaSection(QString name)
{
       mAtaSection = name;
       setSoftware(mEmpty);

       emit ataSectionChanged();
}

void Backend::setSoftware(QString name)
{
    mSoftware = name;
    resetSome();
    if (name.isEmpty()) {
        setSoftwareTitle(mEmpty);
        setSoftwareCoC(mEmpty);
        setRemainingTime(mEmpty);
    } else {
        qDebug() << "setSoftware" << name;
        setSoftwareTitle(db.getSoftwareTitle(name));
        setSoftwareCoC(db.getCocFormIssuer(name));
        setDisk(db.getMediaCount(name));
        setRemainingTime(db.getEstimateLoadTime(name));
        setUsbButtonEnabled(true);
    }

    emit softwareChanged();
}

void Backend::setSoftwareTitle(QString name)
{
    //qDebug() << "setSoftwareTitle" << name;

    mSoftwareTitle = name.left(32);
    emit softwareTitleChanged();
}

void Backend::setSoftwareCoC(QString name)
{
    //qDebug() << "setSoftwareCoC" << name;
    mSoftwareCoC = name;
    emit softwareCocChanged();
}

void Backend::setStatus(QString status)
{
    mStatus = status;
    setStatusColor(getStatusColorString());
    emit statusChanged();
}

void Backend::setStatusColor(QString color)
{
    mStatusColor = color;
    emit statusColorChanged();
}

void Backend::setBuildInfo(QString info)
{
    mBuildInfo = info;
    emit buildInfoChanged();
}

void Backend::setUSBButtonText(QString text)
{
    mUSBButtonText = text;
    emit usbButtonTextChanged();
}

void Backend::setPdlName(QString status)
{
    mPdlName = status;
    emit pdlNameChanged();
}

void Backend::setLastSync(int tm)
{
    QDateTime dt;
    qDebug() << "setLastSync" << tm;
    getSettingsDB().set_update_timestamp(tm);
    dt.setTime_t(tm);
    mLastSync = dt.toString("yyyy-MM-dd hh:mm");
    emit lastSyncChanged();
}

void Backend::setUpdateStatus(QString status)
{
    mUpdateStatus = status;
    emit updateStatusChanged();
}

void Backend::setUpdateProgress(QString progress)
{
    mUpdateProgress = progress;
    emit updateProgressChanged();
}

void Backend::setUploadStatus(QString status)
{
    mUploadStatus = status;
    emit uploadStatusChanged();
}

void Backend::setUpdateStartTime(QString time)
{
    mUpdateStartTime = time;
    emit updateStartTimeChanged();
}

void Backend::setPage(QString page)
{
    mCurrPage = page;
    mPages.append(mCurrPage);
    emit pageChanged();
}

void Backend::writeStringToFile(const char * file, const char *data) {
    QFile f(file);
    if (!f.open(QIODevice::WriteOnly)) {
        qDebug() << "Can't open:" << file;
        return;
    }
    f.write(data, strlen(data));
    f.close();
}

void Backend::turnOffScreen() {
    emit switchScreen(false);
}

void Backend::switchScreen(bool onoff) {
    qDebug() << "########## switchScreen:" << onoff;
    QDir d("/sys/class/gpio/gpio13");
    if (!d.exists()) {
        writeStringToFile("/sys/class/gpio/export", "13");
    }
    writeStringToFile("/sys/class/gpio/gpio13/direction", (onoff) ? "in\n" : "out\n");

    emit screenStateChanged(onoff);
}

void Backend::appTimeout() {
    qDebug() << "appTimeout";
    mApp->exit(0);
}

void Backend::appRestart(QApplication *app)
{
    mApp = app;
    qDebug() << "appRestart";
    QTimer::singleShot(10000, this, SLOT(appTimeout()));
}

void Backend::setPower(bool power)
{
    mPower = power;
    switch(mPower) {
    case true:
        sleep(1);
        gViewer->show();
        break;
    case false:
        gViewer->hide();
        break;
    }
}

bool Backend::isPower()
{
    return mPower;
}

void Backend::popPage(bool Home)
{
    if (Home || mPages.isEmpty()) {
        mCurrPage = "Home";
        mPages.clear();
    } else {
        mCurrPage = mPages.takeLast();
    }
    emit pageChanged();
}


void Backend::timeout()
{
    updateElapsed();
}

void Backend::updateElapsed()
{
    QTime x;

    if (!mUpdating)
        return;

    int elapsed = mElapseTimer.elapsed();
    x = x.addMSecs(elapsed);

    QString e = x.toString("mm:ss");
    qDebug() << "updateElapsed" << e << elapsed;
    setElapsedTime(e);
}

void Backend::updateConnectionType()
{
}

// Start the synchronisation
void Backend::startUpdate()
{
    QMutexLocker l(&mMutex);

    if (mInAbort) {
        return;
    }
    if (mUpdating)
        return;
    //*((char *)0) = 0;
    resetSome();
    setShowLogWindow(true);

    setUpdateStartTime(QTime::currentTime().toString("HH:mm:ss"));
    mElapseTimer.start();
    setElapsedTime("00:00");
    setUpdateProgress("0%");
    setConnectionType("");

    QTimer::singleShot(100, this, SLOT(startSynchroniseWithServer()));
    setUpdating(true);
    setHomeEnabled(false);
    gBackend->mFi.setVerified(false);
    mFi.inSync(true);
}

void Backend::stopUpdate()
{
    QMutexLocker l(&mMutex);
    if (mInAbort) {
        return;
    }
    mInAbort = true;

    if (!mUpdating)
        return;

    qDebug() << "Stop Update Begin";
    mFi.inSync(false);
    setUpdateStatus("ABORTED");
    setHomeEnabled(true);
    setUploadStatus("X"); //KMU
    emit setSyncStatus(SYNCST_ABORTED);
    ConfigHelper::instance()->execute("C GetRepositoryInfo");
    qDebug() << "Stop Update End";
}

void Backend::reloadData()
{
    qDebug() << "reloadData:in";
    getDB().loadDB(true);
    getDB().verifySWPNs(true);
    getSettingsDB().openDB();
    getSettingsDB().loadDB();
    qDebug() << "reloadData:out";
}

void Backend::runCommand(QString command)
{
    if (mProcess)
        mProcess->deleteLater();

    mProcess = new QProcess(this);

    /* Process start will start the app in this object */
    qDebug() << "Running:" + command;
    mProcess->start(command);
}

void Backend::clearAuto()
{
    qDebug() << "clearAuto";
    mSyncTimer = 0;
    mTimerSync.stop();
    resetAll();
}

void Backend::resetAll()
{
    setAirline("");
    setACType("");
    setTailsign("");
    setAtaChapter("");
    setAtaSection("");
    setSoftware("");
    resetSome();
}

void Backend::resetSome()
{
    clearLog();
    setShowLogWindow(false);
    setUpdateProgress("");
    setUpdateStatus("");
    setStatus("");
    setUpdateStartTime("");
    setUploadStatus("");
    setUpdating(false);
    setRemainingTime("");
    setElapsedTime("");
    setLDR("");
    setLRU("");
    setTx("");
    setRx("");
     setDis("");
     setLast("");
     setReqs("");
     setMode("");
     setTotBlocks("");
     setDisk("");
     setDisks("");
     setFile("");
     setBlock("");
     setBlocks("");
     setI("");
     setN("");
     setL("");
     setB("");

}

void Backend::quit()
{
    qDebug() << "quit";
    QString t = mFi.getUtilName() + "-s";
    runCommand(t);
}

QString Backend::doCheckTail(QString in) {
    //qDebug() << "doCheckTail" << in;

    setTailsign(in);
    if (in.isEmpty()) {
        mTailVal = SWPN_NOTSET;
        checkManualComplete();
        return in;
    }

    if (in.length() > 0) {
        //qDebug() << "doCheckTailMatch";
        mTailVal = SWPN_MATCH;
        checkManualComplete();
        return in;
    }
    if (in.length() == 1) {
        if (mRETailSignStart.exactMatch(in)) {
            qDebug() << "doCheckTailMatch";
            mTailVal = SWPN_VALID;
        } else {
            qDebug() << "doCheckTailValid";
            mTailVal = SWPN_NOK;
        }
        checkManualComplete();
        return in;
    }

    if (mRETailSignWhole.exactMatch(in)) {
        if (in.length() == 6) {
            qDebug() << "doCheckTailMatch";
            mTailVal = SWPN_MATCH;
        } else {
            qDebug() << "doCheckTailValid";
            mTailVal = SWPN_VALID;
        }
    } else {
        qDebug() << "doCheckTailNoMatch";
        mTailVal = SWPN_NOK;
    }
    checkManualComplete();
    return in;
}

void Backend::checkManualComplete() {
    if (mSwpnVal == SWPN_MATCH && mTailVal == SWPN_MATCH) {
        setSoftware(mManSoftware);
    } else {
        setSoftware("");
    }
}

QString Backend::doCheckSwpn(QString in) {
    //qDebug() << "doCheckSwpn" << in;
    mManSoftware = in;
    setSoftwareCoC(db.getCocFormIssuer(in));
    setSoftwareTitle(db.getSoftwareTitle(in));
    if (in.isEmpty()) {
        mSwpnVal = SWPN_NOTSET;
        checkManualComplete();
        return in;
    }
    in = in.toUpper();
    QStringListIterator qit(db.getSwpnList());
    while (qit.hasNext()) {
        QString qs = qit.next();
        //qDebug() << "look swpn" << qs;
        if (qs == in) {
            qDebug() << "doCheckSwpn:match" << qs;
            mSwpnVal = SWPN_MATCH;
            setSoftwareCoC(db.getCocFormIssuer(in));
            setSoftwareTitle(db.getSoftwareTitle(in));
            checkManualComplete();
            return in;
        }
        if (qs.startsWith(in)) {
            qDebug() << "doCheckSwpn:valid" << qs;
            mSwpnVal = SWPN_VALID;
            checkManualComplete();
            return in;
        }
    }
    qDebug() << "doCheckSwpn:invalid";
    mSwpnVal = SWPN_NOK;
    checkManualComplete();
    return in;
}

void Backend::checkTail()
{
    qDebug() << "checkTail";
}

void Backend::checkSwpn()
{
    qDebug() << "checkSwpn";
}

void Backend::restart()
{
    qDebug() << "Restart" << mRunClient.getState();
    switch (mRunClient.getState()) {
    case RUN_IDLE:
        mRunClient.doRun("/home/kevin/test.sh");
        break;
    case RUN_STARTUP:
        break;
    case RUN_RUNNING:
        mRunClient.stopCommand();
        break;
    }

}

#if 0
    modalDialog *dialog = new modalDialog( );
    dialog->exec();
    return;
    QByteArray xml;
    QString filename = "/Users/kevin/Oldhome/Dataloader/development/delivery/home/root/loader/tmpdir/test.meta";
    QFile f(filename);

    qDebug() << "Opening" << filename;

    if (!f.open(QIODevice::ReadOnly))  {
        qDebug() << "Failed to open file.";
        return;
    }
    xml = f.readAll();

    f.close();

    mediaInfo mi;
    mi.dumpInfo(xml);
    qDebug() << "shutdown";
    //mi.buildInfoFile("/Users/kevin/Oldhome/Dataloader/development/delivery/home/root/loader/Repository/SW/ZZZTEST");
    return;
    ReplyProcessor rp;
    rp.setDirectory(mFi.getRepositorySWDir(), "ZZZTEST1");
    //rp.testFromFile("/Users/kevin/Oldhome/Dataloader/development/delivery/home/root/loader/tmpdir/0724165332_meta_copy");
    rp.testFromFile("/tmp/dumpmetatest.orig");

    return;

    CheckRepository cr;
    cr.checkAll(false);
    return;

    if (cr.checkPartNumber("D1046RR07C0LH02", true))
        qDebug() << "checkPartNumber return true";
    else
        qDebug() << "checkPartNumber return false";
    return;
#endif

void Backend::shutdown()
{
    qDebug() << "shutdown";
    ReplyProcessorMeta rp;
    rp.testFromFile("/Users/kevin/metadataproblem_reply");

    return;

    QString t = mFi.getUtilName() + "-s";
    runCommand(t);
}

void Backend::createSslThread()
{
    mSslThread      = new SyncThread;
    SslClient *mSsl = new SslClient();
    mSsl->setThread(mSslThread);
    mSsl->moveToThread(mSslThread);


    connect(mSsl,    SIGNAL(addLog(QString)),                     this,      SLOT(addLog(QString)),                    Qt::QueuedConnection);
    connect(mSsl,    SIGNAL(reload()),                            this,      SLOT(reloadData()),                       Qt::QueuedConnection);
    connect(mSsl,    SIGNAL(setLastSync(int)),                    this,      SLOT(setLastSync(int)),                   Qt::QueuedConnection);
    connect(mSsl,    SIGNAL(setUpdateProgress(QString)),          this,      SLOT(setUpdateProgress(QString)),         Qt::QueuedConnection);
    connect(mSsl,    SIGNAL(setUpdating(bool)),                   this,      SLOT(setUpdating(bool)),                  Qt::QueuedConnection);
    connect(mSsl,    SIGNAL(setConnectionType(QString)),          this,      SLOT(setConnectionType(QString)),         Qt::QueuedConnection);
    connect(mSsl,    SIGNAL(updateElapsed()),                     this,      SLOT(updateElapsed()),                    Qt::QueuedConnection);

    connect(this,    SIGNAL(synchroniseWithServer()),             mSsl,      SLOT(synchroniseWithServer()),            Qt::QueuedConnection);
    connect(this,    SIGNAL(setSyncStatus(syncstates_t)),         mSsl,      SLOT(setSyncStatus(syncstates_t)), Qt::QueuedConnection);

    mSslThread->start();
}

void Backend::createThread()
{
    mThread = new QThread;
    mWorker = new BThread();
    mWorker->moveToThread(mThread);

    connect(mWorker, SIGNAL(error(QString)),        this,      SLOT(errorString(QString)));
    connect(mThread, SIGNAL(started()),             mWorker,   SLOT(process()));
    connect(this,    SIGNAL(sendAString(QString)),  mWorker,   SLOT(receiveString(QString)), Qt::QueuedConnection);
#if 0
    connect(worker, SIGNAL(error(QString)), this,     SLOT(errorString(QString)));
    connect(thread, SIGNAL(started()),      worker,   SLOT(process()));
    connect(worker, SIGNAL(finished()),     thread,   SLOT(quit()));
    connect(worker, SIGNAL(finished()),     worker,   SLOT(deleteLater()));
    connect(thread, SIGNAL(finished()),     thread,   SLOT(deleteLater()));
#endif
    mThread->start();

    emit sendAString((QString)"gift");
}

void Backend::connRemoteSuport()
{
    qDebug() << "connRemoteSuport";
    QString t = mFi.getUtilName() + "-c";
    runCommand(t);
}

void Backend::purgeRepository()
{
    QString proc = mFi.getScriptName() + "-P";
    int ret = QProcess::execute(proc);
    qDebug() << "purgeRepository" << proc << ret;
    reloadData();
    // reset change date.
    getSettingsDB().set_db_pdl_change_time(QString("2000-01-01T00:00:00+00:00"));

    ConfigHelper::instance()->execute("C GetRepositoryInfo");
}

void Backend::restartGUI()
{
    // restart GUI only on PDL
    qDebug() << "restartGUI";
    QString t = mFi.getUtilName() + "-g";
    runCommand(t);
}

void Backend::stopDataLoad()
{
    if (!mRunning)
        return;

    qDebug() << "STOP DATA LOAD()";
    mStatus = "ABORT";
    addLog("Completed");
    setShowLogWindow(false);
    mFi.inLoad(false);
    setRunning(false);
    load.send2Loader("ABORT");

    setHomeEnabled(true);
    if (mFi.isDemoMode()) {
        restartGUI();
    }

    emit statusChanged();
    emit startTimeChanged();
    emit remainTimeChanged();
    emit elapsedTimeChanged();
    emit progressChanged();
}

void Backend::startSync()
{
    qDebug() << "startSync called";
//    startUpload();
}

void Backend::creatMonitorSocket() {
    if (mMonServer) {
        return;
    }
    mMonServer = new MonitorServer();
    bool success = mMonServer->listen(QHostAddress::Any, 4200);
    if(!success) {
        gBackend->addLog("Could not listen on port 4200.");
        //qFatal("Could not listen on port 4200.");
    }

    qDebug() << "creatMonitorSocket";

}

void Backend::dataLoadStopped()
{
    qDebug() << "Dataload stopped";
    addLog("Completed");
    mFi.inLoad(false);
    setRunning(false);
    load.send2Loader("ABORT");

    setHomeEnabled(true);

    if (mFi.isDemoMode()) {
        restartGUI();
    }

    emit statusChanged();
    emit startTimeChanged();
    emit remainTimeChanged();
    emit elapsedTimeChanged();
    emit progressChanged();
}

void Backend::commandResponse(int retcode, QString output) {
    qDebug() << "commandResponse" << "retcode" << retcode << "output" << output;
    emit prepareUSB();
}

void Backend::commandOutput(QString output) {
    qDebug() << "commandOutput" << "output" << output;
}

#ifdef Q_WS_MAC
#define USBTEST
#endif

void Backend::resetUSB(usb_reset_t state)
{
}

void Backend::onAbortUSB()
{
    qDebug() << "onAbortUSB";
    mUSBState = USB_ABORT;
    prepareUSB();
}

void Backend::prepareUSB()
{
    QString mess;
    QString t;
    bool ok;
    CheckRepository checkRep;

    qDebug() << "prepareUSB IN" << mUSBState;
    switch (mUSBState) {

    case USB_PREPARE:
        clearLog();
        setShowLogWindow(true);
        setShowUSBAbort(true);
        //setUsbButtonEnabled(false);
        setRunning(true);
        setHomeEnabled(false);
        setUSBButtonText("Abort");

        ok = checkRep.checkPartNumber4Load(mSoftware);
        mess = t.sprintf("%s Verification: %s", (const char*) mSoftware.toUtf8(), (ok) ? "OK" : "FAILED");
        addLog(mess);
        if (!ok) {
            addLog("Please abort data load and re-synchronise the PDL");
            break;
        }
        setStatus("Preparing Secure USB");
        addLog("Copying to secure USB");

#ifndef USBTEST
        //t = mFi.getUSBScript() + " -k \"" + mSoftware + "\"";
        t = mFi.getUSBScript() + " -a \"" + mTailsign + "\" -k \"" + mSoftware + "\"";
#else
        t = "/Users/kevin/usbtest prepare" " -a \"" + mTailsign + "\" -k \"" + mSoftware + "\"";
#endif

        qDebug() << "prepare USB" << t;
        mRunClient.doRun(t);
        mUSBState = USB_PREPARE_RESP;
        break;

    case USB_PREPARE_RESP:
        setShowUSBAbort(false);
        if (mRunClient.getExitStatus()) {
            qDebug() << "Process crashed";
            resetUSB(EXUSB_EXCEPTION);
            break;
        }
        if (mRunClient.getReturnCode()) {
            addLog("Copy failed");
            setStatus("Secure USB unavailable");
            resetUSB(EXUSB_ERROR);
            break;
        }
        addLog("Copy complete");
        mUSBState = USB_SECURE;

        setStatus("Secure USB ready");
        setUSBButtonText("Shutdown Secure USB");
        setUsbButtonEnabled(true);
        break;

    case USB_SECURE:
        setStatus("Verifying Data on Secure USB");
        addLog("Verifying Data on Secure USB");
#ifndef USBTEST
        t = mFi.getUSBScript() + " -s";
#else
        t = "/Users/kevin/usbtest secure";
#endif
        qDebug() << "secure USB" << t;
        mRunClient.doRun(t);
        mUSBState = USB_SECURE_RESP;
        setUSBButtonText("Abort");
        setUsbButtonEnabled(true);
        break;

    case USB_SECURE_RESP:
        if (mRunClient.getExitStatus()) {
            qDebug() << "Process crashed";
            resetUSB(EXUSB_EXCEPTION);
            break;
        }
        if (mRunClient.getReturnCode()) {
            addLog("Copy failed");
            setStatus("Secure USB unavailable");
            resetUSB(EXUSB_ERROR);
            break;
        }
        setShowUSBAbort(false);
        setUsbButtonEnabled(true);
        setUSBButtonText("Enable Secure USB");
        setHomeEnabled(true);
        setRunning(false);
        mUSBState = USB_PREPARE;
        break;

    case USB_ABORT:
        mRunClient.stopCommand();
#ifndef USBTEST
        t = mFi.getUSBScript() + " -A";
#else
        t = "/Users/kevin/usbtest abort";
#endif
        qDebug() << "secure USB" << t;
        mRunClient.doRun(t);
        mUSBState = USB_ABORT_RESP;
        break;

    case USB_ABORT_RESP:
        setShowLogWindow(false);
        setShowUSBAbort(false);
        setUSBButtonText("Enable Secure USB");
        setStatus("Aborted by user");
        setHomeEnabled(true);
        setRunning(false);
        mUSBState = USB_PREPARE;
        break;
    }
    qDebug() << "prepareUSB OUT" << mUSBState;
}

void Backend :: startDataLoad()
{
    if (mRunning)
        return;

    qDebug() << "START Load";
    mFi.inLoad(true);

    mStatus = "RDY";
    mStart = QTime::currentTime().toString("HH:mm:ss");
    mProgress = "0%";

    if (mFi.isDemoMode()) {
        setShowLogWindow(false);
    } else {
        setShowLogWindow(true);
    }
    clearLog();

    load.synchroniseWithServer(this);
    setRunning(true);

    setHomeEnabled(false);

    emit statusChanged();
    emit startTimeChanged();
    emit remainTimeChanged();
    emit elapsedTimeChanged();
    emit connectionTypeChanged();
    emit progressChanged();

}

QString Backend::getAirline()
{
    return mAirline;
}

QString Backend::getAcType()
{
    return mACType;
}

QString Backend::getTailsign()
{
    return mTailsign;
}

QString Backend::getAtaChapter()
{
    return mAtaChapter;
}

QString Backend::getAtaSection()
{
    return mAtaSection;
}

QString Backend::getHwFin()
{
    return mHWFin;
}

QString Backend::getSoftware()
{
    return mSoftware;
}

QString Backend::getSoftwareTitle()
{
    return mSoftwareTitle;
}

QString Backend::getSoftwareCoC()
{
    return mSoftwareCoC;
}

QString Backend::getPdlName()
{
    return mPdlName;
}

QString Backend::getLastSync()
{
    return mLastSync;
}

QString Backend::getStatus()
{
    return mStatus;
}

QString Backend::getStatusColor()
{
    return mStatusColor;
}

QString Backend::getBuildInfo()
{
    return mBuildInfo;
}

QString Backend::getStartTime()
{
    return mStart;
}

QString Backend::getLDR()
{
    return mLiLDR;
}


QString Backend::getLRU()
{
    return mLiLRU;
}

QString Backend::getTx()
{
    return mLiTx;
}

QString Backend::getRx()
{
    return mLiRx;
}

QString Backend::getDis()
{
    return mLiDis;
}

QString Backend::getLast()
{
    return mLiLast;
}

QString Backend::getReqs()
{
    return mLiReqs;
}

QString Backend::getMode()
{
    return mLiMode;
}

QString Backend::getTotBlocks()
{
    return mLiTotBlocks;
}

QString Backend::getDisk()
{
    return mLiDisk;
}

QString Backend::getDisks()
{
    return mLiDisks;
}

QString Backend::getFile()
{
    return mLiFile;
}

QString Backend::getBlock()
{
    return mLiBlock;
}

QString Backend::getBlocks()
{
    return mLiBlocks;
}

QString Backend::getI()
{
    return mLiI;
}

QString Backend::getN()
{
    return mLiN;
}

QString Backend::getL()
{
    return mLiL;
}

QString Backend::getB()
{
    return mLiB;
}

QString Backend::getRemainingTime()
{
    return mRemainingTime;
}

QString Backend::getElapsedTime()
{
    return mElapsedTime;
}

QString Backend::getConnectionType()
{
    return mConnectionType;
}

QString Backend::getProgress()
{
    return mProgress;
}

bool Backend::getShowLogWindow()
{
    return mShowLogWindow;
}

bool Backend::getShowUSBAbort()
{
    return mShowUSBAbort;
}

bool Backend::getUsbButtonEnabled()
{
    return mUsbButtonEnabled;
}

bool Backend::getHomeEnabled()
{
    return mHomeEnabled;
}

int Backend::getServerId()
{
    return mServerId;
}

QString Backend::getServerName()
{
    return mServerName;
}

bool Backend::getInstallRunning()
{
    return mRunning;
}

bool Backend::getUpdateRunning()
{
    return mUpdating;
}

bool Backend::getShowAdministration()
{
    return g_ShowAdministration;
}

QString Backend::getLogText()
{
    return mLogText;
}

QString Backend::getUpdateStatus()
{
    return mUpdateStatus;
}

bool Backend::getDemoPDLMode()
{
    return mDemoPDLMode;
}

QString Backend::getUpdateStartTime()
{
    return mUpdateStartTime;
}

QString Backend::getUpdateProgress()
{
    return mUpdateProgress;
}

QString Backend::getUploadStatus()
{
    return mUploadStatus;
}

void Backend::setShowLogWindow(bool showWindow)
{
    //qDebug() << "setShowLogWindow" << showWindow;
    mShowLogWindow = showWindow;
    clearLog();
    emit showLogWindowChanged();
}

void Backend::setShowUSBAbort(bool show)
{
    mShowUSBAbort = show;
    emit showUSBAbortChanged();
}

void Backend::setUsbButtonEnabled(bool enabled)
{
    mUsbButtonEnabled = enabled;
    emit showUsbButtonEnabled();
}

void Backend::setHomeEnabled(bool enable)
{
    mHomeEnabled = enable;
    emit showHomeEnabledChanged();
}

void Backend::setServerId(int serverId)
{
    mServerId = serverId;
    qDebug() << "setServerId" << serverId;
    QString name = "Unknown";
    switch(serverId) {
    case 1:
        name = "eESM Intra (Test)";
        break;
    case 2:
        name = "eESM Intra (Prod)";
        break;
    case 3:
        name = "eESM (Test)";
        break;
    case 4:
        name = "eESM (Prod)";
        break;
    case 5:
        name = "FLS-Desk (Test)";
        break;
    case 6:
        name = "FLS-Desk (Prod)";
        break;
    }
    setServerName(name);
    gConfig->setConfig((conf_type_t)serverId);
    mFi.setConnection(serverId);

    emit showServerIdChanged();
}

void Backend::setServerName(QString serverName)
{
    mServerName = serverName;
    qDebug() << "setServerName" << serverName;
    emit showServerNameChanged();
}

void Backend::setRunning(bool running)
{
    mRunning = running;
    emit installRunningChanged();
}

void Backend::setUpdating(bool updating)
{
    mUpdating = updating;
    mInAbort  = false;

    setUploadStatus(""); //KMU

    emit updateRunningChanged();

    if (!mUpdating and mSyncTimer) {
        if (mSyncTimerExc) {
            mTimerSync.start(mSyncTimerExc * 1000 * 60);
            qDebug() << "setSyncTimerExc" << mSyncTimerExc;
            mSyncTimerExc = 0;
            return;
        }
        settingsdb.getValueInt("sync_interval", &mSyncTimer);
        mTimerSync.start(mSyncTimer * 1000 * 60);
        qDebug() << "setSyncTimer" << mSyncTimer;
    }
}

void Backend::clearLog()
{
    mLogText = "";
    emit logTextChanged();
    emit showLogWindowChanged();
}

void Backend::addLog(QString line)
{
    if (mLogText.isEmpty())
        mLogText = line;
    else
        mLogText += "\n" + line;
    if (mLogText.length() > 1500) {
        mLogText = mLogText.right(1000);
        qDebug() << "######################### trunc log";
    }

    emit logTextChanged();
}

void Backend::setRemainingTime(QString remainingTime)
{
    mRemainingTime = remainingTime;
    if (mRemainingTime == "")
        mRemainingTime = "N/A";
    emit remainTimeChanged();
}

void Backend::setElapsedTime(QString elapsedTime)
{
    mElapsedTime = elapsedTime;
    emit elapsedTimeChanged();
}

void Backend::setConnectionType(QString connectionType)
{
    mConnectionType = connectionType;
    emit connectionTypeChanged();
}

void Backend::setLDR(QString LDR)
{
    mLiLDR = LDR;
    emit LDRChanged();
}

void Backend::setLRU(QString LRU)
{
    mLiLRU = LRU;
    emit LRUChanged();
}

void Backend::setTx(QString Tx)
{
    mLiTx = Tx;
    emit TxChanged();
}

void Backend::setRx(QString Rx)
{
    mLiRx = Rx;
    emit RxChanged();
}

void Backend::setDis(QString Dis)
{
    mLiDis = Dis;
    emit DisChanged();
}

void Backend::setLast(QString Last)
{
    mLiLast = Last;
    emit LastChanged();
}

void Backend::setReqs(QString Reqs)
{
    mLiReqs = Reqs;
    emit ReqsChanged();
}

void Backend::setMode(QString Mode)
{
    mLiMode = Mode;
    emit ModeChanged();
}

void Backend::setTotBlocks(QString TotBlocks)
{
    mLiTotBlocks = TotBlocks;
    emit TotBlocksChanged();
}

void Backend::setDisk(QString Disk)
{
    mLiDisk = Disk;
    emit DiskChanged();
}

void Backend::setDisks(QString Disks)
{
    mLiDisks = Disks;
    emit DisksChanged();
}

void Backend::setFile(QString File)
{
    mLiFile = File;
    emit FileChanged();
}

void Backend::setBlock(QString Block)
{
    mLiBlock = Block;
    emit BlockChanged();
}

void Backend::setBlocks(QString Blocks)
{
    mLiBlocks = Blocks;
    emit BlocksChanged();
}

void Backend::setI(QString I)
{
    mLiI = I;
    emit IChanged();
}

void Backend::setN(QString N)
{
    mLiN = N;
    emit NChanged();
}

void Backend::setL(QString L)
{
    mLiL = L;
    emit LChanged();
}

void Backend::setB(QString B)
{
    mLiB = B;
    emit BChanged();
}

bool Backend::getA6153Enabled()
{
    bool result = false;
    getSettingsDB().get_a6153_enabled(&result);
    return true;
    return result;
}

bool Backend::getA615AEnabled()
{
    bool result = false;
    getSettingsDB().get_a615a_enabled(&result);
    return result;
}

bool Backend::getSecureUsbEnabled()
{
    bool result = false;
    getSettingsDB().get_secure_usb_enabled(&result);
    result = true;
    return result;
}

QStringList Backend::getRepositoryData()
{
    return db.getRepositoryData();
}

void Backend::startNetConfigServer()
{
    return;
    QString command = mFi.getNetConfigServerName() + "-f " + mFi.getConfigsDir() + "/config.txt";
    qDebug() << "startNetConfigServer" << command;
    //QString command = mFi.getNetConfigServerName();
    mNetConfigServer = new QProcess(this);
    mNetConfigServer->start(command);
    mNetConfigServer->waitForStarted(2000);
}

void Backend::stopNetConfigServer()
{
    if (mNetConfigServer) {
        mNetConfigServer->kill();
        mNetConfigServer = NULL;
    }
}

bool Backend::isDebugMode()
{
   return mFi.isDebugMode();
}

bool Backend::getFLSDesk()
{
    return mFLSDeskChecked;
}

bool Backend::getEESM()
{
    return mEESMChecked;
}

void Backend::setFLSDesk(bool bFLSDesk)
{
    mFLSDeskChecked = bFLSDesk;
    qDebug() << "setFLSDesk" << mFLSDeskChecked;
    emit flsDeskChanged();
}

void Backend::setEESCM(bool bEESCM)
{
    mEESMChecked = bEESCM;

    emit eescmChanged();
}

void Backend::startSynchroniseWithServer()
{
    qDebug() << "startSynchroniseWithServer";
    emit synchroniseWithServer();

}

void Backend::startAutoSync()
{
    qDebug() << "startAutoSync";
    //SyncStarter::getInstance()->syncScreen();
    mTimerSync.stop();

    emit autoSyncStarted();
    //emit switchScreen(false);
    emit startUpdate();
    QTimer::singleShot(10000, this, SLOT(turnOffScreen()));
}


