#include "replyprocessorsoftware.h"
#include <QDebug>
#include "fileinfo.h"

ReplyProcessorSoftware::ReplyProcessorSoftware()
{
    //mDebug      = true;
    //mDumpData   = true;
}

bool ReplyProcessorSoftware::addData(QByteArray & nData) {
    if (mCompleted)
        return false;

    mInOffset     = 0;
    mLocalBuffer.append(nData);
    mLength       = mLocalBuffer.length();
    bool ret      = false;

    if (mDebug) qDebug() << "nData.length" << nData.length() << mLength;

    dumpData(nData);
    if (mState == PROC_FIRST_HEADER) {
        if (mLength < 1300)
            return ret;
        mState = PROC_FIRST_DATA;
        getHeader(mLocalBuffer);
    }
    if (mDebug)  qDebug() << "mInOffset" << mInOffset;

    bool end = (mChunked) ? getChunked(mLocalBuffer) : getData(mLocalBuffer);
    mLocalBuffer.clear();

    write2File();

    if (!end and mFileSize == 0)
            end = true;

    if (end) {
        qDebug() << "endOfMessageDetected Software:";
        closeFile();
        ret = true;
    }

    return ret;
}

int ReplyProcessorSoftware::getMessageInfo() {
    int offset = 0;
    QByteArray line;
    bool xmlfnd=false;
    int mtlc=0;
    while (true) {
        offset = getLineGen(mBuffer, offset, line);
        qDebug() << offset << "line:" << line;

        if (line.isEmpty()) {
            qDebug() << "Line empty";
            mtlc++;
            if (mtlc > 2) {
                qDebug() << "MAX line";
                mBuffer = mBuffer.mid(offset);
                if (mDebug) qDebug() << "buffer" << hex << mBuffer;
                return offset;
            }
            continue;
        }
        if (line.startsWith("<soap")) {
            if (mDebug)
                qDebug() << "XML" << line;
            xmlfnd=true;
            dumpInfo(line);
            mFileSize = mNameVal["fileSize"].toInt();
            qDebug() << "mFileSize" << mFileSize;
            mLimit    = 250000;
            mCurr     = 0;
        }
        if (line.startsWith("Content-Type: application/octet-stream")) {
            mtlc = 5; // exit next empty line
        }
    }
}

bool ReplyProcessorSoftware::dumpInfo(QByteArray xml) {
    QString name;
    QXmlStreamReader sr(xml);
    QXmlStreamAttributes sa;

    while (!sr.atEnd()) {
          sr.readNext();
          switch (sr.tokenType()) {
          case QXmlStreamReader::StartElement:
              name = sr.name().toString();
              sa = sr.attributes();
              conv2sql(name, sr.attributes());
              continue;
          case QXmlStreamReader::EndElement:
              name = sr.name().toString();
              continue;
          case QXmlStreamReader::Characters:
              qDebug() << "Characters" << name << sr.text().toString();
              mNameVal[name] = sr.text().toString();
              break;
          default:
              qDebug() << "Token:" << sr.tokenType();
              qDebug() << name << sr.text().toString();
              break;
          }
    }
    return false;
}

void ReplyProcessorSoftware::conv2sql(QString name,  QXmlStreamAttributes sa) {
    QXmlStreamAttribute  attr;
    for (int i=0; i < sa.count(); i++) {
        attr = sa[i];
        QString qualname   = name + '.' + attr.name().toString();
        qDebug() << "name:" << qualname << attr.value().toString();
        mNameVal[qualname] = attr.value().toString();
    }
}

bool ReplyProcessorSoftware::openFile() {
    if (mOpened)
        return true;

    FileInfo mFi;

    QString filename = mFi.getSWUPdatesDir() + "/" + mNameVal["fileName"];
    mFile.setFileName(filename);
    qDebug() << "zipfile:" << filename;

    if (!mFile.open(QIODevice::WriteOnly)) {
        qDebug() << "Can't open:<" + filename + ">";
        return false;
    }
    mOpened = true;
    return true;
}

void ReplyProcessorSoftware::write2File() {
    int offset = 0;

    if (mState == PROC_FIRST_DATA) {
        offset = getMessageInfo();
        mState = PROC_DATA;
    }
    if (!openFile()) {
        qDebug() << "File not open";
        return;
    }

    // Only write file size length.
    // qDebug() << "Length" << mBuffer.length() << "rest" << mFileSize;
    int length = mBuffer.length();
    if (length > mFileSize)
        length = mFileSize;

    mFileSize -= length;
    mCurr     += length;
    if (mCurr >= mLimit) {
        //qDebug() << "Length" << length << "rest" << mFileSize;
        mCurr = 0;
    }
    if (length) {
        //qDebug() << "write" << length;
        mFile.write(mBuffer, length);
    }

    mBuffer.clear();
}
