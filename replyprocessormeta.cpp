#include "replyprocessormeta.h"
#include <QDebug>
#include <QFile>
#include "fileinfo.h"

ReplyProcessorMeta::ReplyProcessorMeta()
{
    mFLSMeta = false;
    mFLSMetaOpen = false;
}

bool ReplyProcessorMeta::addData(QByteArray & nData) {
    mInOffset     = 0;
    dumpData(nData);
    mLocalBuffer.append(nData);
    mLength       = mLocalBuffer.length();
    bool ret      = false;

    if (mDebug) qDebug() << "nData.length" << nData.length() << mLength;

    if (mState == PROC_FIRST_HEADER) {
        if (mLength < 750)
            return ret;
        getHeader(mLocalBuffer);
        mState = (mContentLen != -1) ? PROC_FIRST_DATA : PROC_DATA;
        //qDebug() << "mContentLen" << mContentLen;
    }

    if (mDebug)  qDebug() << "mInOffset" << mInOffset;

    bool end = (mChunked) ? getChunked(mLocalBuffer) : getData(mLocalBuffer);
    mLocalBuffer.clear();

    //qDebug() << "end:" << end << mBuffer.length();
    if (mState == PROC_FIRST_DATA) {
        mBuffer = mBuffer.mid(mInOffset);
        mState = PROC_DATA;
    }
    //qDebug() << "mBuffer.length   " << mBuffer.length();
    mZipBuffer.append(mBuffer);
    //qDebug() << "mZipBuffer.length" << mZipBuffer.length();
    //qDebug() << "mZipBuffer" << mZipBuffer;
    mBuffer.clear();

    if (mContentLen >= 0 and mZipBuffer.length() >= mContentLen)
        end = true;

    //qDebug() << "endcc:" << end << mContentLen << mZipBuffer.length();
    if (end) {
        closeDump();
        mBuffer = (mGZipped) ? gzipDecompress(mZipBuffer) : mZipBuffer;
        //qDebug() << "Length" << mBuffer.length() << mBuffer;
        getMessageInfo();
        return true;
    }

    return ret;
}

int ReplyProcessorMeta::getMessageInfo() {
    int offset = 0;
    QByteArray line;
    line.reserve(200000);
    bool xmlfnd=false;
    int mtlc=0;
    QFile of;
    FileInfo mFi;
    while (true) {
        offset = getLineGen(mBuffer, offset, line);
        //if (mDebug) qDebug() << "line:" << line;

        if (line.isEmpty()) {
            qDebug() << "Line empty";
            mtlc++;
            if (xmlfnd) {
                if (mDebug) qDebug() << "File offset:" << hex << offset;
                mBuffer = mBuffer.mid(offset);
                if (mDebug) qDebug() << "buffer" << hex << mBuffer;
                return offset;
            }
            if (mtlc > 5)
                return offset;
            continue;
        }
        if (line.startsWith("<soap")) {
            if (line.contains("<metaDataFlat>")) {
                mFLSMeta = true;
                continue;
            }
            if (mDebug) qDebug() << "XML" << line;
            xmlfnd=true;
            mMediaInf.dumpInfo(line, mDirname);
        }
        if (mFLSMeta) {
            if (line.startsWith("DBFILENAME")) {
                line.remove(0, 11);
                QString filename = mFi.getDataBasePath() + '/' + line;
                of.setFileName(filename);
                if (of.open(QIODevice::WriteOnly)) {
                    mFLSMetaOpen = true;
                }
                continue;
            }
            if (line.startsWith("DBCLOSE")) {
                if (mFLSMetaOpen) {
                    of.close();
                    mFLSMetaOpen = false;
                }
                continue;
            }
            line.append('\n');
            of.write(line, line.length());
            mMediaInf.buildInfoFile(line, mDirname);
        }
    }
}
