#ifndef REPLYPROCESSORSOFTWARE_H
#define REPLYPROCESSORSOFTWARE_H
#include "replyprocessor.h"

class ReplyProcessorSoftware : public ReplyProcessor
{
public:
    ReplyProcessorSoftware();
    bool addData(QByteArray & nData);
private:
    bool openFile();
    void write2File();
    int getMessageInfo();
    bool dumpInfo(QByteArray xml);
    void conv2sql(QString name,  QXmlStreamAttributes sa);
};

#endif // REPLYPROCESSORSOFTWARE_H
