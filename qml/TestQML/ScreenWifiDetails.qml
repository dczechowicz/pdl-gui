// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "Components"

Rectangle {
    width: 480;
    height: 750;
    id: container
    color: "lightgray"

    Rectangle {
        x: 40
        y:1
        Column {
            spacing: 1;

            HeaderText {
                text: "IP Settings";
            }

            Rectangle {
                color: "white"

                border.color: "gray"
                border.width: 2
                radius: 10

                width: 400
                height: 80 + 36 + 36 + 36 + 85

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 4
                    anchors.leftMargin: 5

                    Column {
                        id: settingsColumn

                        OnOffSetting {
                            id: dhcpSwitch
                            width: 380
                            text: "DHCP"

                            onOnChanged: {
                                wiFiConfig.useDhcp = on;
                            }
                        }
                        EditValueItem {
                            id: ipAddressEdit
                            enabled: !dhcpSwitch.on
                            text: qsTr("IP Address")
                            valueMask: "000.000.000.000;_"
                            numericValue: true
                        }
                        EditValueItem {
                            id: subnetEdit
                            enabled: !dhcpSwitch.on
                            text: qsTr("Subnet mask")
                            valueMask: "000.000.000.000;_"
                            numericValue: true
                        }
                        EditValueItem {
                            id: gatewayEdit
                            enabled: !dhcpSwitch.on
                            text: qsTr("Gateway")
                            valueMask: "000.000.000.000;_"
                            numericValue: true
                        }
                        EditValueItem {
                            id: dnsEdit
                            enabled: !dhcpSwitch.on
                            text: qsTr("Primary DNS")
                            valueMask: "000.000.000.000;_"
                            numericValue: true
                        }
                    }

                    Button {
                        anchors {
                            top: settingsColumn.bottom
                            topMargin: 10
                            horizontalCenter: parent.horizontalCenter
                        }
                        text: qsTr("Apply settings")

                        onClicked: {
                            wiFiConfig.ipAddress = ipAddressEdit.value;
                            wiFiConfig.subnetMask = subnetEdit.value;
                            wiFiConfig.defaultGateway = gatewayEdit.value;
                            wiFiConfig.primaryDns = dnsEdit.value;
                        }
                    }
                }


            }

            HeaderText {
                text: "HTTP Proxy"
            }

            Rectangle {
                id: networklist
                width: 400
                height: 80 + 36 + 36
                radius: 10
                border.width: 2
                border.color: "gray"

                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 4
                    anchors.leftMargin: 5

                    Column {
                        EditValueItem {
                            text: qsTr("Server")
                        }
                        EditValueItem {
                            text: qsTr("Port")
                            validator: IntValidator{bottom: 1; top: 65535;}
                            numericValue: true
                        }
                        EditValueItem {
                            text: qsTr("Username")
                        }
                        EditValueItem {
                            text: qsTr("Password")
                            echoMode: TextInput.Password
                        }
                    }
                }
            }
        }
    }
}
