#include <QDebug>
#include "Backend.h"
#include "sslclientbase.h"

#ifdef Q_WS_WIN32
#define SNPRINTF _snprintf
#include "Windows.h"
#else
#define SNPRINTF ::snprintf
#endif

SslClientBase::SslClientBase(QObject *parent) :
    QObject(parent), socket(0)
{
    mRetryFailCount = 0;
    mUseProxy = false;
}

void SslClientBase::myConnect()
{
    disconnect();
    if (!socket) {
        socket = new QSslSocket(this);
        connect(socket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(socketStateChanged(QAbstractSocket::SocketState)));
        connect(socket, SIGNAL(encrypted()),                                this, SLOT(socketEncrypted()));
        connect(socket, SIGNAL(sslErrors(QList<QSslError>)),                this, SLOT(sslErrors(QList<QSslError>)));
        connect(socket, SIGNAL(readyRead()),                                this, SLOT(socketReadyRead()));
    }

    if (gConfig->getSecure()) {
        qDebug() << "cert file" << mFi.getCertName();
        QFile f(mFi.getCertName());
        if (!f.exists()) {
            qDebug() << "Does not exist";
            state = 0;
            return;
        }

        QSslCertificate  c (&f);

        QList<QSslError> expectedSslErrors;
        expectedSslErrors.append(QSslError(QSslError::UnableToGetLocalIssuerCertificate, c));
        expectedSslErrors.append(QSslError(QSslError::UnableToGetIssuerCertificate, c));
        expectedSslErrors.append(QSslError(QSslError::InvalidCaCertificate, c));
        expectedSslErrors.append(QSslError(QSslError::NoPeerCertificate, c));
        expectedSslErrors.append(QSslError(QSslError::UnspecifiedError, c));
        expectedSslErrors.append(QSslError(QSslError::SelfSignedCertificate, c));
        expectedSslErrors.append(QSslError(QSslError::CertificateUntrusted, c));

        socket->ignoreSslErrors(expectedSslErrors);
        socket->addCaCertificate(c);
        if (mUseProxy)
            socket->setProxy(mProxy);

        qDebug() << "Remote connection" << gConfig->getURL() << gConfig->getPort();
        socket->connectToHostEncrypted(gConfig->getURL(), gConfig->getPort());
    } else {
        qDebug() << "Local connection" << gConfig->getURL() << gConfig->getPort();
        socket->connectToHost(gConfig->getURL(), gConfig->getPort());
    }


}

void SslClientBase::setProxy()
{
    if (!mUseProxy)
        return;

    QString proxyHostName;
    int     proxyPort;
    QString proxyUser;
    QString proxyPassword;

    mProxy.setType(QNetworkProxy::Socks5Proxy);

    if (!gBackend->getSettingsDB().getValueString("proxyHostName", proxyHostName))
        return;
    mProxy.setHostName(proxyHostName);

    if (!gBackend->getSettingsDB().getValueInt("proxyPort", &proxyPort))
        return;

    mProxy.setPort(proxyPort);
    if (gBackend->getSettingsDB().getValueString("proxyUser", proxyUser)) {
        mProxy.setUser(proxyUser);
        if (gBackend->getSettingsDB().getValueString("proxyPassword", proxyPassword)) {
            mProxy.setPassword(proxyPassword);
        }
    }
    socket->setProxy(mProxy);
}

void SslClientBase::disconnect(bool force)
{
    if (socket) {
        socket->close();
        if (force) {
            socket->deleteLater();
            socket = NULL;
        }
    }
}


QString SslClientBase::getLogin() {
    char buf[1024];
    SNPRINTF(buf, 1024, (const char*) mLogin.toUtf8(), (const char*)mKey.toUtf8());
    QString r = QString::fromAscii(buf);
    qDebug() << r.toUtf8();
    tempfile = mFi.getTempName("login");
    return r;
}

QString SslClientBase::getLogoff(int status) {
    char buf[1024];
    qDebug() << "getLogoff status" << status;
    SNPRINTF(buf, 1024, (const char*) mLogoff.toUtf8(), (const char*)mTicket.toUtf8()), (long)status;
    QString r = QString::fromAscii(buf);
    qDebug() << r.toUtf8();
    tempfile = mFi.getTempName("logoff");
    return r;
}

QString SslClientBase::getLogout(int status) {
    char buf[1024];
    qDebug() << "getLogout status" << status;
    SNPRINTF(buf, 1024, (const char*) mLogout.toUtf8(), (const char*)mTicket.toUtf8());
    QString r = QString::fromAscii(buf);
    qDebug() << r.toUtf8();
    tempfile = mFi.getTempName("logout");
    return r;
}

QString SslClientBase::getIsSoftwareAvailable() {
    char buf[1024];
    qDebug() << "getIsSoftwareAvailable";
    SNPRINTF(buf, 1024, (const char*) mIsSoftware.toUtf8(), (const char*)mTicket.toUtf8());
    QString r = QString::fromAscii(buf);
    qDebug() << r.toUtf8();
    tempfile = mFi.getTempName("issoftware");
    return r;
}

QString SslClientBase::getGetSoftware() {
    char buf[1024];
    qDebug() << "getGetSoftware";
    SNPRINTF(buf, 1024, (const char*) mGetSoftware.toUtf8(), (const char*)mTicket.toUtf8());
    QString r = QString::fromAscii(buf);
    qDebug() << r.toUtf8();
    tempfile = mFi.getTempName("getsoftware");
    return r;
}

QString SslClientBase::getClientInformation() {
    char buf[1024];
    qDebug() << "getClientInformation";
    SNPRINTF(buf, 1024, (const char*) mClInfo.toUtf8(), (const char*)mTicket.toUtf8());
    QString r = QString::fromAscii(buf);
    qDebug() << r.toUtf8();
    tempfile = mFi.getTempName("clinfo");
    return r;
}

QString SslClientBase::getMeta() {
    char buf[1024];
    SNPRINTF(buf, 1024,
             (const char*) (gConfig->isFLSDesk()) ? mGetMetaFLS.toUtf8() : mGetMeta.toUtf8(),
             (const char*)mTicket.toUtf8());
    QString r = QString::fromAscii(buf);
    qDebug() << r.toUtf8();
    return r;
}

QString SslClientBase::getMedia(QString partnumber, int seq) {
    char buf[1024];
    SNPRINTF(buf, 1024, (const char*) mGetMedia.toUtf8(), (const char*)mTicket.toUtf8(),
             (const char*) partnumber.toUtf8(), seq);
    QString r = QString::fromAscii(buf);
    qDebug() << "------------------------> getMedia" << partnumber << seq;
    //qDebug() << r.toUtf8();
    return r;
}

void SslClientBase::initStrings() {
    mKey    = "5e3249be9de0814c58d94efee4be5f5b987ec8c3";
    mKey    = "a589c8e7087c24da82600a8f6a4aa345f7a4bfcf";

    mLogin  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mLogin += "<soapenv:Header/><soapenv:Body>\n";
    mLogin += "<pdl:clientLogin>\n";
    mLogin += "<clientKey>%s</clientKey>\n";               // Client key
    mLogin += "<loginInformation repoSize=\"1024\" availableSize=\"1024\" version=\"0.9\"/>\n";
    mLogin += "</pdl:clientLogin>\n";
    mLogin += "</soapenv:Body></soapenv:Envelope>\n";

    mIsSoftware  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mIsSoftware += "<soapenv:Header/><soapenv:Body>\n";
    mIsSoftware += "<pdl:isLoaderSoftwareUpdateAvailable>\n";
    mIsSoftware += "<ticket>%s</ticket>\n";                   // Ticket
    mIsSoftware += "</pdl:isLoaderSoftwareUpdateAvailable>\n";
    mIsSoftware += "</soapenv:Body></soapenv:Envelope>\n";

    mGetSoftware  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mGetSoftware += "<soapenv:Header/><soapenv:Body>\n";
    mGetSoftware += "<pdl:retrieveLoaderSoftware>\n";
    mGetSoftware += "<ticket>%s</ticket>\n";                   // Ticket
    mGetSoftware += "</pdl:retrieveLoaderSoftware>\n";
    mGetSoftware += "</soapenv:Body></soapenv:Envelope>\n";

    mGetMeta  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mGetMeta += "<soapenv:Header/><soapenv:Body>\n";
    mGetMeta += "<pdl:retrieveMetadata>\n";
    mGetMeta += "<ticket>%s</ticket>\n";                   // Ticket
    mGetMeta += "</pdl:retrieveMetadata>\n";
    mGetMeta += "</soapenv:Body></soapenv:Envelope>\n";

    mLogoff  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mLogoff += "<soapenv:Header/><soapenv:Body>\n";
    mLogoff += "<pdl:clientLogoff>\n";
    mLogoff += "<ticket>%s</ticket>\n";                   // Ticket
    mLogoff += "<syncstatus>%d</syncstatus>\n";           // syncStatus
    mLogoff += "</pdl:clientLogoff>\n";
    mLogoff += "</soapenv:Body></soapenv:Envelope>\n";

    mLogout  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mLogout += "<soapenv:Header/><soapenv:Body>\n";
    mLogout += "<pdl:clientLogout>\n";
    mLogout += "<ticket>%s</ticket>\n";                   // Ticket
    mLogout += "</pdl:clientLogout>\n";
    mLogout += "</soapenv:Body></soapenv:Envelope>\n";

    mClInfo  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mClInfo += "<soapenv:Header/><soapenv:Body>\n";
    mClInfo += "<pdl:clientInformation>\n";
    mClInfo += "<ticket>%s</ticket>\n";                   // Ticket
    mClInfo += "</pdl:clientInformation>\n";
    mClInfo += "</soapenv:Body></soapenv:Envelope>\n";

    mGetMetaFLS  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mGetMetaFLS += "<soapenv:Header/><soapenv:Body>\n";
    mGetMetaFLS += "<pdl:retrieveMetadataFLS>\n";
    mGetMetaFLS += "<ticket>%s</ticket>\n";                   // Ticket
    mGetMetaFLS += "</pdl:retrieveMetadataFLS>\n";
    mGetMetaFLS += "</soapenv:Body></soapenv:Envelope>\n";

    mGetMedia  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mGetMedia += "<soapenv:Header/><soapenv:Body>\n";
    mGetMedia += "<pdl:downloadMedia>\n";
    mGetMedia += "<ticket>%s</ticket>\n";                   // Ticket
    mGetMedia += "<mediaRequest offset=\"1\">\n";
    mGetMedia += "<partnumber>%s</partnumber>\n";           // Part number
    mGetMedia += "<sequenceNumber>%d</sequenceNumber>\n";   // Sequence number
    mGetMedia += "</mediaRequest>\n";
    mGetMedia += "</pdl:downloadMedia>\n";
    mGetMedia += "</soapenv:Body></soapenv:Envelope>\n";

    mLoadLogPre  = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:pdl=\"http://www.lufthansa-technik.de/fls/ws/pdl\">\n";
    mLoadLogPre += "<soapenv:Header/><soapenv:Body>\n";
    mLoadLogPre += "<pdl:postLoadLog>\n";
    mLoadLogPre += "<ticket>%s</ticket>\n";                // Ticket
    mLoadLogPre += "<loadLog>\n";

    mLoadLogInf += "<loadStart>%s</loadStart>\n";          // Load start
    mLoadLogInf += "<loadEnd>%s</loadEnd>\n";              // Load end
    mLoadLogInf += "<loadType>%s</loadType>\n";            // Load type (ARINC 615-3)
    mLoadLogInf += "<loadStatus>%s</loadStatus>\n";        // Load status (COMP. XFER, ABORT HDRW)
    mLoadLogInf += "<loaderVersion>%s</loaderVersion>\n";  // Loader version (opt)
    mLoadLogInf += "<userComment>%s</userComment>\n";      // User comment (opt)
    mLoadLogInf += "<partnumber>%s</partnumber>\n";        // Part number (opt)
    mLoadLogInf += "<acreg>%s</acreg>\n";                  // Aircraft reg. (opt)
    mLoadLogInf += "<pnJacksum>%s</pnJacksum>\n";          // Jacksum of first disk (opt)

    mLogFilePre  = "<log_files>\n";

    mLogFile     = "<log_file>\n";
    mLogFile    += "<filename>%s</filename>\n";           // File name
    mLogFile    += "<description>%s</description>\n";     // Description
    mLogFile    += "<file>%s</file>\n";                   // File
    mLogFile    += "</log_file>\n";

    mLogFilePost = "</log_files>\n";

    mLoadLogPost  = "</loadLog>\n";
    mLoadLogPost += "</pdl:postLoadLog>\n";
    mLoadLogPost += "</soapenv:Body></soapenv:Envelope>\n";
}

void SslClientBase::sendMessageMTOM(QString mess)
{
    myConnect();
    QString t;

    QString boundary = t.sprintf("--%s\r\n", BOUNDARY);
    QByteArray bindata;
    mLoadInfo->appendFiles2Message(bindata, boundary);

    QString nmess;
    nmess  = boundary;
    nmess += "Content-Type: application/xop+xml; type=\"text/xml\"; charset=utf-8\r\n";
    nmess += "\r\n";
    nmess += mess;
    nmess += boundary;

    QString post = t.sprintf("POST %s HTTP/1.1\r\n", (const char*)gConfig->getPost().toUtf8());
    post += "User-Agent: Jakarta Commons-HttpClient/3.1\r\n";
    post += "Host: mbs\r\n";
    post += "Authorization: Basic bWJzOnNibTIwMTI=\r\n";
    post += "Content-Type: Multipart/Related; type=\"application/xop+xml\"; start-info=\"text/xml\";";
    post += t.sprintf("boundary=\"%s\"\r\n", BOUNDARY);
    post += "Content-Length: ";
    post += t.sprintf("%d", nmess.length() + bindata.length());
    post += "\r\n\r\n";

    post += nmess;

    //appendString(post + '\n');

    // File saved for test / investigation purposes
    QFile f("/Users/kevin/PDL_dev/tmpdir/upload");
    if (!f.open(QIODevice::WriteOnly)) {
        qDebug() << "Failed to open file.";
    } else {
        qDebug() << "Writing file";
        f.write(post.toUtf8());
        f.write(bindata);
        f.close();
    }
    socket->write(post.toUtf8());
    socket->write(bindata);
}

void SslClientBase::sendMessage(QString mess, bool usecomp, bool first)
{
    myConnect();
    if (usecomp)
        usecomp = gBackend->mFi.noMetaGzip() ? false : true;

    QString t;
    QString post = t.sprintf("POST %s HTTP/1.1\r\n", (const char*)gConfig->getPost().toUtf8());
    post += "User-Agent: Jakarta Commons-HttpClient/3.1\r\n";
    post += "Host: mbs\r\n";
    post += "Authorization: Basic bWJzOnNibTIwMTI=\r\n";
    if (usecomp)
        post += "Accept-Encoding: gzip\r\n";
    post += "Content-Length: ";
    post += t.sprintf("%d", mess.length());
    post += "\r\n\r\n";
    post += mess;

    //qDebug() << "post" << post;
    socket->write(post.toUtf8());
}

#define MXOPINC "<xop:Include xmlns:xop=\"http://www.w3.org/2004/08/xop/include\" href=\"cid:%s\"/>"

QString SslClientBase::getUpload() {
    char buf[1024];
    char inc[1024];
    SNPRINTF(buf, 1024, (const char*) mLoadLogPre.toUtf8(), (const char*)mTicket.toUtf8());
    QString r = QString::fromAscii(buf);
    qDebug() << "1" << r;

    QStringList ql = mLoadInfo->getLoadInfo();
    qDebug() << "ql.count()" << ql.count();
    for(int i=0; i < ql.count() || i < mLoadLogInf.count(); i++) {
        SNPRINTF(buf, 1024, (const char*) mLoadLogInf[i].toUtf8(), (const char*)ql[i].toUtf8());
        r += QString::fromAscii(buf);
        //qDebug() << "2" << r;
    }
    r += mLogFilePre;
    qDebug() << "3" << r;

    QStringList qlf = mLoadInfo->getFileInfo();
    for(int i=0; i < qlf.count(); i++) {
        QStringList fi = qlf[i].split("|");
        fi.pop_front();
        while(fi.count() < 3)
            fi.append("");
        SNPRINTF(inc, 1024, MXOPINC, (const char*)fi[2].toUtf8());
        SNPRINTF(buf, 1024, (const char*) mLogFile.toUtf8(),
                 (const char*)fi[0].toUtf8(),
                 (const char*)fi[1].toUtf8(),
                 inc);
        r += QString::fromAscii(buf);
        //qDebug() << r;
    }

    r += mLogFilePost;
    r += mLoadLogPost;
    qDebug() << "getUpload"<< r.toUtf8();
    mLoadInfo->reset();
    return r;
}

void SslClientBase::sslErrors(const QList<QSslError> &errors)
{
    qDebug() << "sslErrors" << socket->state();

    socket->ignoreSslErrors(errors);
}

