#ifndef WEBCONNECT_H
#define WEBCONNECT_H

#include <QDebug>
#include <QTcpSocket>
#include <QAbstractSocket>


class WebConnect : public QObject
{
    Q_OBJECT

public:
    explicit WebConnect(QObject *parent = 0);
    void Test();

signals:

public slots:
    void connected();
    void disconnected();
    void error(QAbstractSocket::SocketError socketError );
    void stateChanged( QAbstractSocket::SocketState socketState );
    void aboutToClose();
    void bytesWritten (qint64 bytes);
    void readChannelFinished();
    void readyRead();

private:
    QTcpSocket *socket;
};

#endif // WEBCONNECT_H
