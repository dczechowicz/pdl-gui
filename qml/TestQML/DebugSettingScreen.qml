import QtQuick 1.1
import "./Components"

Page {
    id: root


    Rectangle {
        visible: true
        Row {
            y: 10
            x: 40
            Text {
                x: 50
                font.pixelSize: 17
                text: qsTr("Server:")
                color: "black"
            }
            Text {
                x: 250
                text: backend.serverName
                color: "blue"
                font.pixelSize: 17
            }
        }
        Column {
            x: 40
            y: 40
            spacing: 10
            Button {
                width: 120
                text: "eESM Intra (Test)"
                height: 40
                onClicked:{
                    backend.setServerId(1)
                }
            }
            Button {
                width: 120
                text: "eESM Intra (Prod)"
                height: 40
                onClicked:{
                    backend.setServerId(2)
                }
            }
        }
        Column {
            x: 170
            y: 40
            spacing: 10
            Button {
                width: 120
                text: "eESM (Test)"
                height: 40
                onClicked:{
                    backend.setServerId(3)
                }
            }
            Button {
                width: 120
                text: "eESM (Prod)"
                height: 40
                onClicked:{
                    backend.setServerId(4)
                }
            }
        }
        Column {
            x: 300
            y: 40
            spacing: 10
            Button {
                width: 120
                text: "FLS-Desk (Test)"
                height: 40
                onClicked:{
                    backend.setServerId(5)
                }
            }
            Button {
                width: 120
                text: "FLS-Desk (Prod)"
                height: 40
                onClicked:{
                    backend.setServerId(6)
                }
            }

        }
    }

    Rectangle {
        x: 23
        y: 150
        Row {
            spacing: 10
            CheckBox {
                width: 120
                text: "Proxy"
                height: 100
                onCheckedChanged:{
                    backend.isProxy = true;
                }
            }
        }
        Column {
            x: 23
            y: 30
            visible: false //backend.isProxy
            Row {
                width: 400
                height: 30
                Text {
                    id: text0
                    height: 25
                    width: 140
                    text: qsTr("Address:")
                    font.pixelSize: 17
                }

                TextInputBackground {
                    id: textInput0
                    width: 180
                    height: 25
                }
            }
            Row {
                width: 400
                height: 30
                Text {
                    id: text1
                    height: 25
                    width: 140
                    text: qsTr("Username:")
                    font.pixelSize: 17
                }

                TextInputBackground {
                    id: textInput1
                    width: 180
                    height: 25
                }
            }

            Row {
                width: 400
                height: 30
                Text {
                    id: text2
                    height: 25
                    text: "Password";
                    font.pixelSize: 17
                    width: 140
                }

                TextInputBackground {
                    id: textInput2
                    width: 240
                    height: 25
                    passwordCharacter: "*"
                    echoMode: TextInput.Password
                }
            }
        }

    }

}
