#ifndef REPLYPROCESSORLOGIN_H
#define REPLYPROCESSORLOGIN_H
#include "replyprocessor.h"

class ReplyProcessorLogin : public ReplyProcessor
{
public:
    ReplyProcessorLogin();
    bool addData(QByteArray & nData);
private:
    int getMessageInfo();
    bool dumpInfo(QByteArray xml);
    void conv2sql(QString name,  QXmlStreamAttributes sa);
};

#endif // REPLYPROCESSORLOGIN_H
