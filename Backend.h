#ifndef BACKEND_H
#define BACKEND_H

#include <QObject>
#include <QStringListModel>
#include <QDir>
#include <QMap>
#include <QHash>
#include <QDateTime>
#include <QProcess>
#include <QRegExp>
#include <QThread>
#include <QMutex>
#include <QMutexLocker>
#include "sslclient.h"
#include "loadclient.h"
#include "dbsqlite.h"
#include "dbsettings.h"
#include "bthread.h"
#include "syncthread.h"
#include "monitorserver.h"
#include "runclient.h"

#define SYNC_DELAY (6000 * 1)

struct Backend_SoftwareInfo
{
    int avgLoad;
    int noMedia;
    QString title;
    QDateTime added;
};

typedef enum {
        SWPN_NOTSET,
        SWPN_NOK,
        SWPN_VALID,
        SWPN_MATCH
} swpnval_t;

typedef enum {
    EXUSB_EXCEPTION,
    EXUSB_ERROR,
} usb_reset_t;

typedef enum {
    USB_PREPARE,
    USB_PREPARE_RESP,
    USB_SECURE,
    USB_SECURE_RESP,
    USB_ABORT,
    USB_ABORT_RESP,
} usbstate_t;

// Warning order must follow above enum (Change);
typedef enum {
        COL_WHITE,
        COL_RED,
        COL_YELLOW,
        COL_GREEN,
        COL_BLUE
} colors_t;

#define IFDBG(x) if (gBackend->ifDebug(x))

#define IFDBGALL IFDBG(DBG_ALL)
#define IFDBGSQL IFDBG(DBG_SQL)
#define IFDBGREP IFDBG(DBG_REP)
#define IFDBGCOM IFDBG(DBG_COM)

typedef enum {
    DBG_ALL = 0x0001,
    DBG_SQL = 0x0002,
    DBG_REP = 0x0004,
    DBG_COM = 0x0008
} debugws_t;

class Backend;
extern Backend *gBackend;

class Backend : public QObject
{
    Q_OBJECT
// DB Related changes
private:
    DBSqlite db;
    DBSettings settingsdb;
    QHash <QString, int> mColHash;

// Thread stuff
private:
    SyncThread *mSslThread;
    QThread *mThread;
    BThread *mWorker;
    RunClient mRunClient;

    void createSslThread();
    void createThread();
public slots:
    void errorString(QString error) {
        qDebug() << "errorString" << error;
    }
// End of thread stuff
// PrepareSUB stuff
public slots:
    void commandResponse(int retcode, QString output);
    void commandOutput(QString output);

// Signal to SSL
signals:
    void synchroniseWithServer();
    void setSyncStatus(syncstates_t status);

private:
    void resetUSB(usb_reset_t state);
    QApplication *mApp;

signals:
    void sendAString(QString string);

public:
    DBSqlite & getDB()           { return db;}
    DBSettings & getSettingsDB() { return settingsdb;}
    QString getAccessKey()       {
        if (mAccessKey.isEmpty()) {
            mAccessKey = "be49325e9de0814c58d94efee4be5f5b987ec8c3";
        }
        return mAccessKey;
    }
private:
    QString mAccessKey;

public:
    void appRestart(QApplication *app);
    explicit Backend(QObject *parent = 0);

    void Init();
    void WatchInit();
    QStringListModel *airlineModel();
    QStringListModel *actypeModel();
    QStringListModel *tailsignModel();
    //QStringListModel *hwfinModel();
    QStringListModel *softwareModel();

    QString getSWPath() {
        QString t;
        return t.sprintf("%s/SW/%s", (const char*) mAirlineKey.toUtf8(), (const char*) mSoftware.toUtf8());
    }
    Q_INVOKABLE QString doCheckTail(QString in);
    Q_INVOKABLE QString doCheckSwpn(QString in);
    QStringList mColors;
    QRegExp mRETailSignWhole;
    QRegExp mRETailSignStart;

signals:
    void airlineChanged();
    void actypeChanged();
    void tailsignChanged();
    void ataChapterChanged();
    void ataSectionChanged();
    void softwareChanged();
    void softwareTitleChanged();
    void softwareCocChanged();
    void statusChanged();
    void statusColorChanged();
    void buildInfoChanged();
    void usbButtonTextChanged();
    void startTimeChanged();
    void remainTimeChanged();
    void elapsedTimeChanged();
    void connectionTypeChanged();
    void progressChanged();

    void showUsbButtonEnabled();
    void showUSBAbortChanged();
    void showLogWindowChanged();
    void showHomeEnabledChanged();
    void showServerIdChanged();
    void showServerNameChanged();

    void logTextChanged();
    void installRunningChanged();
    void updateRunningChanged();
    void showAdminChanged();

    void pdlNameChanged();
    void lastSyncChanged();
    void updateStatusChanged();
    void updateProgressChanged();
    void updateStartTimeChanged();

    void uploadStatusChanged();

    void pageChanged();
    void remainingTimeChanged();
    void LDRChanged();
    void LRUChanged();
    void TxChanged();
    void RxChanged();
    void DisChanged();
    void LastChanged();
    void ReqsChanged();
    void ModeChanged();
    void TotBlocksChanged();
    void DiskChanged();
    void DisksChanged();
    void FileChanged();
    void BlockChanged();
    void BlocksChanged();
    void IChanged();
    void NChanged();
    void LChanged();
    void BChanged();

    void flsDeskChanged();
    void eescmChanged();

    void screenStateChanged(bool turnedOn);
    void autoSyncStarted();

    void demoPDLModeChanged();

  public slots:

    void setConfigValue(const QString a, const QString b);

    void runCommand(QString command);
    void setAirline(QString name);
    void setACType(QString name);
    void setTailsign(QString name);
    void setAtaChapter(QString name);
    void setAtaSection(QString name);
    void setSoftware(QString name);
    void setSoftwareTitle(QString name);
    void setSoftwareCoC(QString name);

    void setPdlName(QString status);
    void setLastSync(int tm);

    void setStatus(QString status);
    void setStatusColor(QString status);
    void setBuildInfo(QString info);
    void setUSBButtonText(QString status);
    void setUpdateStatus(QString status);
    void setUpdateProgress(QString progress);
    void setUpdateStartTime(QString time);
    void reloadData();

    void setUploadStatus(QString status);

    void setPage(QString time);
    void popPage(bool Home);

    void onAbortUSB();
    void prepareUSB();
    void startDataLoad();
    void stopDataLoad();
    void dataLoadStopped();

    void creatMonitorSocket();
    void updateElapsed();
    void updateConnectionType();
    void startUpdate();
    void stopUpdate();
    void startSynchroniseWithServer();
    void startAutoSync();

    void checkTail();
    void checkSwpn();

    void quit();
    void restart();
    void shutdown();
    void clearAuto();
    void resetAll();
    void resetSome();
    void restartGUI();
    void startSync();
    void connRemoteSuport();
    void purgeRepository();

    void addLog(QString line);
    void setRunning(bool running);
    void setUpdating(bool updating);

    void setRemainingTime(QString RemainingTime);
    void setElapsedTime(QString ElapsedTime);
    void setConnectionType(QString ConnectionType);
    void setLDR(QString LDR);
    void setLRU(QString LRU);
    void setTx(QString Tx);
    void setRx(QString Rx);
    void setDis(QString Dis);
    void setLast(QString Last);
    void setReqs(QString Reqs);
    void setMode(QString Mode);
    void setTotBlocks(QString TotBlocks);
    void setDisk(QString Disk);
    void setDisks(QString Disks);
    void setFile(QString File);
    void setBlock(QString Block);
    void setBlocks(QString Blocks);
    void setI(QString I);
    void setN(QString N);
    void setL(QString L);
    void setB(QString B);

    void setFLSDesk(bool bFLSDesk);
    void setEESCM(bool bEESCM);

    bool getA6153Enabled();
    bool getA615AEnabled();
    bool getSecureUsbEnabled();

    QStringList getRepositoryData();

    void startNetConfigServer();
    void stopNetConfigServer();
    void switchScreen(bool onoff);
    void turnOffScreen();
    bool isDebugMode();
    bool isDemoPDLMode() { return mDemoPDLMode; }

private:
    QMutex mMutex;
    QTimer mTimer;
    QTimer mAppTimer;
    QTimer mTimerSync;

private slots:
    void timeout();
    void appTimeout();

protected slots:
    void setUsbButtonEnabled(bool enable);
    void setShowUSBAbort(bool showUSBAbort);
    void setShowLogWindow(bool showWindow);
public slots:
    void setHomeEnabled(bool enabled);
    void setServerId(int ServerId);
    void setServerName(QString ServerName);

    void clearLog();
public:
    void setSyncTimerExc(int syncTime) { mSyncTimerExc = syncTime; }
    void writeStringToFile(const char * file, const char *data);

    void setPower(bool power);
    bool isPower();

    int mDebugSetting;
    bool ifDebug(debugws_t dbg) {
        return (mDebugSetting & (DBG_ALL || dbg) ? true : false);
    }

    MonitorServer *mMonServer;
    QString mTailsign;
    QString mSoftware;
    QString mSoftwareTitle;
    QString mSoftwareCoC;
    bool getUpdateRunning();
    FileInfo mFi;

    swpnval_t getSwpnState() {
        //qDebug() << "getSwpnState" << (int) mSwpnVal;
        return mSwpnVal;
    }

    swpnval_t getTailState() {
        //qDebug() << "getTailState" << (int) mTailVal;
        return mTailVal;
    }

    Q_INVOKABLE QString getUsbButtonText() {
        return mUSBButtonText;
    }

    Q_INVOKABLE QString getStatusColorString() {
        QString color = mColors[mColHash.value(mStatus, COL_BLUE)];
        //qDebug() << "getStatusColor" << color;
        return color;
    }

    Q_INVOKABLE QString getSwpnStateColor() {
        QString color = mColors[(int)getSwpnState()];
        //qDebug() << "getSwpnState" << color;
        return color;
    }

    Q_INVOKABLE QString getTailStateColor() {
        QString color = mColors[(int)getTailState()];
        //qDebug() << "getTailState" << color;
        return color;
    }

  private:
    QProcess *mProcess;
    QProcess *mNetConfigServer;
    void checkManualComplete();
    QString mManSoftware;
    QString mManTailSign;
    LoadClient load;
    QString mAirline;
    QString mAirlineKey;
    QString mACType;
    QString mHWFin;
    QString mAtaChapter;
    QString mAtaSection;

    QTime   mElapseTimer;

    QString mUSBButtonText;
    bool mShowUSBAbort;
    bool mUsbButtonEnabled;
    usbstate_t mUSBState;

    bool mDemoPDLMode;

    QString mLiLDR;
    QString mLiLRU;
    QString mLiTx;
    QString mLiRx;
    QString mLiDis;
    QString mLiLast;
    QString mLiReqs;
    QString mLiMode;
    QString mLiTotBlocks;
    QString mLiDisk;
    QString mLiDisks;
    QString mLiFile;
    QString mLiBlock;
    QString mLiBlocks;
    QString mLiI;
    QString mLiN;
    QString mLiL;
    QString mLiB;

    QString mStatus;
    QString mStatusColor;
    QString mBuildInfo;
    QString mStart;
    QString mRemainingTime;
    QString mElapsedTime;
    QString mConnectionType;
    QString mProgress;

    QString mPdlName;
    QString mLastSync;

    QString mUpdateStatus;
    QString mUpdateProgress;
    QString mUpdateStartTime;

    QString mUploadStatus;

    QStringListModel *mAirlineModel;
    QStringListModel *mAcTypeModel;
    QStringListModel *mTailsignModel;
    QStringListModel *mHwFinModel;
    QStringListModel *mSoftwareModel;

    QMap<QString, QString>     Airlines;
    QMap<QString, QStringList> Aircraft_Type;
    QMap<QString, QStringList> Aircraft_Tailsign;
    QMap<QString, QStringList> Type_HwFin;
    QMap<QString, QStringList> Type_Software;

    QStringList All_Software;

    QDir mWorkdir;
    bool mSyncAllowed;
    bool mPower;
    bool mPreparing;
    bool mRunning;
    bool mUpdating;
    bool mInAbort;
    int  mCompleteTime;
    bool mShowLogWindow;
    bool mHomeEnabled;
    int  mSyncTimerExc;
    int  mSyncTimer;
    int  mServerId;
    QString mServerName;
    bool mFLSDeskChecked;
    bool mEESMChecked;

    swpnval_t mTailVal;
    swpnval_t mSwpnVal;

    QStringList mPages;
    QString mCurrPage;

    QString mLogText;
    QString mEmpty;

    QString getAirline();
    QString getAcType();
    QString getTailsign();
    QString getAtaChapter();
    QString getAtaSection();
    QString getHwFin();
    QString getSoftware();
    QString getSoftwareTitle();
    QString getSoftwareCoC();

    QString getStatus();
    QString getStatusColor();
    QString getBuildInfo();
    QString getStartTime();
    QString getRemainingTime();
    QString getElapsedTime();
    QString getConnectionType();
    QString getProgress();

    int  getServerId();
    QString  getServerName();
    bool getUsbButtonEnabled();
    bool getHomeEnabled();
    bool getShowLogWindow();
    bool getShowUSBAbort();
    bool getInstallRunning();
    bool getShowAdministration();

    bool getFLSDesk();
    bool getEESM();
    bool getDemoPDLMode();

    QString getLogText();

    QString getPdlName();
    QString getLastSync();
    QString getUpdateStatus();
    QString getUpdateStartTime();
    QString getUpdateProgress();

    QString getUploadStatus();
    QString getLDR();
    QString getLRU();
    QString getTx();
    QString getRx();
    QString getDis();
    QString getLast();
    QString getReqs();
    QString getMode();
    QString getTotBlocks();
    QString getDisk();
    QString getDisks();
    QString getFile();
    QString getBlock();
    QString getBlocks();
    QString getI();
    QString getN();
    QString getL();
    QString getB();

    Q_PROPERTY(QString pdlName       READ getPdlName                              NOTIFY pdlNameChanged)
    Q_PROPERTY(QString lastSync      READ getLastSync                             NOTIFY lastSyncChanged)

    Q_PROPERTY(bool homeEnabled      READ getHomeEnabled                          NOTIFY showHomeEnabledChanged)
    Q_PROPERTY(int serverId          READ getServerId                             NOTIFY showServerIdChanged)
    Q_PROPERTY(QString serverName    READ getServerName                           NOTIFY showServerNameChanged)

    Q_PROPERTY(QString airline       READ getAirline       WRITE setAirline       NOTIFY airlineChanged)
    Q_PROPERTY(QString actype        READ getAcType        WRITE setACType        NOTIFY actypeChanged)
    Q_PROPERTY(QString tailsign      READ getTailsign      WRITE setTailsign      NOTIFY tailsignChanged)
    Q_PROPERTY(QString ataChapter    READ getAtaChapter    WRITE setAtaChapter    NOTIFY ataChapterChanged)
    Q_PROPERTY(QString ataSection    READ getAtaSection    WRITE setAtaSection    NOTIFY ataSectionChanged)
    Q_PROPERTY(QString software      READ getSoftware      WRITE setSoftware      NOTIFY softwareChanged)
    Q_PROPERTY(QString softwareTitle READ getSoftwareTitle WRITE setSoftwareTitle NOTIFY softwareTitleChanged)
    Q_PROPERTY(QString softwareCoC   READ getSoftwareCoC   WRITE setSoftwareCoC   NOTIFY softwareCocChanged)

    Q_PROPERTY(QString usbButtonText READ getUsbButtonText                        NOTIFY usbButtonTextChanged)

    Q_PROPERTY(QString status        READ getStatus                               NOTIFY statusChanged)
    Q_PROPERTY(QString statusColor   READ getStatusColor                          NOTIFY statusColorChanged)
    Q_PROPERTY(QString buildInfo     READ getBuildInfo                            NOTIFY buildInfoChanged)
    Q_PROPERTY(QString startTime     READ getStartTime                            NOTIFY startTimeChanged)
    Q_PROPERTY(QString remainTime    READ getRemainingTime                        NOTIFY remainTimeChanged)
    Q_PROPERTY(QString elapsedTime   READ getElapsedTime                          NOTIFY elapsedTimeChanged)
    Q_PROPERTY(QString connectionType   READ getConnectionType                         NOTIFY connectionTypeChanged)
    Q_PROPERTY(QString progress      READ getProgress                             NOTIFY progressChanged)

    Q_PROPERTY(QString demoPDLMode     READ getDemoPDLMode  NOTIFY demoPDLModeChanged)
    Q_PROPERTY(QString updateStatus    READ getUpdateStatus NOTIFY updateStatusChanged)
    Q_PROPERTY(QString updateProgress  READ getUpdateProgress NOTIFY updateProgressChanged)
    Q_PROPERTY(QString updateStartTime READ getUpdateStartTime NOTIFY updateStartTimeChanged)

    Q_PROPERTY(QString uploadStatus    READ getUploadStatus NOTIFY uploadStatusChanged)

    Q_PROPERTY(bool installRunning READ getInstallRunning NOTIFY installRunningChanged)
    Q_PROPERTY(bool updateRunning  READ getUpdateRunning NOTIFY updateRunningChanged)

    Q_PROPERTY(bool usbButtonEnabled READ getUsbButtonEnabled NOTIFY showUsbButtonEnabled)
    Q_PROPERTY(bool showLogWindow    READ getShowLogWindow    NOTIFY showLogWindowChanged)
    Q_PROPERTY(bool showUSBAbort     READ getShowUSBAbort     NOTIFY showUSBAbortChanged)
    Q_PROPERTY(QString logText       READ getLogText          NOTIFY logTextChanged)

    Q_PROPERTY(bool showAdministration READ getShowAdministration NOTIFY showAdminChanged )
    Q_PROPERTY(QString li_LDR        READ getLDR        NOTIFY LDRChanged)
    Q_PROPERTY(QString li_LRU        READ getLRU        NOTIFY LRUChanged)
    Q_PROPERTY(QString li_Tx         READ getTx         NOTIFY TxChanged)
    Q_PROPERTY(QString li_Rx         READ getRx         NOTIFY RxChanged)
    Q_PROPERTY(QString li_Dis        READ getDis        NOTIFY DisChanged)
    Q_PROPERTY(QString li_Last       READ getLast       NOTIFY LastChanged)
    Q_PROPERTY(QString li_Reqs       READ getReqs       NOTIFY ReqsChanged)
    Q_PROPERTY(QString li_Mode       READ getMode       NOTIFY ModeChanged)
    Q_PROPERTY(QString li_TotBlocks  READ getTotBlocks  NOTIFY TotBlocksChanged)
    Q_PROPERTY(QString li_Disk       READ getDisk       NOTIFY DiskChanged)
    Q_PROPERTY(QString li_Disks      READ getDisks      NOTIFY DisksChanged)
    Q_PROPERTY(QString li_File       READ getFile       NOTIFY FileChanged)
    Q_PROPERTY(QString li_Block      READ getBlock      NOTIFY BlockChanged)
    Q_PROPERTY(QString li_Blocks     READ getBlocks     NOTIFY BlocksChanged)
    Q_PROPERTY(QString li_I          READ getI          NOTIFY IChanged)
    Q_PROPERTY(QString li_N          READ getN          NOTIFY NChanged)
    Q_PROPERTY(QString li_L          READ getL          NOTIFY LChanged)
    Q_PROPERTY(QString li_B          READ getB          NOTIFY BChanged)

    Q_PROPERTY(bool flsDeskChecked  READ getFLSDesk WRITE setFLSDesk NOTIFY flsDeskChanged)
    Q_PROPERTY(bool eescmChecked READ getEESM WRITE setEESCM NOTIFY eescmChanged)
};

#endif // BACKEND_H
