/****************************************************************************
 **
 ** Copyright (C) 2012 Nokia Corporation and/or its subsidiary(-ies).
 ** Contact: http://www.qt-project.org/
 **
 ** This file is part of the examples of the Qt Toolkit.
 **
 ** $QT_BEGIN_LICENSE:BSD$
 ** You may use this file under the terms of the BSD license as follows:
 **
 ** "Redistribution and use in source and binary forms, with or without
 ** modification, are permitted provided that the following conditions are
 ** met:
 **   * Redistributions of source code must retain the above copyright
 **     notice, this list of conditions and the following disclaimer.
 **   * Redistributions in binary form must reproduce the above copyright
 **     notice, this list of conditions and the following disclaimer in
 **     the documentation and/or other materials provided with the
 **     distribution.
 **   * Neither the name of Nokia Corporation and its Subsidiary(-ies) nor
 **     the names of its contributors may be used to endorse or promote
 **     products derived from this software without specific prior written
 **     permission.
 **
 ** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 ** "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 ** LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 ** A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 ** OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 ** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 ** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
 ** DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 ** THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 ** (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 ** OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE."
 **
 ** $QT_END_LICENSE$
 **
 ****************************************************************************/

#include "certificateinfo.h"
#include "sslclient.h"
#include "sslclientbase.h"
#include "hide.h"
#include "loadinfo.h"
#include "config.h"
#include "Backend.h"
#include "setting.h"
#include "./helpers/confighelper.h"

#include <QtGui/QScrollBar>
#include <QtGui/QStyle>
#include <QtGui/QToolButton>
#include <QtNetwork/QSslCipher>
#include <QFile>
#include <QDebug>
#include <QIODevice>
#include <QTimer>

#ifdef Q_WS_WIN
#include <time.h>
#endif

#ifdef Q_WS_WIN32
#define SNPRINTF _snprintf
#include "Windows.h"
#else
#define SNPRINTF ::snprintf
#endif

SslClient::SslClient(QObject *parent)
    : SslClientBase(parent), mReply(0), executingDialog(false)
{
    script          = mFi.getScriptName();
    mEndOfMessage.setPattern("--uuid:[0-9a-f]{8}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{4}-[0-9a-f]{12}--");
    mDatePattern.setPattern("20[0-9]{2}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9][+-][01][0-9]:[0-5][0-9]");
    mDatePatternMs.setPattern("20[0-9]{2}-[0-1][0-9]-[0-3][0-9]T[0-2][0-9]:[0-5][0-9]:[0-5][0-9].[0-9]{3}[+-][01][0-9]:[0-5][0-9]");
    mpReply         = NULL;
    process         = NULL;
    mLoadInfo       = NULL;
    mUp2Date        = false;
    mPerBase        = 0;
    mDiskAct        = 0;
    mDiskExp        = 0;

    initStrings();
    init();

    mSyncStatii.append("SYNCHRONISING");
    mSyncStatii.append("METADATA RETRIEVAL COMPLETE");
    mSyncStatii.append("UPLOADING");
    mSyncStatii.append("SYNC COMPLETE");
    mSyncStatii.append("SYNC ABORTED BY USER");
    mSyncStatii.append("SYNC FAILED");
    mSyncStatii.append("SYNC TIMEOUT");
    mSyncStatii.append("NETWORK SETUP");
    mSyncStatii.append("LOGIN");
    mSyncStatii.append("RETRIEVING METADATA");
    mSyncStatii.append("PROCESSING METADATA");
    mSyncStatii.append("DOWNLOADING");

    mSyncLogmsg.append("Synchronising");
    mSyncLogmsg.append("Metadata retrieval complete");
    mSyncLogmsg.append("Checking for uploads");
    mSyncLogmsg.append("Synchronisation complete");
    mSyncLogmsg.append("Synchronisation aborting. Please wait ...");
    mSyncLogmsg.append("Synchronisation failed");
    mSyncLogmsg.append("Synchronisation timeout");
    mSyncLogmsg.append("Setting up network connection");
    mSyncLogmsg.append("Login");
    mSyncLogmsg.append("Retrieving metadata");
    mSyncLogmsg.append("Processing metadata");
    mSyncLogmsg.append("Downloading");


    connect(this,       SIGNAL(startLogin()),        this,       SLOT(startSynchronisationLogin()));
    connect(&timer,     SIGNAL(timeout()),           this,       SLOT(timeout()));
    connect(this,       SIGNAL(processMessage()),    this,       SLOT(handleMessage()),        Qt::QueuedConnection);
    connect(this,       SIGNAL(startSync()),         this,       SLOT(startSynchronisation()), Qt::QueuedConnection);
    connect(this,       SIGNAL(checkPN(QString)),    &mCheckRep, SLOT(checkPN(QString)),       Qt::QueuedConnection);
    connect(&mCheckRep, SIGNAL(PNOk(QString, bool)), this,       SLOT(PNOk(QString, bool)),    Qt::QueuedConnection);

    connect(ConfigHelper::instance(), SIGNAL(reply(QString, QString)), this, SLOT(setConfigValue(QString, QString)));
}

SslClient::~SslClient()
{

}

void SslClient::init() {
    tempfile        = mFi.getTempName("login");
    mPerBase        = 0;
    mDiskAct        = 0;
    mDiskExp        = 0;
    mSyncThread     = 0;
    mRequestOpen    = false;
    mTimerActive    = false;
    mDelayCount     = 0;

    setReplyProcessor(NULL);
    if (mLoadInfo) {
        delete mLoadInfo;
    }
    mLoadInfo       = NULL;
    disconnect(true);
    disableTimer();

    mProxy.setType(QNetworkProxy::Socks5Proxy);
    mProxy.setHostName("localhost");
    mProxy.setHostName("192.168.20.10");
    mProxy.setPort(8889); // squid ?
    mProxy.setPort(3128); // ssh
    mUseProxy = false;
}

void SslClient::setConfigValue(const QString a, const QString b) {
    qDebug() << "setConfigValue ssl" << a << b << mPerBase;
    if (a == "setNetworkOnline") {
        qDebug() << "emit";
        emit setConnectionType(b);
        if (b.toLower().startsWith("off")) {
            emit addLog("No network available");
            stopSync("Synchronisation terminated", "TIMEOUT");
            setSyncStatus(SYNCST_TIMEOUT);
            return;
        }
        emit startLogin();
    }
}

// *************************************************************************************
//
// Communication timeout routines
//
// *************************************************************************************

void SslClient::enableTimer() {
    qDebug() << "enableTimer";
    mTimerActive = true;
    startTimer();
}

void SslClient::disableTimer() {
    qDebug() << "disableTimer";
    mTimerActive = false;
    stopTimer();
}

void SslClient::startTimer() {
    if (!mTimerActive)
        return;

    timer.start(45000);
}

void SslClient::stopTimer() {
    qDebug() << "stopTimer";
    timer.stop();
}

void SslClient::timeout() {
    qDebug() << "Timeout";
    disableTimer();
    disconnect();
    stopSync("Connection timeout", "TIMEOUT");
    setSyncStatus(SYNCST_TIMEOUT);
    //emit reloadData();
}
// *************************************************************************************


// *************************************************************************************
//
// Signal handlers
//
// *************************************************************************************
void SslClient::handleMessage()
{
    qDebug() << "-----------------------> handleMessage";
    procMessage();
}

void SslClient::socketStateChanged(QAbstractSocket::SocketState sockstate)
{
    qDebug() << "socketStateChanged" << sockstate;
    if (mpReply != NULL && state != SYNC_EXTRACTDISK && sockstate == QAbstractSocket::UnconnectedState) {
        if (mRequestOpen) {
            emit addLog("Server dropped the connection");
            mRequestOpen=false;
            setSyncStatus(SYNCST_FAILED);
            state = SYNC_NULL;
            disconnect(true);
            return;
        }
        return;
    }
    if (mRequestOpen && state == SYNC_EXTRACTDISK && sockstate == QAbstractSocket::UnconnectedState) {
        // dropped connection
        if (mRetryPartNumber.isEmpty()) {
            mRetryPartNumber = mPartnumber;
            emit addLog("Lost connection - retrying");
            qDebug() << "RetryMedia" << mPartnumber;
            state = SYNC_RETRYPARTNUM;
            mReply.clear();
            procMessage();
            disconnect(true);
            return;
        }
        if (mRetryFailCount >= SYNC_MAXFAILCOUNT) {
            emit addLog("Max fail count reached");
            setSyncStatus(SYNCST_FAILED);
            return;
        }
        mRetryFailCount++;
        emit addLog("Retry failed continuing with next");
        qDebug() << "Retry failed" << mPartnumber;
        mRetryPartNumber.clear();
        state = SYNC_GETNEXTPARTNUM;
        procMessage();
        return;
    }
    if (sockstate == QAbstractSocket::UnconnectedState) {
        disconnect();
        if (mReply.length() > 0) {
            qDebug() << "Reply?:" << mReply;
            qDebug() << "########################################### endOfMessageNOTDetected:";
            procMessage();
        }
    }
}

void SslClient::socketEncrypted()
{
    qDebug() << "socketEncrypted";
}

void SslClient::socketReadyRead()
{
    QByteArray mess = socket->readAll();
    if (state == SYNC_PROCGETMETA) qDebug() << "socketReadyRead:";

    if (0) {
        if (state < 5) {
            qDebug() << "socketReadyRead:" << mess;
        } else {
            //qDebug() << "socketReadyReadLength:" << mess.count();
            //qDebug() << "socketReadyRead:" << mess;
        }
    }

    startTimer();
    if (mpReply) {
        if (mpReply->addData(mess)) {
            mReply.clear();
            mRequestOpen = false;
            disconnect();
            emit processMessage();
            return;
        }
        return;
    }
    // Old message handler
    qDebug() << "Old message handling:";
    mReply.append(mess);
    if (isEndOfMessage()) {
        qDebug() << "endOfMessageDetected:";
        procMessage();
        socket->close();
    }
    //appendString(QString::fromUtf8(mess)); // Message received from server
}

void SslClient::synchroniseWithServer() {
    qDebug() << "synchroniseWithServer";
    emit startSync();
}


void SslClient::PNOk(QString pn, bool pnok) {

}

// *************************************************************************************
//
// redundant
//
// *************************************************************************************

bool SslClient::isEndOfMessage()
{
    int lrep = mReply.length();
    for(int i=-1; i > -12; i--) {
        if (mReply[lrep+i] != '-')
            continue;
        QString t = mReply.mid(lrep + i - 44, 45).constData();
        //qDebug() << "right:" << t;
        return (mEndOfMessage.exactMatch(t)) ? true : false;
    }
    return false;
}
// *************************************************************************************

// *************************************************************************************
//
// Utility functions
//
// *************************************************************************************
void SslClient::saveReply()
{
    QFile f(tempfile);
    if (!f.open(QIODevice::WriteOnly)) {
        //appendString("Can't open:<" + tempfile + ">");
        return;
    }
    f.write(mReply);
    f.close();

/*
    QString tt = tempfile + "_copy";
    f.setFileName(tt);
    if (!f.open(QIODevice::WriteOnly)) {
        //appendString("Can't open:<" + tt + ">");
        return;
    }
    f.write(reply);
    f.close();
*/
    mReply.clear();
}

void SslClient::setReplyProcessor(ReplyProcessor *rp) {
    if (mpReply) {
        delete mpReply;
    }
    mpReply = rp;
    mRequestOpen = (rp) ? true : false;
}

// *************************************************************************************

void SslClient::startSynchronisation() {
    init();

    mLoadInfo   = new LoadInfo();
    mKey        = gBackend->getAccessKey();
    if (mKey.isEmpty()) {
        emit addLog("Access key missing");
        setSyncStatus(SYNCST_FAILED);
        emit setUpdateProgress("0%");
        return;
    }

    if (mFi.isAbortTest()) {
        *((char *)0) = 0;
    }

    commode = COM_SYNC;

    setReplyProcessor(NULL);

    emit addLog("Connecting to server " + gConfig->getName());
    state = SYNC_SETUPCOMM;
    setSyncStatus(SYNCST_NETWORK_SETUP);
    ConfigHelper::instance()->execute("C setNetworkOnline");
}

void SslClient::startSynchronisationLogin() {
    mRetryFailCount = 0;
    enableTimer();
    setSyncStatus(SYNCST_LOGIN);
    setReplyProcessor(new ReplyProcessorLogin());
    sendMessage(getLogin(), false, true);
}

void SslClient::readFromStdOut()
{
    mReply.append(process->readAllStandardOutput());
}

void SslClient::finishedApp()
{
    qDebug() << "reply" << mReply;
    procMessage();
}

void SslClient::runCommand(QString command)
{
    /* Make sure that the old process will be deleted later */
    if (process)
        process->deleteLater();

    process = new QProcess();

    /* Connect to readyReadStandardOutput to get the output */
    connect(process, SIGNAL(readyReadStandardOutput()), this, SLOT(readFromStdOut()));
    /* Connect to the finished(int) signal to get when the application is done */
    connect(process, SIGNAL(finished(int)), this, SLOT(finishedApp()));

    /* Process start will start the app in this object */
    QString tcom = script + command;
    qDebug() << "Running:" + tcom;
    process->start(tcom);
}

bool SslClient::extractPartnumbers()
{
    // Open load and check for missing swpns
    //gBackend->getDB().openDB();
    gBackend->getDB().loadDB();

    // returns no. of missing swpns (disks).
    mDiskExp = gBackend->getDB().verifySWPNs();

    // open temp table containing missing swpns
    gBackend->getDB().openNewSwpn();

    mPartnumber.clear();
    mDiskAct = 0;
    mSequence = 1;

    return true;
}

bool SslClient::getNextPartNumber()
{
    if (!mPartnumber.isEmpty()) {
        if (++mSequence <= diskcount) {
            qDebug() << mPartnumber << mSequence;
            return true;
        }
    }
    mSequence = 1;
    if (!gBackend->getDB().nextNewSwpn(&diskcount, mPartnumber)) {
        gBackend->getDB().verifySWPNs(true);
        return false;
    }
    qDebug() << "Next:" << diskcount << mPartnumber;
    return true;
}

void SslClient::procMessage()
{
    switch(commode) {
    case COM_SYNC:     return procMessage_sync();
    case COM_SWUPDATE: return procMessage_swupdate();
    }
}

void SslClient::procMessage_swupdate()
{

}

void SslClient::setSyncStatus(syncstates_t status) {

    qDebug() << "setSyncStatus" << status << mSyncLogmsg[status] << mSyncStatii[status];

    emit addLog(mSyncLogmsg[status]);
    gBackend->setUpdateStatus(mSyncStatii[status]);

    switch(status) {
    case SYNCST_NETWORK_SETUP:
        break;
    case SYNCST_LOGIN:
        break;
    case SYNCST_RETRIEVE_META:
        break;
    case SYNCST_PROC_META:
        break;
    case SYNCST_SYNCHRONISING:
        break;
    case SYNCST_DOWNLOADING:
        break;
    case SYNCST_UPLOADING:
        break;
    case SYNCST_COMPLETE:
        state = SYNC_NULL;
        disableTimer();
        disconnect();
        setProgress();
        emit setLastSync(time(0));
        emit setUpdateProgress("100%");
        gBackend->setUpdating(false);
        ConfigHelper::instance()->execute("C GetRepositoryInfo");
        gBackend->setHomeEnabled(true);
        if (mNewSoftware) {
            emit addLog("Installing new software version");
            startSWUpdate();
        }
        break;

    case SYNCST_ABORTED:
        mRequestOpen = false;
        state = SYNC_NULL;
        disableTimer();
        disconnect();
        mFi.setVerified(false);
        gBackend->reloadData();
        setProgress();
        emit addLog("Aborted complete");
        gBackend->setSyncTimerExc(5); // Minutes
        gBackend->setUpdating(false);
        gBackend->setHomeEnabled(true);
        ConfigHelper::instance()->execute("C GetRepositoryInfo");
        break;

    case SYNCST_FAILED:
        state = SYNC_NULL;
        disableTimer();
        disconnect();
        mFi.setVerified(false);
        gBackend->reloadData();
        setProgress();
        gBackend->setSyncTimerExc(5); // Minutes
        gBackend->setUpdating(false);
        gBackend->setHomeEnabled(true);
        ConfigHelper::instance()->execute("C GetRepositoryInfo");
        break;

    case SYNCST_TIMEOUT:
        state = SYNC_NULL;
        disableTimer();
        disconnect();
        mFi.setVerified(false);
        gBackend->reloadData();
        setProgress();
        gBackend->setSyncTimerExc(5); // Minutes
        gBackend->setUpdating(false);
        gBackend->setHomeEnabled(true);
        ConfigHelper::instance()->execute("C GetRepositoryInfo");
        break;
    }
}

void SslClient::startSWUpdate()
{
    QProcess pr;
    pr.execute("sh /home/root/loader/bin/doSoftwareUpdate.sh " + mSWCheckSum);
}

void SslClient::stopSync(QString logmsg, QString status)
{
    emit addLog(logmsg);
    emit setUpdateStatus(status);
    emit setUpdating(false);
    setProgress();
    state = SYNC_NULL;
    disableTimer();
}

void SslClient::setProgress()
{
    QString t;
    int expected = mDiskExp + mLoadInfo->getCount();
    int actual   = mDiskAct + mLoadInfo->getCurrCount();

    qDebug() << "************************************* > progress:" << actual << "/" << expected << calcPercent(actual, expected);
    qDebug() << "actual                   :" << actual;

    emit setUpdateProgress(calcPercent(actual, expected));
}


QString SslClient::calcPercent(int n, int from)
{
    QString t;
    if (from == 0) {
        if (mPerBase == 4)
         return "100%";
        t.sprintf("%d%%", mPerBase);
        return t;
    }


    if (n > from)
        n = from;
    //qDebug() << "Perbase  : " << mPerBase;

    t.sprintf("%d%%", ((n * 96) / from) + mPerBase);
    return t;
}

// ######################################### procMessage_sync

void SslClient::checkPartNumber(QString pnfs)
{
    if (pnfs.isEmpty())
        return;

    QString mess;
    QString t;
    bool ok = mCheckRep.checkPartNumber(pnfs, true);
    mess = t.sprintf("%s Verification: %s", (const char*) pnfs.toUtf8(), (ok) ? "OK" : "FAILED");
    emit addLog(mess);
}

// Example string "2013-09-16T16:47:17+02:00"
// Example string "2000-01-01T00:00:00+00:00"
// getSettingsDB().set_db_pdl_change_time(QString("2000-01-01T00:00:00+00:00"));

qint64 SslClient::getUTC(QString datetime)
{
    QDateTime dt = QDateTime::fromString(datetime.left(19), "yyyy-MM-ddTHH:mm:ss");
    qint64 secs  = dt.toMSecsSinceEpoch() / 1000;
    int offset = (mDatePattern.exactMatch(datetime)) ? 19 : 23;

    QTime  dta    = QTime::fromString(datetime.mid(offset+1), "hh:mm");

    int adj      = (((dta.hour() * 60) + dta.minute()) * 60) * ((datetime.mid(offset,1) == "+") ? -1 : 1);
    secs += adj;

    qDebug() << "adjusted" << secs << "adj" << adj << dta.hour() << dta.minute();
    return secs;
}

// ######################################### procMessage_sync

void SslClient::procMessage_sync()
{
    QString mess;
    QString t;
    QString pnfs;

    qDebug() << "STATE ------------------------ " << state;
    if (state != SYNC_NULL) {
        if (!gBackend->isPower()) {
            stopSync("Synchronisation terminated", "POWERLOSS");
            setSyncStatus(SYNCST_ABORTED);
            return;
        }
        if (!gBackend->getUpdateRunning()) {
            stopSync("Synchronisation terminated", "ABORTED");
            setSyncStatus(SYNCST_ABORTED);
            emit reloadData();
            return;
        }
    }

    switch (state) {

    case SYNC_SETUPCOMM:
    case SYNC_LOGIN:    // Login
        mDiskAct = 0;
        mDiskExp = 0;
        mPerBase = 0;
        stopTimer();

    case SYNC_POSTLOGIN:
        if (mpReply) {
            mTicket = mpReply->getValue(QString("loginResponse.clientKey"));
        }

        mReply.clear();
        qDebug() << "Ticket" << mTicket;

        if (mTicket.isEmpty() || !mTicket.endsWith(".ticket")) {
            const QString s("faultstring");
            QString r = mpReply->getValue(s);
            if (r.startsWith("Login failed: "))
                r.remove(0, 14);
            emit addLog(r);
            stopSync("Login failed", "FAILED");
            setSyncStatus(SYNCST_FAILED);
            break;
        }
        mPerBase = 1;
        emit addLog("Login complete");
        emit setUpdateProgress("1%");
        {
            QString rep;
            rep = mpReply->getValue(QString("loginResponse.dbDate"));
            if (!mDatePattern.exactMatch(rep)) {
                qDebug() << "bad match - dbDate" << rep;
            } else {
                gBackend->getSettingsDB().set_db_time(rep);
                qint32 now = (qint32) getUTC(rep);
                setSystemDateTime(now);
                // set PDL time here
            }

            mLastChange.clear();
            rep = mpReply->getValue(QString("loginResponse.lastChange"));
            if (!mDatePattern.exactMatch(rep) and !mDatePatternMs.exactMatch(rep)) {
                qDebug() << "bad match - lastchange" << rep;
            } else {
                QString value;
                gBackend->getSettingsDB().getDBValue(QString("db_pdl_change_time"), value);
                qDebug() << "get_db_pdl_change_time" << value;
                mLastChange = rep;
                qint32 server = (qint32)getUTC(rep);
                qint32 pdl    = (qint32)getUTC(value);
                mUp2Date = (pdl < server) ? false : true;
                //mUp2Date = false;
            }
            mSWCheckSum = mpReply->getValue(QString("pdlLoaderSoftwareInformation.checksum"));

            mNewSoftware = false;
            if (!mSWCheckSum.isEmpty() and !mFi.isSoftwareInstalled(mSWCheckSum)) {
                qDebug() << "Software available";
                goto GETSOFTWARE;
            }
            qDebug() << "Software not available";
            goto SENDCLIENT;

        }

    case SYNC_ISSORTWARE:
        tempfile = mFi.getTempName("clinfo");
        startTimer();
        setReplyProcessor(new ReplyProcessorLogin());
        state = SYNC_ISSORTWAREPROC;
        sendMessage(getIsSoftwareAvailable());
        break;
    case SYNC_ISSORTWAREPROC:
        mNewSoftware = false;
        {
            QString rep;
            rep = mpReply->getValue(QString("available"));
            if (rep == "true") {
                qDebug() << "Software available";
                goto GETSOFTWARE;
            }
        }
        qDebug() << "Software not available";
        goto SENDCLIENT;

GETSOFTWARE:
    case SYNC_GETSORTWARE:
        tempfile = mFi.getTempName("clinfo");
        startTimer();
        setReplyProcessor(new ReplyProcessorSoftware());
        state = SYNC_GETSORTWAREPROC;
        emit addLog("Downloading new software version");
        sendMessage(getGetSoftware());
        break;
    case SYNC_GETSORTWAREPROC:
        state = SYNC_SENDCLINFO;
        mNewSoftware = true;
        emit addLog("New software version downloaded");

SENDCLIENT:
        if (!gConfig->isClInfo())
            goto SENDMETA;

    case SYNC_SENDCLINFO:
        //setSyncStatus(SYNCST_RETRIEVE_META);
        tempfile = mFi.getTempName("clinfo");
        startTimer();
        setReplyProcessor(new ReplyProcessorLogin());
        state = SYNC_PROCCLINFO;
        sendMessage(getClientInformation());
        break;

    case SYNC_PROCCLINFO:
        if (mpReply) {
            Setting pdlsettings;
            pdlsettings.saveSettings(mpReply->getValue(QString ("vendorSpecificSettings")));
            pdlsettings.setValue(QString("sync_interval"), mpReply->getValue(QString ("syncIntervall")));
            pdlsettings.setValue(QString("pdl_name"),      mpReply->getValue(QString ("name")));
            pdlsettings.setValue(QString("clientId"),      mpReply->getValue(QString ("clientInformation.clientId")));
            pdlsettings.setValue(QString("syncEnabled"),   mpReply->getValue(QString ("clientInformation.syncEnabled")));
            pdlsettings.setValue(QString("type"),          mpReply->getValue(QString ("clientInformation.type")));
            pdlsettings.dump2File();
        }

SENDMETA:
        if (mUp2Date) {
            mPerBase += 2;
            emit updateElapsed();
            emit setUpdateProgress("3%");
            stopTimer();
            emit addLog("Metadata up-to-date");
            goto UPLOAD;
        }
    case SYNC_SENDGETMETA:
        setSyncStatus(SYNCST_RETRIEVE_META);
        //emit addLog("Retrieving Metadata");
        tempfile = mFi.getTempName("meta");
        startTimer();
        setReplyProcessor(new ReplyProcessorMeta());
        mpReply->setRepDirectory(mFi.getRepositorySWDir());
        state = SYNC_EXTRACTMETA;
        sendMessage(getMeta(), true);
        break;

    case SYNC_EXTRACTMETA:
        setSyncStatus(SYNCST_RETRIEVE_META_COMPLETE);
        mPerBase += 2;
        emit updateElapsed();
        emit setUpdateProgress("3%");
        stopTimer();
        setSyncStatus(SYNCST_PROC_META);
        emit addLog("Checking for new swpns");
        extractPartnumbers();
        emit addLog("Metadata processing complete");
        state = SYNC_GETNEXTPARTNUM;
        setSyncStatus(SYNCST_DOWNLOADING);
        processMessage();
        break;

NEXTPN:
    case SYNC_GETNEXTPARTNUM:
        setProgress();
        pnfs = mPartnumber;
        if (!getNextPartNumber()) {
            checkPartNumber(pnfs);
            emit addLog("Download complete - reloading DB");
            if (mRetryFailCount == 0)
                gBackend->getSettingsDB().set_db_pdl_change_time(mLastChange);
            mSyncThread->mMySleep(500);
            emit reload();
            goto UPLOAD;
        }
        if (!pnfs.isEmpty() && pnfs != mPartnumber) {
            checkPartNumber(pnfs);
        }
    case SYNC_RETRYPARTNUM:
        if (mDelayCount++ >= DOWNLOAD_INTV) {
            qDebug() << "Start Delay";
            //mSyncThread->mMySleep(DOWNLOAD_TIME);
            qDebug() << "End Delay";
            mDelayCount=0;

        }

        // Partnumber enter as they come from man. system and must have their / converted to % and ' ' to @.
        pnfs = mPartnumber;
        pnfs.replace('/', '%');
        pnfs.replace(' ', '@');

        mSWPath = t.sprintf("%s/SW/%s", (const char*) mFi.getRepositoryDir().toUtf8(),  (const char*) pnfs.toUtf8());
        mess = t.sprintf("%s Downloading disk %d/%d", (const char*) mPartnumber.toUtf8(), mSequence, diskcount);
        emit addLog(mess);
        tempfile = mFi.getTempName("media");
        startTimer();

        setReplyProcessor(new ReplyProcessorMedia());
        mpReply->setDirectory(mFi.getRepositorySWDir(), pnfs);

        mReply.clear();
        sendMessage(getMedia(mPartnumber, mSequence));
        state = SYNC_EXTRACTDISK;
        break;

    case SYNC_EXTRACTDISK:
        stopTimer();
        mDiskAct++;
        state = SYNC_GETNEXTPARTNUM;
        goto NEXTPN;

        // These are the upload cases
UPLOAD:
    case UPLD_STARTUPLOAD:
        setSyncStatus(SYNCST_UPLOADING);
        qDebug() << "count" <<  mLoadInfo->getCount();
        if (mLoadInfo->getCount() == 0) {
            emit addLog("Nothing to upload");
            stopSync("Upload complete", "COMPLETE");
            syncComplete();
            return;
        }

        state = UPLD_SETUPLOADED;
        goto NEXTUPLOAD;

    case UPLD_SETUPLOADED:
        mLoadInfo->setUploaded();

NEXTUPLOAD:
        if (!mLoadInfo->getNextLoad()) {
            stopSync("Upload complete", "COMPLETE");
            syncComplete();
            break;
        }
        qDebug() << "Uploading:" << mLoadInfo->getCurrDir();
        mess = t.sprintf("Uploading %s", (const char*)mLoadInfo->getCurrDir().toUtf8());
        emit addLog(mess);
        tempfile = mFi.getTempName("upload");
        setReplyProcessor(new ReplyProcessorLogin());
        sendMessageMTOM(getUpload());
        break;
    }
}

void SslClient::syncComplete() {
    mPerBase += 1;
    emit setUpdating(false);
    gBackend->mFi.inSync(false);
    gBackend->setHomeEnabled(true);
    state = SYNC_PROCESSLOGOFF;

    setSyncStatus(SYNCST_COMPLETE);

    setReplyProcessor(new ReplyProcessorLogin());
    //if (gConfig->isFLSDesk())
        //sendMessage(getLogoff(1));
    //else
    sendMessage(getLogout(1));
    disableTimer();
}

void SslClient::setSystemDateTime(qint32 time) {
    QDateTime now = QDateTime::fromTime_t(time);
    now.setTimeSpec(Qt::UTC);
    QString dt = "date " + now.toString("MMddhhmmyyyy.ss");
    qDebug() << "setSystemDateTime" << dt << time;
    QProcess p;
    p.execute(dt);

    QProcess h;
    h.execute(QString("hwclock -w"));
}

