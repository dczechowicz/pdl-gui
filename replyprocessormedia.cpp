#include "replyprocessormedia.h"
#include <QDebug>

ReplyProcessorMedia::ReplyProcessorMedia()
{
}

bool ReplyProcessorMedia::addData(QByteArray & nData) {
    if (mCompleted)
        return false;

    mInOffset     = 0;
    mLocalBuffer.append(nData);
    mLength       = mLocalBuffer.length();
    bool ret      = false;

    if (mDebug) qDebug() << "nData.length" << nData.length() << mLength;

    dumpData(nData);
    if (mState == PROC_FIRST_HEADER) {
        if (mLength < 1700)
            return ret;
        mState = PROC_FIRST_DATA;
        getHeader(mLocalBuffer);
    }
    if (mDebug)  qDebug() << "mInOffset" << mInOffset;
    bool end = (mChunked) ? getChunked(mLocalBuffer) : getData(mLocalBuffer);
    mLocalBuffer.clear();

    mFileOffset += mLength;
    write2File();

    // true = end of message (file)
    if (mFileSize == 0) {
        qDebug() << "endOfMessageDetected Media:";
        closeFile();
        ret = true;
    }
    return ret;
}

int ReplyProcessorMedia::getMessageInfo() {
    int offset = 0;
    QByteArray line;
    bool xmlfnd=false;
    int mtlc=0;
    while (true) {
        offset = getLineGen(mBuffer, offset, line);
        qDebug() << offset << "line:" << line;

        if (line.isEmpty()) {
            qDebug() << "Line empty";
            mtlc++;
            if (mtlc > 5) {
                qDebug() << "MAX line";
                return offset;
            }
            if (xmlfnd) {
                if (mBuffer[offset] != 'P' || mBuffer[offset+1] != 'K')
                    continue;
                //if (mDebug)
                    qDebug() << mDumpCount << "File offset:" << hex << offset;
                mBuffer = mBuffer.mid(offset);
                if (mDebug) qDebug() << "buffer" << hex << mBuffer;
                return offset;
            }
            continue;
        }
        if (line.startsWith("<soap")) {
            if (mDebug)
                qDebug() << "XML" << line;
            xmlfnd=true;
            mMediaInf.loadInfo(line);
            mMediaInf.buildDiskInfoFile(mSWPath);
            mFileSize = mMediaInf.getValue("filesize").toInt();
            mLimit    = 250000;
            mCurr     = 0;
        }
    }
}
