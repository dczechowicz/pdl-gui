// import QtQuick 1.0 // to target S60 5th Edition or Memo 5
import QtQuick 1.1
import Network 1.0
import "../colibri"
import "./Components"
import "../js/Styler.js" as Styler

Rectangle {
    id: main
    width: 480
    height: 800
    x: 0
    y: 0

    property string oldState: ""

    Connections {
        target: backend

        onScreenStateChanged: {
            if (!turnedOn)
                fakeTouchPage.visible = true;
            else
                fakeTouchPage.visible = false;
        }

        onAutoSyncStarted: {
            if (main.state != "UpdateRepository") {
                main.oldState = main.state;
                main.state = "UpdateRepository";
            }
        }
    }


    HomePage {
        id: homePage

        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0

        onDataLoadManualClicked: {
            backend.resetAll();
            main.state = "DataLoadMan";
        }
        onDataLoadClicked: {                 
            backend.clearAuto();        
            main.state = "DataLoadPage";      
        }                              
        onDataLoadUsbClicked: {
            dataLoadUsbPage.reset();
            main.state = "DataLoadUsb";
        }

        onSecureUsbClicked: {
            backend.resetAll();
            main.state = "SecureUsb";
        }

        onSystemClicked: {
            backend.clearAuto();
            main.state = "SystemPage";
        }

        onUpdateRepositoryClicked: {
            backend.clearAuto();
            backend.resetAll();
            main.oldState = main.state;
            main.state = "UpdateRepository";
        }

        onInformationClicked: {
            backend.clearAuto();
            main.oldState = main.state;
            main.state = "Information";
        }

        onRepositoryClicked: {
            backend.clearAuto();
            main.state = "RepositoryPage";
        }

        onInternalClicked: {
            backend.clearAuto();
            main.state = "InternalPage";
        }
    }


///* XXX
    MessageBox {
        id: messageBox

        z: 100

        anchors {
            bottom: parent.bottom
        }

        onAccepted: backend.shutdown();
    }

    WiFiConfiguration {
        id: wiFiConfig
    }

    FakeTouchPage {
        id: fakeTouchPage

        anchors.fill: parent

        z: 1000

        visible: false

        onClicked: fakeTouchPage.visible = false
    }

    A615ADataLoadPage {
        id: a615ADataLoadPage

        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
    }

    A615ADataLoadManPage {
        id: a615ADataLoadManPage

        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
    }

    ScreenInformation {
        id: screen_info

        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
        visible: false

        onInternalClicked: {
            backend.clearAuto();
            main.state = "InternalPage";
        }
    }
    DataLoadPage {
        id: dataLoadPage

        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0

        onA6153MenuSelection: {
            backend.resetAll();
            screen_dataload_1.airlineText = "";
            main.state = "DataLoad_1";
        }

        onA6153ManualSelection: {
            backend.resetAll();
            main.state = "DataLoadMan";
        }

        onSecUsbMenuSelection: {
            backend.resetAll();
            dataLoadUsbPage.reset();
            main.state = "DataLoadUsb";
        }

        onSecUsbManualSelection: {
            backend.resetAll();
            main.state = "DataLoadUsbManPage";
        }

        onA615AMenuSelection: {
            backend.resetAll();
            a615ADataLoadPage.airlineText = "";
            main.state = "A615ADataLoadPage";
        }

        onA615AManualSelection: {
            main.state = "A615ADataLoadManPage";
        }
    }

    DataLoadUsbManPage {
        id: dataLoadUsbManPage

        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
    }

    SystemPage {
        id: systemPage
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0

        onUpdateRepositoryClicked: {
            backend.resetAll();
            main.oldState = main.state;
            main.state = "UpdateRepository";
        }
        onUploadResultsClicked:    {
            backend.resetAll();
            main.oldState = main.state;
            main.state = "UploadResults";
        }
    }

    ScreenMain {
        id: screen_home
        visible: false
        color: Styler.Colors.Background.Color
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
        border.color: "#000000"

        onDataLoadManualClicked:   {
            backend.resetAll();
            main.state = "DataLoadMan";
        }
        onDataLoadClicked:         {
            backend.resetAll();
            main.state = "DataLoad_1";
        }
        onSecureUsbClicked:        {
            backend.resetAll();
            main.state = "SecureUsb"; }

        onUpdateRepositoryClicked: {
            backend.resetAll();
            main.state = "UpdateRepository";
        }
        onUploadResultsClicked:    {
            backend.resetAll();
            main.state = "UploadResults";
        }
        onAdministrationClicked:   { main.state = "Administration"; }
        onInformationClicked:      { main.state = "Information"; }

        onShutdownClicked:         {
            backend.shutdown();
        }
        onRestartClicked:          {
            backend.restart();
        }
    }

    ScreenDataLoadMan {
        id: screen_dataloadman
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0

        property string currentType;

        onTailChanged: backend.checkTail();
        onSwpnChanged: backend.checkSwpn();

        onSelectData: {
            currentType = name;
            screen_dataselect.loadData(name);
            screen_dataselect.selectItem(selected);

            main.oldState = main.state;
            main.state = "DataSelect";
        }

        function test(name)
        {
            screen_dataselect.selectItem(name);
            setData(currentType, name);
        }
    }

    ScreenDataLoad_1 {
        id: screen_dataload_1
        visible: false
//        color: Styler.Colors.Background.Color
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
//        border.color: "#000000"

        property string currentType;

        onSelectData: {
            currentType = name;
            screen_dataselect.loadData(name);
            screen_dataselect.selectItem(selected);

            main.oldState = main.state;
            main.state = "DataSelect";
        }

        function test(name, value)
        {
            screen_dataselect.selectItem(name);
            if (currentType == "Airline") {
                screen_dataload_1.airlineText = value;
            }
            setData(currentType, name);
        }
    }

    DataLoadUsbPage {
        id: dataLoadUsbPage
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0

        property string currentType;

        onSelectData: {
            currentType = name;
            screen_dataselect.loadData(name);
            screen_dataselect.selectItem(selected);

            main.oldState = main.state;
            main.state = "DataSelect";
        }

        function test(name, value)
        {
            screen_dataselect.selectItem(name);
            if (currentType == "Airline") {
                dataLoadUsbPage.airlineText = value;
            }
            setData(currentType, name);
        }
    }

    ScreenUpdateFirmware {
        id: screen_updatefirmware;
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0

        property string currentType;

        onSelectData: {
            console.log("Test:" + name + " select: " + selected);

            currentType = name;
            screen_dataselect.loadData(name);
            screen_dataselect.selectItem(selected);

            main.oldState = main.state;
            main.state = "DataSelect";
        }

        function test(name)
        {
            console.log("all is wel: " + name);
            screen_dataselect.selectItem(name);
            setData(currentType, name);
        }
    }

    ScreenDataSelect {
        id: screen_dataselect;
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0

        onDataSelected: {
            console.log("Selected: " + name);

            if (main.oldState == "DataLoad") {
                screen_dataload.test(name);
            }
            else if (main.oldState == "DataLoadMan") {
                screen_dataloadMan.test(name);
            }
            else if (main.oldState == "DataLoad_1") {
                screen_dataload_1.test(name, value);
            }
            else if (main.oldState == "SecureUsb") {
                screen_secureUSb.test(name);
            }
            else if (main.oldState == "UpdateRepository") {
                screen_updatefirmware.test(name)
            }
            else if (main.oldState == "Administration") {
                screen_administration.test(name)
            }
            else if (main.oldState == "DataLoadUsb") {
                dataLoadUsbPage.test(name, value);
            }

            main.state = main.oldState;
        }
    }

    RepositoryPage {
        id: repositoryPage

        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
    }

    DebugSettingScreen {
        id: screen_debugSetting
        visible: false
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 0
        anchors.top: header.bottom
        anchors.topMargin: 0
    }
//XXX */

    Rectangle {
        id: header
        width: 480
        height: 50
        gradient: Gradient {
            GradientStop {
                position: 0
                color: "#646464"
            }

            GradientStop {
                position: 0.340
                color: "#000000"
            }

            GradientStop {
                position: 0.750
                color: "#000000"
            }

            GradientStop {
                position: 1
                color: "#646363"
            }
        }
        transformOrigin: Item.Center
        visible: true
        border.color: "#000000"
        border.width: 0
        anchors.right: parent.right
        anchors.rightMargin: 0
        anchors.left: parent.left
        anchors.leftMargin: 0
        anchors.top: parent.top
        anchors.topMargin: 0

        Text {
            id: header_name
            color: Styler.Fonts.Header.Color
            text: qsTr("Home")
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            anchors.fill: parent

            font {
                family: Styler.Fonts.Family
                weight: Styler.Fonts.Header.Weight == "Bold" ? Font.Bold : Font.Normal
                pixelSize: Styler.Fonts.Header.PixelSize
            }
        }

        Button {
            id: header_lock
            x: 361
            y: 15
            width: 94
            height: 20
            color: "#000000"
            radius: 15
            border.width: 2
            border.color: "#808080"
            anchors.top: parent.top
            anchors.topMargin: 10
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            anchors.right: parent.right
            anchors.rightMargin: 25
            text: qsTr("Lock Screen");
            textSize: 8
            visible: false
            //onClicked: main.state = "Login";
        }

        Button {
            id: header_home
            y: 12
            width: 94
            height: 20
            color: "#424891"
            radius: 15
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 10
            border.color: "#808080"
            text: qsTr("Home");
            textSize: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            border.width: 2

            onClicked: {
                if (backend.homeEnabled) {
                    main.state = "Home";
                }
            }
        }
        Button {
            id: header_back
            y: 12
            width: 94
            height: 20
            color: "#424891"
            radius: 15
            anchors.left: parent.left
            anchors.leftMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 10
            border.color: "#808080"
            text: qsTr("Back");
            textSize: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            border.width: 2

            visible: false
            onClicked: {
                if (backend.homeEnabled) {
                    if (main.state == "UploadResults") {
                        screen_uploadResults.visible = false;
                        main.oldState = "Home";
                        main.state = "SystemPage";
                    } /*else if (main.state == "UpdateRepository") {
                        screen_updatefirmware.visible = false;
                        main.oldState = "Home";
                        main.state = "SystemPage";
                    }*/ else {
                        main.state = main.oldState;
                    }
                }
            }
        }

        Button {
            id: header_purge
            y: 12
            width: 94
            height: 20
            color: "#424891"
            radius: 15
            anchors.right: parent.right
            anchors.rightMargin: 5
            anchors.top: parent.top
            anchors.topMargin: 10
            border.color: "#808080"
            text: qsTr("Purge");
            textSize: 8
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 10
            border.width: 2
            visible: false
            onClicked: {
                messageBoxClearRepository.show(qsTr("Do you really want to purge repository? WARNING: After repository has been purged a complete synchronisation is needed in order to use PDL for data load."), qsTr("Yes"), qsTr("No"));
            }
        }

    }

    MessageBox {
        id: messageBoxClearRepository
        z: 100
        anchors {
            bottom: parent.bottom
        }
        onAccepted: {
            backend.purgeRepository()
            repositoryPage.visible = false;
            repositoryPage.visible = true;
            messageBoxClearRepository.visible = false;
        }
        onRejected: messageBoxClearRepository.visible = false;
    }

    CLKeyboard {
        id: keyboard

        anchors {
            left: parent.left
            right: parent.right
            bottom: parent.bottom
            margins: 5
        }

        z: 100

        color: "white"
        radius: 10

        visible: false

        onOkCLButtonClicked: keyboard.visible = false;
        onCancelCLButtonClicked: keyboard.visible = false;
        onLeftArrowClicked: keyboardHelper.pressKey("", Qt.Key_Left);
        onRightArrowClicked: keyboardHelper.pressKey("", Qt.Key_Right);
        onBackspaceClicked: keyboardHelper.pressKey("", Qt.Key_Backspace);
    }

    state: "Home"
    //state: main.state = backend.mCurrPage

    onStateChanged: keyboard.visible = false

    states: [
        State {
            name: "Home"
            PropertyChanges { target: homePage; visible: true }

            PropertyChanges { target: header_home; visible: false }
            PropertyChanges { target: header_name; text: "Home" }
        },
        State {
            name: "SystemPage"
            PropertyChanges { target: systemPage; visible: true }
            PropertyChanges { target: header_home; visible: true }

            PropertyChanges { target: header_name; text: "System" }
        },
        State {
            name: "DataLoad"
            PropertyChanges { target: screen_dataload; visible: true }
            PropertyChanges { target: header_name; text: "Data Load" }
        },
        State {
            name: "DataLoadMan"
            PropertyChanges { target: screen_dataloadman; visible: true }
            PropertyChanges { target: header_name; text: "Data Load" }
        },
        State {
            name: "DataLoad_1"
            PropertyChanges { target: screen_dataload_1; visible: true }
            PropertyChanges { target: header_name; text: "Data Load" }
        },
        State {
            name: "DataSelect"
            PropertyChanges { target: screen_dataselect; visible: true }
            PropertyChanges { target: header_back; visible: true }

            PropertyChanges { target: header_name; text: screen_dataselect.displayName }
        },
        State {
            name: "SecureUsb"
            PropertyChanges { target: screen_secure  ; visible: true }

            PropertyChanges { target: header_name; text: "Secure USB" }
        },
        State {
            name: "UpdateRepository"
            PropertyChanges { target: screen_updatefirmware; visible: true }
            PropertyChanges { target: header_back; visible: true }

            PropertyChanges { target: header_name; text: "Synchronisation" }
        },
        State {
            name: "Administration"
            PropertyChanges { target: screen_administration; visible: true }
            PropertyChanges { target: header_back; visible: true }

            PropertyChanges { target: header_name; text: "Administration" }
        },
        State {
            name: "Information"
            PropertyChanges { target: screen_info; visible: true }
            PropertyChanges { target: header_back; visible: true }

            PropertyChanges { target: header_name; text: "Information" }
        },
        State {
            name: "DataLoadUsb"
            PropertyChanges { target: dataLoadUsbPage; visible: true }
            PropertyChanges { target: header_name; text: "Data Load(USB)" }
        },
        State {
            name: "DataLoadPage"
            PropertyChanges { target: dataLoadPage; visible: true }
            PropertyChanges { target: header_name; text: "Data Load" }
        },
        State {
            name: "DataLoadUsbManPage"
            PropertyChanges { target: dataLoadUsbManPage; visible: true }
            PropertyChanges { target: header_name; text: "USB Manual" }
        },
        State {
            name: "RepositoryPage"
            PropertyChanges { target: repositoryPage; visible: true }
            PropertyChanges { target: header_name; text: "Repository" }
            PropertyChanges { target: header_purge; visible: true }

        },
        State {
            name: "A615ADataLoadPage"
            PropertyChanges { target: a615ADataLoadPage; visible: true }
            PropertyChanges { target: header_name; text: "Data Load" }
        },
        State {
            name: "A615ADataLoadManPage"
            PropertyChanges { target: a615ADataLoadManPage; visible: true }
            PropertyChanges { target: header_name; text: "Data Load(Manual)" }
        },
        State {
            name: "InternalPage"
            PropertyChanges { target: screen_debugSetting; visible: true }
            PropertyChanges { target: header_name; text: "Internal" }

        }
    ]

}
