// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import Models 1.0
import "./Components"

Page {
    id: container

    property string airlineText: ""

    signal selectData( string name, string selected)

    function setData(datatype, value)
    {
        if (datatype == "Airline")
        {
            backend.airline = value;
        }
        else if (datatype == "ACType")
        {
            backend.actype = value;
        }
        else if (datatype == "Tailsign")
        {
            backend.tailsign = value;
        }
        else if (datatype == "ATA Chapter")
        {
            backend.ataChapter = value;
        }
        else if (datatype == "ATA Section")
        {
            backend.ataSection = value;
        }
//        else if (datatype == "HWFIN")
//        {
//            backend.hwfin = value;
//        }
        else if (datatype == "Software")
        {
            backend.software = value;
        }
    }

    Flickable {
        anchors.topMargin: 1
        anchors.fill: parent
        contentHeight: column.childrenRect.height + 20
        Column {
            id: column
            anchors.horizontalCenter: parent.horizontalCenter
            HeaderText {
                text: "Select Software"
            }

            GroupBox {
                radius: 10
                border.width: 2
                border.color: "blue"

                color: "lightblue"

                MenuItem {
                    id: bsAirline
                    width: 390
                    text: "Airline"
                    value: airlineText;
                    enabled: !backend.installRunning

                    onClicked: container.selectData("Airline", value);
                }

                MenuItem {
                    id: bsACType
                    width: 390
                    text: "A/C Model"
                    value: backend.actype;
                    enabled: !backend.installRunning
                    showSelector: backend.airline != ""
                    clickable: backend.airline != ""

                    onClicked: container.selectData("ACType", value);
                }

                MenuItem {
                    id: bsTailsign
                    width: 390
                    text: "A/C Reg"
                    value: backend.tailsign
                    enabled: !backend.installRunning
                    showSelector: backend.actype != ""
                    clickable: backend.actype != ""

                    onClicked: container.selectData("Tailsign", value);
                }

                MenuItem {
                    id: bsAtaChapter;
                    width: 390
                    text: "ATA Chapter";
                    value: backend.ataChapter
                    enabled: !backend.installRunning
                    showSelector: backend.tailsign != ""
                    clickable: backend.tailsign != ""

                    onClicked: container.selectData("ATA Chapter", value);
                }

                MenuItem {
                    id: bsAtaSection;
                    width: 390
                    text: "ATA Section";
                    value: backend.ataSection
                    enabled: !backend.installRunning
                    showSelector: backend.ataChapter != ""
                    clickable: backend.ataChapter != ""

                    onClicked: container.selectData("ATA Section", value);
                }

                MenuItem {
                    id: bsSoftware;
                    width: 390
                    text: "Software P/N";
                    value: backend.software
                    enabled: !backend.installRunning
                    showSelector: backend.ataSection != ""
                    clickable: backend.ataSection != ""

                    onClicked: container.selectData("Software", value);
                }
            }

            HeaderText {
                text: "Data Load"
            }

            Rectangle {
                radius: 10
                border.width: 2
                border.color: "blue"

                width: 400
                height: 200

                color: "lightblue"
                Column {
                    anchors.fill: parent;
                    anchors.margins: 5
                    spacing: 1
                    Row {
                        SmallValItem {
                            text: "Status"
                            value: backend.status
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Disk"
                            value: backend.li_Disk
                            width: 190
                        }
                        SmallValItem {
                            text: "Disks"
                            value: backend.li_Disks
                            width: 200
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "File"
                            value: backend.li_File
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Block"
                            value: backend.li_Block
                            width: 190
                        }
                        SmallValItem {
                            text: "Blocks"
                            value: backend.li_Blocks
                            width: 200
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Estimate Time Remaining"
                            value: backend.remainTime
                            width: 390
                        }
                    }
                }

            }

            Button {
                text: backend.installRunning?"Abort Data Load":"Start Data Load"
                enabled: backend.software != ""
                onClicked: {
                    if (backend.installRunning) {
                        backend.stopDataLoad();
                    } else {
                        backend.startDataLoad();
                    }
                }
            }

            Rectangle {
                width: 400
                height: 190
                color: "white"
                radius: 10
                visible: backend.showLogWindow;
                border.width: 2
                border.color: "gray"

                Flickable {
                    contentHeight: logid.height
                    contentWidth: logid.width
                    anchors.margins: 10
                    anchors.fill: parent;
                    clip: true
                    flickableDirection: Flickable.VerticalFlick
                    contentY: logid.height - height;

                    Text {
                        id: logid
                        text: backend.logText;
                    }
                }
            }
        }
    }
}
