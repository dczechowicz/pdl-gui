// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../colibri_mod"
import "./Components"

Page {
    id: container

    signal selectData( string name, string selected)
    signal tailChanged
    signal swpnChanged

    onVisibleChanged: {
        clkeyboard1.text  = ""
        clkeyboard2.text  = ""
    }

    Rectangle {
        x: 40
        y: 1
        color: "lightgray"

        Column {
            spacing: 7
            HeaderText {
                text: "Selection";
            }
            Column {
                spacing: 7
                Row {
                    width: 200
                    height: 40
                    Text {
                        id: text1
                        anchors.verticalCenter: textInput1.verticalCenter
                        height: 25
                        width: 160
                        text: qsTr("A/C Reg")
                        font.pixelSize: 22
                    }

                    TextInputBackground {
                        id: textInput1
                        width: 125
                        height: 40
                        useDefaultKeyboard: false
                        color: backend.getTailStateColor();
                        onGotFocus: {
                            keyboard1.visible = focus;
                        }
                        onFocusChanged: {
                            keyboard1.visible = focus;
                        }


                        text: clkeyboard1.text
                        //elideleft: true;

                        onTextChanged: {
                            clkeyboard1.text = backend.doCheckTail(clkeyboard1.text);
                            textInput1.color = backend.getTailStateColor();
                        }

                    }
                }

                Row {
                    width: 400
                    height: 40
                    Text {
                        id: text2
                        anchors.verticalCenter: textInput2.verticalCenter
                        height: 25
                        text: "Software P/N";
                        font.pixelSize: 22
                        width: 160
                    }
                    TextInputBackground {
                        id: textInput2
                        width: 200
                        height: 40

                        useDefaultKeyboard: false
                        color: backend.getSwpnStateColor();

                        //echoMode: TextInput.swpn
                        onGotFocus: {
                            keyboard2.visible = focus;
                        }
                        onFocusChanged: {
                            keyboard2.visible = focus;
                        }
                        onTextChanged: {
                            clkeyboard2.text = backend.doCheckSwpn(clkeyboard2.text);
                            //textInput2.text = clkeyboard2.text;
                            textInput2.color = backend.getSwpnStateColor();
                        }
                        text: clkeyboard2.text
                    }
                }
            }

            Rectangle {
                radius: 10
                border.width: 2
                border.color: "blue"

                width: 400
                height: 45

                color: "lightblue"
                Column {
                    anchors.fill: parent;
                    anchors.margins: 5
                    spacing: 1

                    Row {
                        SmallValItem {
                            id: statusItem
                            text: "Status:"
                            value: backend.status
                            width: 390
                            valuecol: backend.statusColor;
                        }
                    }
                }

            }

            Button {
                text: backend.usbButtonText;
                enabled: backend.software != "" && backend.usbButtonEnabled
                onClicked: {
                        //backend.software = software;
                        keyboard1.visible = false;
                        keyboard2.visible = false;
                        backend.prepareUSB();
                }
            }

            Rectangle {
                width: 400
                height: 190
                color: "white"
                radius: 10
                visible: backend.showLogWindow;
                border.width: 2
                border.color: "gray"

                Flickable {
                    contentHeight: logid.height
                    contentWidth: logid.width
                    anchors.margins: 10
                    anchors.fill: parent;
                    clip: true
                    flickableDirection: Flickable.VerticalFlick
                    contentY: logid.height - height;

                    Text {
                        id: logid
                        text: backend.logText;
                    }
                }
            }
        }
    }

    Rectangle {
        id: keyboard1

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: 260
        color: "white"
        radius: 10
        border.width: 2
        border.color: "gray"
        visible: true;

        CLKeyboard {
            id: clkeyboard1

            anchors.fill: parent;
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            showArrayButtons: false
            showCancelCLButton: false
            showOkCLButton: false;
        }
    }
    Rectangle {
        id: keyboard2
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: 260
        color: "white"
        radius: 10
        border.width: 2
        border.color: "gray"
        visible: false;
        Text {
            text: clkeyboard2.text
            font.pixelSize: 15
            color: "white"
        }

        CLKeyboard {
            id: clkeyboard2
            anchors.fill: parent;
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            showArrayButtons: false
            showCancelCLButton: false
            showOkCLButton: false;
        }
    }

}
