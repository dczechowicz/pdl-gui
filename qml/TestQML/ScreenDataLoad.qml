// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "./Components"

Page {
  id: container

  signal selectData( string name, string selected)

  function setData(datatype, value)
  {
    if (datatype == "Airline")
    {
      backend.airline = value;
    }
    else if (datatype == "ACType")
    {
      backend.actype = value;
    }
    else if (datatype == "Tailsign")
    {
      backend.tailsign = value;
    }
//    else if (datatype == "HWFIN")
//    {
//      backend.hwfin = value;
//    }
    else if (datatype == "Software")
    {
      backend.software = value;
    }
  }

  Rectangle {
      x: 40
      y: 1

      Column {
          HeaderText {
              text: "Select Software"
          }

        Rectangle {
          radius: 10
          border.width: 2
          border.color: "blue"

          width: 400
          height: 82 + 36 + 36 + 36

          color: "lightblue"

          Column {
            anchors.fill: parent;
            anchors.margins: 5
            spacing: 1

            MenuItem {
              id: bsAirline
              text: "Airline"
              value: backend.airline;
              enabled: !backend.installRunning

              onClicked: container.selectData("Airline", value);
            }

            MenuItem {
              id: bsACType
              text: "A/C Model"
              value: backend.actype;
              enabled: !backend.installRunning
              showSelector: backend.airline != ""
              clickable: backend.airline != ""

              onClicked: container.selectData("ACType", value);
            }

            MenuItem {
              id: bsTailsign
              text: "A/C Reg"
              value: backend.tailsign
              enabled: !backend.installRunning
              showSelector: backend.actype != ""
              clickable: backend.actype != ""

              onClicked: container.selectData("Tailsign", value);
            }

//            MenuItem {
//              id: bsHWFin;
//              text: "HW FIN";
//              //value: backend.hwfin
//              enabled: !backend.installRunning
//              showSelector: backend.tailsign != ""
//              clickable: backend.tailsign != ""

//              onClicked: container.selectData("HWFIN", value);
//            }

//            MenuItem {
//              id: bsSoftware;
//              text: "Software P/N";
//              value: backend.software
//              enabled: !backend.installRunning
//              showSelector: backend.hwfin != ""
//              clickable: backend.hwfin != ""

//              onClicked: container.selectData("Software", value);
//            }
          }
        }

        HeaderText {
            text: "Data Load"
        }

        Rectangle {
          radius: 10
          border.width: 2
          border.color: "blue"

          width: 400
          height: 200

          color: "lightblue"
          Column {
            anchors.fill: parent;
            anchors.margins: 5
            spacing: 1
            Row {
                SmallValItem {
                    text: "LDR"
                    value: backend.li_LDR
                    width: 95
                }
                SmallValItem {
                    text: "LRU"
                    value: backend.li_LRU
                    width: 95
                }
                SmallValItem {
                    text: "Tx"
                    value: backend.li_Tx
                    width: 65
                }
                SmallValItem {
                    text: "Rx"
                    value: backend.li_Rx
                    width: 65
                }
                SmallValItem {
                    text: "DIS"
                    value: backend.li_Dis
                    width: 70
                }
            }
            Row {
                SmallValItem {
                    text: "Status"
                    value: backend.status
                    width: 145
                }
                SmallValItem {
                    text: "Last"
                    value: backend.li_Last
                    width: 115
                }
                SmallValItem {
                    text: "Reqs"
                    value: backend.li_Reqs
                    width: 130
                }
            }

            Row {
                SmallValItem {
                    text: "Mode"
                    value: backend.li_Mode
                    width: 120
                }
                SmallValItem {
                    text: "Tot. Blks"
                    value: backend.li_TotBlocks
                    width: 180
                }
                SmallValItem {
                    text: "Disk"
                    value: backend.li_Disk
                    width: 90
                }
            }
            Row {
                SmallValItem {
                    text: "File"
                    value: backend.li_File
                    width: 160
                }
                SmallValItem {
                    text: "Block"
                    value: backend.li_Block
                    width: 115
                }
                SmallValItem {
                    text: "Blocks"
                    value: backend.li_Blocks
                    width: 115
                }
            }
            Row {
                SmallValItem {
                    text: "Start";
                    value: backend.startTime
                    width: 130
                }
                SmallValItem {
                    text: "Est.Rem."
                    value: backend.remainTime
                    width: 130
                }
                SmallValItem {
                    text: "Progress"
                    value: backend.updateProgress
                    width: 130
                }
            }
            Row {
                SmallValItem {
                    text: "I";
                    value: backend.li_I
                    width: 98
                }
                SmallValItem {
                    text: "N";
                    value: backend.li_N
                    width: 98
                }
                SmallValItem {
                    text: "L";
                    value: backend.li_L
                    width: 98
                }
                SmallValItem {
                    text: "B";
                    value: backend.li_B
                    width: 98
                }
            }
          }
        }
        Rectangle {
            color: "#AAAAAA"
            height: 5
            width: 10
        }

        Button {
          text: backend.installRunning?"Abort Data Load":"Start Data Load"
          enabled: backend.software != ""
          onClicked: {
            if (backend.installRunning)
            {
              backend.stopDataLoad();
            }
            else
            {
              backend.startDataLoad();
            }
          }
        }
        Rectangle {
            color: "#AAAAAA"
            height: 5
            width: 10
        }

        Rectangle {
          width: 400
          height: 190
          color: "white"
          radius: 10
          visible: backend.showLogWindow;
          border.width: 2
          border.color: "gray"

          Flickable {
            contentHeight: logid.height
            contentWidth: logid.width
            anchors.margins: 10
            anchors.fill: parent;
            clip: true
            flickableDirection: Flickable.VerticalFlick
            contentY: logid.height - height;

            Text {
              id: logid
              text: backend.logText;
            }
          }
        }
      }
  }
}
