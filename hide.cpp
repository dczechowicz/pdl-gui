#include "hide.h"
#include <QFile>
#include <QDir>
#include <QDebug>
#include "fileinfo.h"

Hide::Hide(QObject *parent) :
    QObject(parent)
{
    files.append("DataLoader.pm");
    files.append("DataLoaderConfig.pm");
    files.append("DataLoaderFile.pm");
    files.append("DataLoaderLog.pm");
    files.append("DataLoaderMBS.pm");
    files.append("DataLoaderRecord.pm");
    files.append("DataLoaderSimdata.pm");
    files.append("DataLoaderState.pm");
    files.append("DataLoaderStats.pm");
    files.append("loader.pl");

    FileInfo fi;

    o_dir = fi.getOrigDir();
    s_dir = fi.getSaveDir();
    w_dir = fi.getWorkDir();
}

void Hide::o2w() {
    copyDir(o_dir, w_dir);
}

void Hide::w2o() {
    copyDir(w_dir, o_dir);
}

void Hide::s2w() {
    copyDir(s_dir, w_dir);
}

void Hide::w2s() {
    copyDir(w_dir, s_dir);
}

void Hide::copyDir(QString in, QString out) {
    qDebug() << "copy from:" << in;
    qDebug() << "copt   to:" << out;
    for (int i=0; i < files.length(); i++) {
        enCode(in + "/" + files[i], out + "/" + files[i]);
    }
}

void Hide::enCode(QString name, QString oname) {

    QByteArray input;
    QByteArray mask;
    mask.append(0x27);
    mask.append(0xe2);
    mask.append(0x2c);
    mask.append(0xce);

    mask.append(0x32);
    mask.append(0xf9);
    mask.append(0x17);
    mask.append(0x58);

    mask.append(0x69);
    mask.append(0x96);
    mask.append(0x34);
    mask.append(0xe5);

    mask.append(0x3f);
    int mlen = mask.length();

    QFile f(name);
    if (!f.open(QIODevice::ReadOnly)) {
       return;
    }
    input = f.readAll();
    f.close();

    QByteArray out;
    int p=0;
    for (int i=0; i < input.length(); i++) {
        out.append(input[i] ^ mask[p]);
        if (++p >= mlen)
            p = 0;
    }
    QFile fo(oname);
    if (!fo.open(QIODevice::WriteOnly)) {
       return;
    }
    fo.write(out);
    fo.close();

}
