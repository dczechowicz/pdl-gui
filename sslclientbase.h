#ifndef SSLCLIENTBASE_H
#define SSLCLIENTBASE_H

#include <QObject>
#include <QSslSocket>
#include <QNetworkProxy>

//#include "Backend.h"
#include "fileinfo.h"
#include "config.h"
#include "loadinfo.h"
#include "checkrepository.h"

class Backend;
class LoadInfo;

#define BOUNDARY "----=_Part_0_14615608.1167536350647"

class SslClientBase : public QObject
{
    Q_OBJECT
    friend class SslClient;
public:
    explicit SslClientBase(QObject *parent = 0);
    
private slots:
    void sslErrors(const QList<QSslError> &errors);

private:
    void sendMessage(QString mess, bool usecomp = false, bool first = false);
    void sendMessageMTOM(QString mess);
    QString getUpload();

    void myConnect();
    void disconnect(bool force = false);

private:
    int state;

    CheckRepository mCheckRep;
    QSslSocket *socket;

    void setProxy();
    QNetworkProxy mProxy;
    bool mUseProxy;

    LoadInfo *mLoadInfo;

    FileInfo mFi;
    QString getLogin();
    QString getMeta();
    QString getMedia(QString partnumber, int seq);
    QString getLogoff(int status);
    QString getLogout(int status);
    QString getClientInformation();
    void initStrings();
    QString getIsSoftwareAvailable();
    QString getGetSoftware();
    QString tempfile;

    QString mTicket;

    QStringList mLoadLogInf;

    QString mKey;
    QString mLogin;
    QString mIsSoftware;
    QString mGetSoftware;
    QString mLogoff;
    QString mLogout;
    QString mClInfo;
    QString mGetMeta;
    QString mGetMetaFLS;
    QString mGetMedia;
    QString mLoadLogPre;
    QString mLoadLogPost;
    QString mLogFilePre;
    QString mLogFile;
    QString mLogFilePost;

    QString mRetryPartNumber;
    int mRetryFailCount;
};

#endif // SSLCLIENTBASE_H
