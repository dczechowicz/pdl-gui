// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../colibri_mod"
import "./Components"

Page {
    id: container

    signal selectData( string name, string selected)
    signal tailChanged
    signal swpnChanged

    Rectangle {
        x: 40
        y: 1
        color: "lightgray"

        Column {
            spacing: 7
            HeaderText {
                text: "Selection";
            }
            Column {
                spacing: 7
                Row {
                    width: 200
                    height: 40
                    Text {
                        id: text1
                        anchors.verticalCenter: textInput1.verticalCenter
                        height: 25
                        width: 120
                        text: qsTr("Tailsign")
                        font.pixelSize: 22
                    }

                    TextInputBackground {
                        id: textInput1
                        width: 125
                        height: 40
                        useDefaultKeyboard: false
                        color: backend.getTailStateColor();
                        onGotFocus: {
                            keyboard1.visible = focus;
                        }
                        onFocusChanged: {
                            keyboard1.visible = focus;
                        }
                        /*
                            Text {
                                text: clkeyboard1.text
                                font.pointSize: 17
                            //    text: clkeyboard1.text
                            //    font.pixelSize: 15
                                color: "blue"
                                onTextChanged: {
                                    clkeyboard1.text = backend.doCheckTail(clkeyboard1.text);
                                }
                            }
                            */
                        text: clkeyboard1.text
                        //elideleft: true;

                        onTextChanged: {
                            clkeyboard1.text = backend.doCheckTail(clkeyboard1.text);
                            textInput1.color = backend.getTailStateColor();
                        }

                    }
                }

                Row {
                    width: 400
                    height: 40
                    Text {
                        id: text2
                        anchors.verticalCenter: textInput2.verticalCenter
                        height: 25
                        text: "SWPN:";
                        font.pixelSize: 22
                        width: 120
                    }
                    TextInputBackground {
                        id: textInput2
                        width: 200
                        height: 40

                        useDefaultKeyboard: false
                        color: backend.getSwpnStateColor();

                        //echoMode: TextInput.swpn
                        onGotFocus: {
                            keyboard2.visible = focus;
                        }
                        onFocusChanged: {
                            keyboard2.visible = focus;
                        }
                        onTextChanged: {
                            clkeyboard2.text = backend.doCheckSwpn(clkeyboard2.text);
                            //textInput2.text = clkeyboard2.text;
                            textInput2.color = backend.getSwpnStateColor();
                        }
                        text: clkeyboard2.text
                    }
                }
            }



            HeaderText {
                text: "Data Load"
            }

            Rectangle {
                id: rect
                radius: 10
                border.width: 2
                border.color: "blue"

                width: 400
                height: 200

                color: "lightblue"
                Column {
                    anchors.fill: parent;
                    anchors.margins: 5
                    spacing: 1
                    Row {
                        SmallValItem {
                            text: "Status"
                            value: backend.status
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Disk"
                            value: backend.li_Disk
                            width: 190
                        }
                        SmallValItem {
                            text: "Disks"
                            value: backend.li_Disks
                            width: 200
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "File"
                            value: backend.li_File
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Block"
                            value: backend.li_Block
                            width: 190
                        }
                        SmallValItem {
                            text: "Blocks"
                            value: backend.li_Blocks
                            width: 200
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Estimate Time Remaining"
                            value: backend.remainTime
                            width: 390
                        }
                    }
                }

            }

            Button {
                text: backend.installRunning?"Abort Data Load":"Start Data Load"
                enabled: backend.software != ""
                onClicked: {
                    keyboard1.visible = false;
                    keyboard2.visible = false;
                    if (backend.installRunning) {
                        backend.stopDataLoad();
                    } else {
                        backend.startDataLoad();
                    }
                }
            }

            Rectangle {
                width: 400
                height: 190
                color: "white"
                radius: 10
                visible: backend.showLogWindow;
                border.width: 2
                border.color: "gray"

                Flickable {
                    contentHeight: logid.height
                    contentWidth: logid.width
                    anchors.margins: 10
                    anchors.fill: parent;
                    clip: true
                    flickableDirection: Flickable.VerticalFlick
                    contentY: logid.height - height;

                    Text {
                        id: logid
                        text: backend.logText;
                    }
                }
            }
        }
    }
    Rectangle {
        id: keyboard1

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: 260
        color: "white"
        radius: 10
        border.width: 2
        border.color: "gray"
        visible: false;

        CLKeyboard {
            id: clkeyboard1
            anchors.fill: parent;
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            showArrayButtons: false
            showCancelCLButton: false
            showOkCLButton: false;
        }
    }
    Rectangle {
        id: keyboard2
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: 260
        color: "white"
        radius: 10
        border.width: 2
        border.color: "gray"
        visible: false;
        Text {
            text: clkeyboard2.text
            font.pixelSize: 15
            color: "white"
        }

        CLKeyboard {
            id: clkeyboard2
            anchors.fill: parent;
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            showArrayButtons: false
            showCancelCLButton: false
            showOkCLButton: false;
        }
    }

}
