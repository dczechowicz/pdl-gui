#ifndef SYNCSTARTER_H
#define SYNCSTARTER_H

#include <QObject>
#include <QDeclarativeView>

class SyncStarter : public QObject
{
    Q_OBJECT
public:
    explicit SyncStarter(QObject *parent = 0);
    
    void connectMainScreen(QString name);
    void connectSyncScreen(QString name);

    static SyncStarter *myone;
    static SyncStarter * getInstance();

    void syncScreen() { emit selectSyncScreen(); }
signals:
    void selectSyncScreen();
    void startSync();

private:
    QDeclarativeView mainView;
    QDeclarativeView syncView;
};

#endif // SYNCSTARTER_H
