#include <QtGui/QApplication>
#include "qmlapplicationviewer.h"
#include <QDeclarativeContext>
#include <QtDeclarative>
#include "Backend.h"
#include <QDebug>
#include <QCursor>
#include "hide.h"
#include "config.h"
#include "fileinfo.h"
#include "dbsqlite.h"
#include "signalsock.h"
#include "syncstarter.h"
#include "./network/lanconfiguration.h"
#include "./network/wificonfiguration.h"
#include "./models/dataloadmodel.h"
#include "./helpers/keyboardhelper.h"
#include "./helpers/confighelper.h"
#include "setting.h";
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include "qmyapplication.h"

#define QApplication QMyApplication

#ifdef Q_WS_WIN32
#define SNPRINTF _snprintf
#include "Windows.h"
#else
#define SNPRINTF ::snprintf
#endif

Backend *gBackend;
QmlApplicationViewer *gViewer;
static struct sigaction sigact;
QApplication *wapp;
QmlApplicationViewer *wviewer;

Q_DECL_EXPORT int mainex(int argc, char *argv[], QString msg)
{
    if (!wapp) {
        int xargc = 4;
        char *xargv[] = {0, "-qws", "-display", "Transformed:rot90", 0 };
        //qDebug() << "mainex" << xargv[0] << xargv[1] << xargv[2] << xargv[3] << getpid();
        wapp = new QApplication(xargc, xargv);

        gBackend = new Backend();
        gBackend->WatchInit();

        gViewer = new QmlApplicationViewer();
        gViewer->setOrientation(QmlApplicationViewer::ScreenOrientationLockPortrait);

#if !defined(Q_WS_WIN32) && !defined(Q_WS_X11) && !defined(Q_WS_MAC)
        gViewer->setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
        gViewer->setWindowState(Qt::WindowFullScreen);

        QPixmap nullCursor(16, 16);
        nullCursor.fill(Qt::transparent);
        wapp->setOverrideCursor( QCursor(nullCursor) );
#endif
        QDeclarativeContext *ctxt = gViewer->rootContext();
        ctxt->setContextProperty("backend", gBackend);

        gViewer->setMainQmlFile("qml/TestQML/WatchDogPage.qml");
        gViewer->showExpanded();
        gBackend->addLog(msg);
        gBackend->appRestart(wapp);
        wapp->exec();
        gViewer->hide();
        delete gBackend;
        delete gViewer;
        delete wapp;
        wapp = NULL;
        qDebug() << "Back from Watchdog";
        return 0;
    }
    return 0;
}

Q_DECL_EXPORT int dumpx(int argc, char *argv[])
{
    return 0;
}

Q_DECL_EXPORT static void signal_handler(int sig) {
    switch(sig) {
    case SIGINT:
        qDebug() << "Caught signal:SIGINT";
        break;
    case SIGBUS:
        qDebug() << "Caught signal:SIGBUS";
        exit(10);
    case SIGSEGV:
        qDebug() << "Caught signal:SIGSEGV";
        exit(11);
    }
}

Q_DECL_EXPORT void init_signals(void){
    sigact.sa_handler = signal_handler;
    sigemptyset(&sigact.sa_mask);
    sigact.sa_flags = 0;
    sigaction(SIGINT,  &sigact, (struct sigaction *)NULL);
    sigaction(SIGBUS,  &sigact, (struct sigaction *)NULL);
    sigaction(SIGSEGV, &sigact, (struct sigaction *)NULL);
}

Q_DECL_EXPORT int main(int argc, char *argv[])
{
    qmlRegisterType<DataLoadModel>("Models", 1, 0, "DataLoadModel");
    qmlRegisterType<LanConfiguration>("Network", 1, 0, "LanConfiguration");
    qmlRegisterType<WiFiConfiguration>("Network", 1, 0, "WiFiConfiguration");
    qmlRegisterUncreatableType<AccessPoint>("Network", 1, 0, "AccessPoint", "Could not instantiate AccessPoint");

    FileInfo mFi;
    bool watchdog = mFi.useWatchDog();
    const char *com = *argv;
    qDebug() << "main" << com;

    if (argc == 2) {
        bool doExit = true;
        Hide h;
        printf("conv opt:%s\n", *(argv+1));
        switch(**(argv+1)) {
        case '1': h.o2w(); break;
        case '2': h.w2s(); break;
        case '3': h.s2w(); break;
        case '4': h.w2o(); break;
        case '8':
            watchdog = false;
            doExit = false;
            break;
        case '9':
            watchdog = true;
            doExit = false;
            break;

        }
        if (doExit)
            return 0;
    }

    if (watchdog) {
        qDebug() << "Watchdog 1";
        int status;
        while (true) {
            int pid = fork();

            if (!pid) {
                char *p[] = { (char *)"8", 0 };
                execl(com, com, p[0], p[1]);
                // Does not return here;
                break;
            }

            qDebug() << "Watchdog 2" << pid;
            status = 0;
            int xpid;
            while ((xpid = waitpid(pid, &status, 0)) != pid) {
                qDebug() << "Watchdog 4" << xpid << pid << hex << status;
                status = 0;
            }
            qDebug() << "Watchdog 3" << xpid << pid << hex << status;
            QString t;
            if (status == 0) {
                exit(0);
            }
            mainex(argc, argv, t.sprintf("Status:0x%08x", status));
        }
    }

    FileInfo fi;
    fi.inLoad(false);
    fi.inSync(false);

    //init_signals();

#if !defined(Q_WS_WIN32) && !defined(Q_WS_X11) && !defined(Q_WS_MAC)
    int xargc = 4;
    char *xargv[] = {0, "-qws", "-display", "Transformed:rot90", 0 };
    xargv[0] = argv[0];
    qDebug() << "xargv" << xargv[0] << xargv[1] << xargv[2] << xargv[3] << getpid();
    QScopedPointer<QApplication> app(new QApplication(xargc, xargv));
#else
    qDebug() << "argv" << argv[0] << argv[1] << argv[2] << argv[3] << getpid();
    QScopedPointer<QApplication> app(new QApplication(argc, argv));
#endif

    QCoreApplication::instance()->installEventFilter(app.data());
    QSplashScreen splash(QPixmap(qApp->applicationDirPath() + "/resources/splash.jpg"));
    splash.show();

    gBackend = new Backend();
    gBackend->Init();

    gBackend->getSettingsDB().loadDB();

    bool bv;
    gBackend->getSettingsDB().get_a6153_enabled(&bv);

    QString t;
    gBackend->getSettingsDB().get_pdl_name(t);
    gBackend->setPdlName(t);

    int tm;
    gBackend->getSettingsDB().get_update_timestamp(&tm);
    gBackend->setLastSync(tm);

    // Load and clean SWPNS.
    gBackend->getDB().loadDB();
    gBackend->getDB().verifySWPNs(true);

    gConfig  = new config();

    conf_type_t actconf;
    int connect_type = fi.getConnection();
    bool usesettings = false;
    switch(connect_type) {
    default:
        actconf = CONF_EESM_PROD_INTRA;
        //usesettings = true;
        break;

    case CONF_PROD:
    case CONF_EESM_TEST_INTRA:
    case CONF_EESM_PROD_INTRA:
    case CONF_EESM_TEST:
    case CONF_EESM_PROD:
    case CONF_FLSD_TEST:
    case CONF_FLSD_PROD:
    case CONF_TEST_LOCAL:
    case CONF_TEST_1UND1:
    case CONF_STUNNEL:
        actconf = (conf_type_t)connect_type;
        break;
    }

    qDebug() << "Connection" << actconf;

    gBackend->setServerId(actconf);
    gConfig->setConfig(actconf);
    if (usesettings) {
        QString server;
        if (gBackend->getSettingsDB().get_sync_server(server) and !server.isEmpty()) {
            gConfig->setURL(server);
            qDebug() << "server" << server;
        }
    }

    QmlApplicationViewer viewer;
    gViewer = &viewer;
    viewer.setOrientation(QmlApplicationViewer::ScreenOrientationLockPortrait);

    #if !defined(Q_WS_WIN32) && !defined(Q_WS_X11) && !defined(Q_WS_MAC)
      viewer.setWindowFlags( Qt::FramelessWindowHint | Qt::WindowStaysOnTopHint);
      viewer.setWindowState(Qt::WindowFullScreen);

      QPixmap nullCursor(16, 16);
      nullCursor.fill(Qt::transparent);
      app->setOverrideCursor( QCursor(nullCursor) );
    #endif


      KeyboardHelper keyboardHelper(&viewer);

    /* Register backend functions */
    QDeclarativeContext *ctxt = viewer.rootContext();
    ctxt->setContextProperty( "backend",     gBackend);
    ctxt->setContextProperty( "lm_airline",  gBackend->airlineModel() );
    ctxt->setContextProperty( "lm_actype",   gBackend->actypeModel());
    ctxt->setContextProperty( "lm_tailsign", gBackend->tailsignModel());
    ctxt->setContextProperty( "lm_software", gBackend->softwareModel());
    ctxt->setContextProperty( "keyboardHelper", &keyboardHelper);
    ctxt->setContextProperty( "configHelper", ConfigHelper::instance());

    viewer.setMainQmlFile(QLatin1String((fi.isDemoMode()) ? "qml/TestQML/maindemo.qml" :  "qml/TestQML/main.qml"));
    viewer.showExpanded();
    splash.finish(&viewer);

    signalSock sigsoc;

    int result = app->exec();

    gBackend->stopNetConfigServer();

    return result;
}
