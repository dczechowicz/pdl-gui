#include "config.h"
#include "fileinfo.h"

class config;
config *gConfig;
config::config()
{
    int e;
    for (e=(int)CONF_PROD; e < (int)CONF_MAX; e++) {
        mConfigs.append(config((conf_type_t)e));
    }
}

QList<config> config::mConfigs;

config::config(conf_type_t type)
{
    FileInfo mFi;
    switch(type) {
    case CONF_PROD:
        mURL     = "kons.manage-m.com";
        mName    = "eESM Test Server";
        mPost    = "/fls-service/services/PdlClientWebService";
        mPort    = 443;
        mSecure  = true;
        mSimload = false;
        mFLSDesk = false;
        mClInfo  = true;
        mCert    = "manage-m.com";
        break;
    case CONF_EESM_TEST_INTRA:
        mURL     = "lww.manage-m.lht.ham.dlh.de";
        mName    = "eESM Production Server (intranet)";
        mPost    = "/fls-service/services/PdlClientWebService";
        mPort    = 443;
        mSecure  = true;
        mSimload = false;
        mFLSDesk = false;
        mCert    = "manage-m.com";
        mClInfo  = true;
        break;
    case CONF_EESM_PROD_INTRA:
        mURL     = "lww.manage-m.lht.ham.dlh.de";
        mName    = "eESM Production Server (intranet)";
        mPost    = "/fls-service/services/PdlClientWebService";
        mPort    = 443;
        mSecure  = true;
        mSimload = false;
        mFLSDesk = false;
        mCert    = "manage-m.com";
        mClInfo  = true;
        break;
    case CONF_EESM_TEST:
        mURL     = "kons.manage-m.com";
        mName    = "eESM Test Server";
        mPost    = "/fls-service/services/PdlClientWebService";
        mPort    = 443;
        mSecure  = true;
        mSimload = false;
        mFLSDesk = false;
        mCert    = "manage-m.com";
        mClInfo  = true;
        break;
    case CONF_EESM_PROD:
        mURL     = "www.manage-m.com";
        mName    = "eESM Production Server";
        mPost    = "/fls-service/services/PdlClientWebService";
        mPort    = 443;
        mSecure  = true;
        mSimload = false;
        mFLSDesk = false;
        mCert    = "manage-m.com";
        mClInfo  = true;
        break;
    case CONF_FLSD_TEST:
        mURL     = "s469674182.online.de";
        mURL     = "192.168.20.10";
        mURL     = "fls-desk.com";
        mURL     = "h2165320.stratoserver.net";
        mURL     = "s15896021.onlinehome-server.info";
        mName    = "FLS-Desk Test server";
        mPost    = "/cgi-bin/flsService.pl";
        mPort    = 29000;
        mSecure  = false;
        mSimload = false;
        mFLSDesk = true;
        mClInfo  = false;
        break;
    case CONF_FLSD_PROD:
        mURL     = "s469674182.online.de";
        mURL     = "pentacom-1.homeip.net";
        mName    = "FLS-Desk Prod server";
        mURL     = "s15896021.onlinehome-server.info";
        mURL     = "192.168.20.10";
        mURL     = "fls-desk.com";
        mPost    = "/cgi-bin/flsService.pl";
        mPort    = 443;
        mSecure  = true;
        mSimload = false;
        mFLSDesk = true;
        mCert    = "flsdesk.com";
        mClInfo  = false;
        break;
    case CONF_STUNNEL:
        mURL     = "192.168.20.10";
        mName    = "eESM Production Server";
        mPost    = "/fls-service/services/PdlClientWebService";
        mPort    = 16000;
        mSecure  = false;
        mSimload = false;
        mFLSDesk = false;
        mClInfo  = true;
        break;
    case CONF_TEST_LOCAL:
        mURL     = "localhost";
        mName    = "local test server";
        mPost    = "/fls-service/services/PdlClientWebService";
        mPort    = 29000;
        mSecure = false;
        mSimload = true;
        mFLSDesk = true;
        mClInfo  = false;
        break;
    case CONF_MAX: break;
    }
}


