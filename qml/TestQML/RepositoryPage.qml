import QtQuick 1.1
import "./Components"
import "./Customized"

Page {
    id: root

    TableView {
        id: tableView

        anchors.fill: parent
    }

    onVisibleChanged: {
        if (visible)
            tableView.update();
    }
}
