// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1
import "../colibri_mod"
import "./Components"

Page {
    id: container

    signal selectData( string name, string selected)
    signal tailChanged
    signal swpnChanged

    MessageBox {
        id: messageBoxAbort
        z: 100
        anchors {
            bottom: parent.bottom
        }
        onAccepted: {
            backend.stopDataLoad();
            messageBoxAbort.visible = false;
        }
        onRejected: messageBoxAbort.visible = false;
    }

    onVisibleChanged: {
        clkeyboard1.text  = ""
        clkeyboard2.text  = ""
    }

    Rectangle {
        x: 40
        y: 1
        color: "lightgray"
        focus: true

        Column {
            spacing: 7
            HeaderText {
                text: "Selection";
            }
            Column {
                spacing: 7
                Row {
                    width: 200
                    height: 40
                    Text {
                        id: text1
                        anchors.verticalCenter: textInput1.verticalCenter
                        height: 25
                        width: 160
                        text: qsTr("A/C Reg")
                        font.pixelSize: 22
                    }

                    TextInputBackground {
                        id: textInput1
                        width: 125
                        height: 40
                        useDefaultKeyboard: false
                        color: backend.getTailStateColor();
                        onGotFocus: {
                            keyboard1.visible = focus;
                        }
                        onFocusChanged: {
                            keyboard1.visible = focus;
                        }

                        text: clkeyboard1.text

                        onTextChanged: {
                            clkeyboard1.text = backend.doCheckTail(clkeyboard1.text);
                            textInput1.color = backend.getTailStateColor();
                        }

                    }
                }

                Row {
                    width: 400
                    height: 40
                    Text {
                        id: text2
                        anchors.verticalCenter: textInput2.verticalCenter
                        height: 25
                        text: "Software P/N";
                        font.pixelSize: 22
                        width: 160
                    }
                    TextInputBackground {
                        id: textInput2
                        width: 200
                        height: 40

                        useDefaultKeyboard: false
                        color: backend.getSwpnStateColor();

                        //echoMode: TextInput.swpn
                        onGotFocus: {
                            keyboard2.visible = focus;
                        }
                        onFocusChanged: {
                            keyboard2.visible = focus;
                        }
                        onTextChanged: {
                            clkeyboard2.text = backend.doCheckSwpn(clkeyboard2.text);
                            //textInput2.text = clkeyboard2.text;
                            textInput2.color = backend.getSwpnStateColor();
                        }
                        text: clkeyboard2.text
                    }
                }
            }



            //HeaderText {
                //text: "Data Load"
            //}

            Rectangle {
                radius: 10
                border.width: 2
                border.color: "blue"

                width: 400
                height: (7 * 34) + 4

                color: "lightblue"
                Column {
                    anchors.fill: parent;
                    anchors.margins: 5
                    spacing: 1
                    Row {
                        SmallValItem {
                            id: textSoftwareTitle
                            text: "Software:"
                            value: backend.softwareTitle
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            id: textCoc
                            text: "CoC/Form1:"
                            value: backend.softwareCoC
                            //value: "R.Harvey-Lee (2013/05/16)"
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Estimate Load Time:"
                            value: backend.remainTime
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Disk:"
                            value: backend.li_Disk
                            width: 160
                        }
                        SmallValItem {
                            text: "Block:"
                            value: backend.li_Block
                            width: 230
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "File:"
                            value: backend.li_File
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            text: "Elapsed Time:"
                            value: backend.elapsedTime
                            width: 390
                        }
                    }
                    Row {
                        SmallValItem {
                            id: statusItem
                            text: "Status:"
                            value: backend.status
                            width: 390
                            valuecol: backend.statusColor;
                        }
                    }
                }

            }

            Rectangle {
                height: 6
                visible: true
            }
            Button {
                id: startDataLoadButton
                text: backend.installRunning?"Abort Data Load":"Start Data Load"
                enabled: backend.software != ""
                onClicked: {
                    keyboard1.visible = false;
                    keyboard2.visible = false;
                    if (backend.installRunning) {
                        messageBoxAbort.show(qsTr("Do you really want to abort data load?"), qsTr("Yes"), qsTr("No"));
                    } else {
                        backend.startDataLoad();
                    }
                }
            }

            Rectangle {
                width: 400
                height: 190
                color: "white"
                radius: 10
                visible: backend.showLogWindow;
                border.width: 2
                border.color: "gray"

                Flickable {
                    contentHeight: logid.height
                    contentWidth: logid.width
                    anchors.margins: 10
                    anchors.fill: parent;
                    clip: true
                    flickableDirection: Flickable.VerticalFlick
                    contentY: logid.height - height;

                    Text {
                        id: logid
                        text: backend.logText;
                    }
                }
            }
        }
    }
    Rectangle {
        id: keyboard1

        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: 260
        color: "white"
        radius: 10
        border.width: 2
        border.color: "gray"
        visible: true;

        CLKeyboard {
            id: clkeyboard1
            anchors.fill: parent;
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            showArrayButtons: false
            showCancelCLButton: false
            showOkCLButton: false;
        }
    }
    Rectangle {
        id: keyboard2
        anchors {
            bottom: parent.bottom
            left: parent.left
            right: parent.right
        }

        height: 260
        color: "white"
        radius: 10
        border.width: 2
        border.color: "gray"
        visible: false;
        Text {
            text: clkeyboard2.text
            font.pixelSize: 15
            color: "white"
        }

        CLKeyboard {
            id: clkeyboard2
            anchors.fill: parent;
            anchors.topMargin: 5
            anchors.bottomMargin: 5

            showArrayButtons: false
            showCancelCLButton: false
            showOkCLButton: false;
        }
    }
}
