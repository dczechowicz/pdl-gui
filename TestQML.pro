# Add more folders to ship with the application, here
folder_01.source = qml/TestQML
folder_01.target = qml
folder_02.source = qml/colibri
folder_02.target = qml
folder_03.source = qml/js
folder_03.target = qml
folder_04.source = configs
folder_04.target = .
folder_05.source = scripts
folder_05.target = .
folder_06.source = qml/colibri_mod
folder_06.target = qml
folder_07.source = resources
folder_07.target = .
DEPLOYMENTFOLDERS = folder_01 folder_02 folder_03 folder_04 folder_05 folder_06 folder_07

# Additional import path used to resolve QML modules in Creator's code model
QML_IMPORT_PATH =

symbian:TARGET.UID3 = 0xE0A676AC

# Smart Installer package's UID
# This UID is from the protected range and therefore the package will
# fail to install if self-signed. By default qmake uses the unprotected
# range value if unprotected UID is defined for the application and
# 0x2002CCCF value if protected UID is given to the application
#symbian:DEPLOYMENT.installer_header = 0x2002CCCF

# Allow network access on Symbian
symbian:TARGET.CAPABILITY += NetworkServices

# If your application uses the Qt Mobility libraries, uncomment the following
# lines and add the respective components to the MOBILITY variable.
# CONFIG += mobility
# MOBILITY +=

QT += network
QT += sql

LIBS += -lz

linux:build_number.h.commands = ../poland/build_number.sh
mac:build_number.h.commands = ../poland/build_number.sh
linux:build_number.h.depends = FORCE
linux:QMAKE_EXTRA_TARGETS += build_number.h
linux:PRE_TARGETDEPS += build_number.h

# Speed up launching on MeeGo/Harmattan when using applauncherd daemon
# CONFIG += qdeclarative-boostable

# Add dependency to Symbian components
# CONFIG += qt-components

# The .cpp file which was generated for your project. Feel free to hack it.
SOURCES += main.cpp \
    Backend.cpp \
    hide.cpp \
    sslclient.cpp \
    webconnect.cpp \
    certificateinfo.cpp \
    fileinfo.cpp \
    loadclient.cpp \
    loadinfo.cpp \
    monitorserver.cpp \
    config.cpp \
    network/configuration.cpp \
    network/wificonfiguration.cpp \
    network/lanconfiguration.cpp \
    dbsqlite.cpp \
    models/dataloadmodel.cpp \
    helpers/keyboardhelper.cpp \
    helpers/confighelper.cpp \
    dbsettings.cpp \
    replyprocessor.cpp \
    mediainfo.cpp \
    checkrepository.cpp \
    replyprocessorwrapper.cpp \
    modaldialog.cpp \
    signalsock.cpp \
    sslclientbase.cpp \
    bthread.cpp \
    replyprocessormeta.cpp \
    replyprocessormedia.cpp \
    syncstarter.cpp \
    syncthread.cpp \
    replyprocessorlogin.cpp \
    runclient.cpp \
    setting.cpp \
    sms.cpp \
    replyprocessorsoftware.cpp \
    qmyapplication.cpp

# Please do not modify the following two lines. Required for deployment.
include(qmlapplicationviewer/qmlapplicationviewer.pri)
qtcAddDeployment()

HEADERS += \
    Backend.h \
    hide.h \
    sslclient.h \
    webconnect.h \
    certificateinfo.h \
    fileinfo.h \
    loadclient.h \
    loadinfo.h \
    monitorserver.h \
    config.h \
    network/configuration.h \
    network/wificonfiguration.h \
    network/lanconfiguration.h \
    dbsqlite.h \
    models/dataloadmodel.h \
    helpers/keyboardhelper.h \
    helpers/confighelper.h \
    dbsettings.h \
    replyprocessor.h \
    mediainfo.h \
    checkrepository.h \
    replyprocessorwrapper.h \
    modaldialog.h \
    signalsock.h \
    sslclientbase.h \
    bthread.h \
    replyprocessormeta.h \
    replyprocessormedia.h \
    syncstarter.h \
    syncthread.h \
    replyprocessorlogin.h \
    runclient.h \
    setting.h \
    sms.h \
    replyprocessorsoftware.h \
    build_number.h \
    qmyapplication.h

FORMS += \
    sslerrors.ui \
    sslclient.ui \
    certificateinfo.ui

OTHER_FILES += \
    qml/js/Styler.js

