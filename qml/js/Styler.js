.pragma library

var Colors = {
    Background: {
        Color: "#D8DBE0"
    },
    Separator: {
        Color:  "#A5A5A5"
    },
    Header: {
        Background: {
            Color: "#343434"
        }
    },
    TableHeader: {
        Color: "gray"
    }
}

var Fonts = {
    Family: "Verdana",

    SectionHeader: {
        Color: "#4D4D4D",
        Weight: "Bold",
        PixelSize: 28
    },
    ItemTitle: {
        Color: "#343434",
        Weight: "Bold",
        PixelSize: 18
    },
    ItemValue: {
        Color: "#1D338C",
        Weight: "Normal",
        PixelSize: 18
    },
    Header: {
        Color: "#FFFFFF",
        Weight: "Bold",
        PixelSize: 30
    },
    TableHeader: {
        Color: "black",
        Weight: "Bold",
        PixelSize: 12
    },
    TableField: {
        Color: "black",
        Weight: "Normal",
        PixelSize: 12
    }
}

var Sizes = {
    MenuItem: {
        Width: 390,
        Height: 50
    },
    Button: {
        Width: 400,
        Height: 70
    }
}
