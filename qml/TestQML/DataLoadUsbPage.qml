import QtQuick 1.1
import "./Components"

Page {
    id: container

    property string airlineText: ""
    property string airline: ""
    property string acType: ""
    property string tailsign: ""
    property string ataChapter: ""
    property string ataSection: ""
    property string software: ""

    function reset() {
        airlineText = "";
        airline = "";
        acType = "";
        tailsign = "";
        ataChapter = "";
        ataSection = "";
        software = "";
    }

    signal selectData( string name, string selected)

    function setData(datatype, value)
    {
        if (datatype == "Airline")
        {
            airline = value;
            setData("ACType", "");
        }
        else if (datatype == "ACType")
        {
            acType = value;
            setData("Tailsign", "");
        }
        else if (datatype == "Tailsign")
        {
            tailsign = value;
            backend.tailsign = tailsign;
            setData("ATA Chapter", "");
        }
        else if (datatype == "ATA Chapter")
        {
            ataChapter = value;
            setData("ATA Section", "");
        }
        else if (datatype == "ATA Section")
        {
            ataSection = value;
            setData("Software", "");
        }
        else if (datatype == "Software")
        {
            software = value;
            backend.software = software;
        }
    }

    Flickable {
        anchors.topMargin: 1
        anchors.fill: parent
        contentHeight: column.childrenRect.height + 20
        Column {
            id: column
            spacing: 7
            anchors.horizontalCenter: parent.horizontalCenter
            HeaderText {
                text: "Select Software"
            }

            GroupBox {
                radius: 10
                border.width: 2
                border.color: "blue"

                color: "lightblue"

                MenuItem {
                    id: bsAirline
                    width: 390
                    text: "Airline"
                    value: airlineText;
                    enabled: !backend.installRunning

                    onClicked: container.selectData("Airline", value);
                }

                MenuItem {
                    id: bsACType
                    width: 390
                    text: "A/C Model"
                    value: acType;
                    enabled: !backend.installRunning
                    showSelector: airline != ""
                    clickable: airline != ""

                    onClicked: container.selectData("ACType", value);
                }

                MenuItem {
                    id: bsTailsign
                    width: 390
                    text: "A/C Reg"
                    value: tailsign;
                    enabled: !backend.installRunning
                    showSelector: acType != ""
                    clickable: acType != ""

                    onClicked: container.selectData("Tailsign", value);
                }

                MenuItem {
                    id: bsAtaChapter;
                    width: 390
                    text: "ATA Chapter";
                    value: ataChapter;
                    enabled: !backend.installRunning
                    showSelector: tailsign != ""
                    clickable: tailsign != ""

                    onClicked: container.selectData("ATA Chapter", value);
                }

                MenuItem {
                    id: bsAtaSection;
                    width: 390
                    text: "ATA Section";
                    value: ataSection;
                    enabled: !backend.installRunning
                    showSelector: ataChapter != ""
                    clickable: ataChapter != ""

                    onClicked: container.selectData("ATA Section", value);
                }

                MenuItem {
                    id: bsSoftware;
                    width: 390
                    text: "Software P/N";
                    value: software;
                    enabled: !backend.installRunning
                    showSelector: ataSection != ""
                    clickable: ataSection != ""

                    onClicked: container.selectData("Software", value);
                }
            }

            Rectangle {
                radius: 10
                border.width: 2
                border.color: "blue"

                width: 400
                height: 45

                color: "lightblue"
                Column {
                    anchors.fill: parent;
                    anchors.margins: 5
                    spacing: 1

                    Row {
                        SmallValItem {
                            id: statusItem
                            text: "Status:"
                            value: backend.status
                            width: 390
                            valuecol: backend.statusColor;
                        }
                    }
                }

            }
            Button {
                text: backend.usbButtonText;
                enabled: software != "" && backend.usbButtonEnabled
                onClicked: {
                    //backend.software = software;
                    if (backend.showUSBAbort) {
                        backend.onAbortUSB();
                    } else {
                        backend.prepareUSB();
                    }
                }
            }

            Rectangle {
                width: 400
                height: 190
                color: "white"
                radius: 10
                visible: backend.showLogWindow;
                border.width: 2
                border.color: "gray"

                Flickable {
                    contentHeight: logid.height
                    contentWidth: logid.width
                    anchors.margins: 10
                    anchors.fill: parent;
                    clip: true
                    flickableDirection: Flickable.VerticalFlick
                    contentY: logid.height - height;

                    Text {
                        id: logid
                        text: backend.logText;
                    }
                }
            }
        }
    }
}
