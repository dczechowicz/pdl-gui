#include "keyboardhelper.h"
#include <QDeclarativeView>
#include <QGraphicsScene>
#include <QKeyEvent>

KeyboardHelper::KeyboardHelper(QDeclarativeView *view, QObject *parent) :
    QObject(parent)
  , m_view(view)
{
}

void KeyboardHelper::pressKey(const QString &character, int key)
{
    QKeyEvent pressEvent(QKeyEvent::KeyPress, key, Qt::NoModifier, character);

    QGraphicsItem *focusItem = m_view->scene()->focusItem();

    if (focusItem) {
        m_view->scene()->sendEvent(focusItem, &pressEvent);
    }
}
