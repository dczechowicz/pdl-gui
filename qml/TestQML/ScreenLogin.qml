import QtQuick 1.1
import "../colibri_mod"
import "./Components"

Page {
  id: container

  signal homeClicked
  signal shutdownClicked
  signal restartClicked

  Rectangle {
      x: 40
      y: 1
      Column {
          HeaderText {
              text: "Login";
          }

          Row {
              width: 400
              height: 30
              Text {
               id: text1
               height: 25
               width: 160
               text: qsTr("Username:")
               font.pixelSize: 19
              }

              TextInputBackground {
                id: textInput1
                width: 240
                height: 25
              }
          }

          Row {
              width: 400
              height: 30
              Text {
                  id: text2
                  height: 25
                  text: "Password";
                  font.pixelSize: 19
                  width: 160
              }

              TextInputBackground {
                  id: textInput2
                  width: 240
                  height: 25
                  passwordCharacter: "*"
                  echoMode: TextInput.Password
              }
          }

          Button {
             id: btnSignals
             text: qsTr("Login")

             onClicked: container.homeClicked();
          }

          HeaderText {
              text: qsTr("System")
          }

          Button {
             id: btnShutdown
             text: qsTr("Shutdown");

             onClicked: container.shutdownClicked()
          }
          Button {
             id: btnRestart
             text: qsTr("Restart");

             onClicked: container.restartClicked()
          }
      }
  }
}
