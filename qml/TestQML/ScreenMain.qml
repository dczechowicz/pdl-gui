// import QtQuick 1.0 // to target S60 5th Edition or Maemo 5
import QtQuick 1.1

Rectangle {
  width: 480;
  height: 750;
  id: container

  signal dataLoadClicked;
  signal dataLoadManualClicked;
  signal secureUsbClicked;
  signal updateRepositoryClicked;
  signal uploadResultsClicked;
  signal administrationClicked;
  signal informationClicked;
  signal shutdownClicked;
  signal restartClicked();

  Rectangle {
    x: 40
    y: 1
    height: parent.height

    Column
    {
      HeaderText {
        text: "PDL"
      }

      Rectangle {
        id: menupdl

        radius: 10
        border.width: 2
        border.color: "gray"

        width: 400
        height: (4 * 36) + 12

          color: "lightgray"

          signal administration;
          signal information;

          Column {
            anchors.fill: parent;
            anchors.margins: 5
            spacing: 1

            MenuItem {
              text: "Data Load"
              onClicked: container.dataLoadClicked();
            }

            MenuItem {
                text: "Data Load (Manual)"
              onClicked: container.dataLoadManualClicked();
            }

            /*
              MenuItem {
              text: "Secure USB"
              //onClicked: container.secureUsbClicked();
            }
            */

            MenuItem {
              text: "Update Repository";
              onClicked: container.updateRepositoryClicked();
            }
            MenuItem {
              text: "Upload Results";
              onClicked: container.uploadResultsClicked();
            }
          }
        }

      HeaderText {
        text: "System"
      }

         Rectangle {
          //anchors.top: system_text.bottom;
          //anchors.topMargin: 5
          //anchors.left: parent.left
          id: menusystem

          radius: 10
          border.width: 2
          border.color: "gray"

          width: 400
          height: (backend.showAdministration?82:46)

          color: "lightgray"

          signal administration;
          signal information;

          Column {
            anchors.fill: parent;
            anchors.margins: 5
            spacing: 1

            MenuItem {
              visible: backend.showAdministration
              text: "Administration"
              onClicked: container.administrationClicked();
            }

            MenuItem {
              text: "Information";
              onClicked: container.informationClicked();
            }
          }
        }

         HeaderText {
             text: qsTr("")
         }

         Button {
            id: btnShutdown
            text: qsTr("Shutdown");

            onClicked: container.shutdownClicked()
         }
         Button {
            id: btnRestart
            text: qsTr("Restart");

            onClicked: container.restartClicked()
         }
        /* Button {
          width: 400
          text: qsTr("Shutdown");
          onClicked: container.shutdownClicked()
        } */
      }
  }
}
