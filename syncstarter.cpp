#include "syncstarter.h"
#include <QDebug>

SyncStarter::SyncStarter(QObject *parent) :
    QObject(parent)
{
    myone = this;
}

SyncStarter * SyncStarter::myone = 0;
SyncStarter * SyncStarter::getInstance() {
    return myone;
}

void SyncStarter::connectMainScreen(QString name) {
    qDebug() << "connectMainScreen" << name;
    mainView.setSource(QUrl(name));
    mainView.setResizeMode(QDeclarativeView::SizeRootObjectToView);
    QObject *rootObject = (QObject*)(mainView.rootObject());
    QObject::connect(this, SIGNAL(selectSyncScreen()), rootObject, SLOT(updateRepositoryClicked()));
}

void SyncStarter::connectSyncScreen(QString name) {
    qDebug() << "connectSyncScreen" << name;
    syncView.setSource(QUrl(name));
    //syncView.setResizeMode(QDeclarativeView::SizeRootObjectToView);
    QObject *rootObject = (QObject*)(syncView.rootObject());
    QObject::connect(this, SIGNAL(selectSyncScreen()), rootObject, SLOT(updateRepositoryClicked()));
}

