#include "replyprocessorlogin.h"
#include <QDebug>
#include <QXmlStreamReader>

ReplyProcessorLogin::ReplyProcessorLogin()
{
}

bool ReplyProcessorLogin::addData(QByteArray & nData) {
    mInOffset     = 0;
    dumpData(nData);
    mLocalBuffer.append(nData);
    mLength       = mLocalBuffer.length();
    bool ret      = false;

    if (mDebug) qDebug() << "nData.length" << nData.length() << mLength;

    if (mState == PROC_FIRST_HEADER) {
        mState = PROC_FIRST_DATA;
        getHeader(mLocalBuffer);
    }

    if (mDebug)  qDebug() << "mInOffset" << mInOffset;

    bool end = (mChunked) ? getChunked(mLocalBuffer) : getData(mLocalBuffer);
    mLocalBuffer.clear();

    qDebug() << "end:" << end << mBuffer.length();
    if (mState == PROC_FIRST_DATA) {
        mBuffer = mBuffer.mid(mInOffset);
        mState = PROC_DATA;
    }
    qDebug() << "mBuffer.length   " << mBuffer.length();
    mZipBuffer.append(mBuffer);
    qDebug() << "mZipBuffer.length" << mZipBuffer.length();
    qDebug() << "mZipBuffer" << mZipBuffer;
    mBuffer.clear();

    if (mContentLen >= 0 and mZipBuffer.length() == mContentLen)
        end = true;

    //qDebug() << "endcc:" << end << mContentLen << mZipBuffer.length();
    if (end) {
        closeDump();
        mBuffer = (mGZipped) ? gzipDecompress(mZipBuffer) : mZipBuffer;
        //qDebug() << "Length" << mBuffer.length() << mBuffer;
        getMessageInfo();
        return true;
    }

    return ret;
}

int ReplyProcessorLogin::getMessageInfo() {
    int offset = 0;
    QByteArray line;
    line.reserve(200000);
    bool xmlfnd=false;
    int mtlc=0;
    while (true) {
        offset = getLineGen(mBuffer, offset, line);
        //if (mDebug) qDebug() << "line:" << line;

        if (line.isEmpty()) {
            qDebug() << "Line empty";
            mtlc++;
            if (xmlfnd) {
                if (mDebug) qDebug() << "File offset:" << hex << offset;
                mBuffer = mBuffer.mid(offset);
                if (mDebug) qDebug() << "buffer" << hex << mBuffer;
                return offset;
            }
            if (mtlc > 5)
                return offset;
            continue;
        }
        if (line.startsWith("<soap")) {
            if (mDebug) qDebug() << "XML" << line;
            xmlfnd=true;
            dumpInfo(line);
        }
    }
}

bool ReplyProcessorLogin::dumpInfo(QByteArray xml) {
    QString name;
    QXmlStreamReader sr(xml);
    QXmlStreamAttributes sa;

    while (!sr.atEnd()) {
          sr.readNext();
          switch (sr.tokenType()) {
          case QXmlStreamReader::StartElement:
              name = sr.name().toString();
              sa = sr.attributes();
              conv2sql(name, sr.attributes());
              continue;
          case QXmlStreamReader::EndElement:
              name = sr.name().toString();
              continue;
          case QXmlStreamReader::Characters:
              qDebug() << "Characters" << name << sr.text().toString();
              mNameVal[name] = sr.text().toString();
              break;
          default:
              qDebug() << "Token:" << sr.tokenType();
              qDebug() << name << sr.text().toString();
              break;
          }
    }
    return false;
}

void ReplyProcessorLogin::conv2sql(QString name,  QXmlStreamAttributes sa) {
    QXmlStreamAttribute  attr;
    for (int i=0; i < sa.count(); i++) {
        attr = sa[i];
        QString qualname   = name + '.' + attr.name().toString();
        qDebug() << "name:" << qualname << attr.value().toString();
        mNameVal[qualname] = attr.value().toString();
    }
}
