#ifndef HIDE_H
#define HIDE_H

#include <QObject>

class Hide : public QObject
{
    Q_OBJECT
public:
    explicit Hide(QObject *parent = 0);
    void enCode(QString name, QString oname);

    void o2w();
    void w2o();
    void s2w();
    void w2s();
    void copyDir(QString in, QString out);

signals:
    
public slots:

private:
    QList<QString> files;
    QString o_dir;
    QString w_dir;
    QString s_dir;
    QString b_dir;
};

#endif // HIDE_H
